package link.ignyte.plugins.bedwars.Utils;

import link.ignyte.plugins.bedwars.Maps.GameMap;
import link.ignyte.plugins.bedwars.Maps.MapManager;

import java.util.ArrayList;

public class MapStatistics {

    public static int finishedMaps;
    public static int totalMaps;
    public static int teams;
    public static int players;
    public static int spawners;
    public static int pages;

    public static void load() {
        ArrayList<GameMap> maps = MapManager.get(MapManager.class).maps;
        totalMaps = maps.size();
        pages = maps.size() / 27;
        finishedMaps = 0;
        for (GameMap map : maps) {
            if (map.available) {
                finishedMaps++;
            }
            teams += map.teamManager.teams.size();
            players += map.teamManager.teams.size() * map.teamManager.getTeamSize();
            spawners += map.locs.getSpawners().size();
        }
    }
}
