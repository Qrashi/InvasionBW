package link.ignyte.plugins.bedwars.Utils;

import hu.trigary.advancementcreator.Advancement;
import hu.trigary.advancementcreator.AdvancementFactory;
import link.ignyte.plugins.bedwars.BedWars;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.advancement.AdvancementProgress;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Iterator;

public class Advancements {

    public static ArrayList<String> registered = new ArrayList<>();

    public static void award(Player player, String key) {
        org.bukkit.advancement.Advancement adv = Bukkit.getAdvancement(KeyAssistant.getKey(key));
        if (adv != null) {
            AdvancementProgress progress = player.getAdvancementProgress(adv);
            if (!progress.isDone()) {
                for (String criteria : progress.getRemainingCriteria()) progress.awardCriteria(criteria);
            }
        }
    }

    public static void initialize() {
        if (needsRegistration("bedwars/root")) {
            AdvancementFactory factory = new AdvancementFactory(BedWars.getInstance(), true, false);
            Advancement root = factory.getRoot("bedwars/root", "Join a game", "BedWars Advancements", Material.RED_BED, "block/red_wool");
            Advancement play = factory.getImpossible("bedwars/root/play", root, "Play a game", "Start a game of BedWars", Material.IRON_SWORD).setHidden(true);

            Advancement die = factory.getImpossible("bedwars/root/play/die", play, "Die", "Suffer a death while playing BedWars\nThere is always a first time :'(", Material.ROSE_BUSH).setHidden(true);
            factory.getImpossible("bedwars/root/play/die/100", die, "Die x100", "Suffer death - 100 times", Material.LAVA_BUCKET).setHidden(true);

            Advancement destroy_bed = factory.getImpossible("bedwars/root/play/destroy_bed", play, "Destroy a bed", "Destroy a opponents team bed", Material.WHITE_BED).setHidden(true);
            factory.getImpossible("bedwars/root/play/destroy_bed/100", destroy_bed, "Destroy 100 beds", "Destroy 100 beds", Material.RED_BED).setHidden(true);

            Advancement kill = factory.getImpossible("bedwars/root/play/kill", play, "Kill", "Kill another player", Material.SKELETON_SKULL).setHidden(true);
            factory.getImpossible("bedwars/root/play/kill/100", kill, "Kill x100", "Kill another player - 100 times", Material.WITHER_SKELETON_SKULL).setHidden(true);

            Advancement win = factory.getImpossible("bedwars/root/play/win", play, "Win", "Win a game of BedWars", Material.CAKE).setHidden(true);
            factory.getImpossible("bedwars/root/play/win/100", win, "Win x100", "Win a game of BedWars - 100 times", Material.FIREWORK_ROCKET).setHidden(true);

            Advancement until_the_end = factory.getImpossible("bedwars/root/play/end", play, "Until the end", "Play a game of BedWars\nuntil the end.", Material.IRON_SWORD).setHidden(true);
            factory.getImpossible("bedwars/root/play/end/nodeath", until_the_end, "Don't die", "Finish a game without dying", Material.DIAMOND).setHidden(true);

            Advancement build = factory.getImpossible("bedwars/root/build", root, "BuildMode", "Help building a map", Material.IRON_AXE).setHidden(true);
            factory.getImpossible("bedwars/root/build/create", build, "Create a map", "Create a map", Material.NETHER_STAR).setHidden(true);
            factory.getImpossible("bedwars/root/build/finish", build, "Finish a map", "Finish a map and mark it as playable", Material.LIME_STAINED_GLASS_PANE).setHidden(true);

            // Future expansions can just add here.

            Bukkit.reloadData();
        }
    }

    private static boolean needsRegistration(String key) {
        // True advancement doesnt exist
        Iterator<org.bukkit.advancement.Advancement> iterator = Bukkit.advancementIterator();
        while (iterator.hasNext()) {
            org.bukkit.advancement.Advancement advancement = iterator.next();
            if (advancement.getKey().getKey().equalsIgnoreCase(key)) {
                return false;
            }
        }
        return true;
    }

}
