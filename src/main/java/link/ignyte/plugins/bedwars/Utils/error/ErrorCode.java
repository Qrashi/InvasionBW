package link.ignyte.plugins.bedwars.Utils.error;

public class ErrorCode {

    private final String code;
    private final ErrorSeverity severity;

    public ErrorCode(ErrorEffect effect, ErrorSeverity severity, ErrorRelation relation, String codeIdentifier) {
        code = effect.toString().charAt(0) + severityToInt(severity) + relation.toString().charAt(0) + "#" + codeIdentifier;
        this.severity = severity;
    }

    public ErrorSeverity getSeverity() {
        return severity;
    }

    public String toString() {
        return code;
    }

    private int severityToInt(ErrorSeverity severity) {
        return switch (severity) {
            case LOWEST -> 0;
            case LOW -> 1;
            case MIDDLE -> 2;
            case HIGH -> 3;
            case ALARM -> 4;
        };
    }

}
