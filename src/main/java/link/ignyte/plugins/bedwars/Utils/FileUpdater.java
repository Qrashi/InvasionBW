package link.ignyte.plugins.bedwars.Utils;

import link.ignyte.plugins.bedwars.BedWars;
import link.ignyte.plugins.bedwars.Maps.ClearState;
import link.ignyte.plugins.bedwars.Maps.GameMap;
import link.ignyte.plugins.bedwars.Maps.MapManager;
import link.ignyte.plugins.bedwars.Maps.SerializableLocation;
import link.ignyte.utils.JsonSingleton.JsonSingletons;
import org.bukkit.Bukkit;

import java.io.File;

import static link.ignyte.plugins.bedwars.BedWars.log;

public class FileUpdater {

    private static boolean updated = false;

    @SuppressWarnings("deprecated")
    public static void update() {
        MapManager mapManager = MapManager.get(MapManager.class);

        if (mapManager.version > MapManager.CURRENT_MAPS_VERSION) {
            throw new NullPointerException("Cannot load maps, version too new, please update plugin!");
        } else if (mapManager.version < MapManager.CURRENT_MAPS_VERSION) {
            updated = true;
            switch (mapManager.version) {
                case 0:
                    for (GameMap map : mapManager.maps) {
                        map.bbox.state = ClearState.NOT_CLEARED;
                        map.teamManager.team_size = map.teamManager.teams.get(0).teamSize;
                    }
                    log("Updated MapManager from v0 to v1");
                    mapManager.version = 1;

                    update();
            }
        }

        Options options = Options.get(Options.class);

        if (options.version > Options.CURRENT_OPTIONS_VERSION) {
            throw new NullPointerException("Cannot load option, version too new, please update plugin!");
        } else if (options.version < Options.CURRENT_OPTIONS_VERSION) {
            updated = true;
            switch (options.version) {
                case 0:
                    BedWars.getLogging().warning("New Advancements have been added. Removing old Advancements...");

                    // BedWars folder -> plugins -> Server folder
                    try {
                        File file = new File(BedWars.getInstance().getDataFolder().getParentFile().getParentFile().getPath() + "/world/datapacks/bukkit/data/bedwars/");
                        if (file.exists()) {
                            if (!file.delete()) {
                                BedWars.getLogging().warning("Could not delete old advancements!");
                            } else {
                                Bukkit.reloadData();
                                BedWars.getLogging().warning("Deleted old advancements!");
                            }
                        }
                    } catch (Exception e) {
                        BedWars.getLogging().warning("Could not delete old advancements!");
                    }

                    options.spawn = new SerializableLocation(0.5, 64.815, 0.5, 90, 0);
                    options.spawn_worldborder_size = 150;
                    options.lobby_time = 12500;
                    options.put_spectators_into_teams = true;
                    options.put_dead_players_into_teams = false;
                    options.version = 1;
                    log("Updated OptionsManager from v0 to v1");

                    update();
            }
        }
        if (updated) {
            log("Files have been updated, saving.");
            JsonSingletons.saveAll();
        }
    }
}
