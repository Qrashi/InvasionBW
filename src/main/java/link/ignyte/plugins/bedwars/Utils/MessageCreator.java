package link.ignyte.plugins.bedwars.Utils;

import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class MessageCreator {

    private static final Options options = Options.get(Options.class);

    public static String translate(String message) {
        return ChatColor.translateAlternateColorCodes('&', "&7" + message);
    }

    public static String t(String message) {
        return translate(message);
    }

    public static TextComponent generateComponent(String message) {
        return new TextComponent(t(message));
    }

    public static void sendPlayersWithPermission(String message, String permission) {
        Bukkit.getOnlinePlayers().forEach(player -> {
            if (player.hasPermission(permission)) {
                player.sendMessage(t(message));
            }
        });
    }

    public static String gameNameNoColors() {
        return options.getGameNameNoColors();
    }

    public static String gameName() {
        return options.game_name;
    }

    public static String withPrefix(String message) {
        return t("&7[" + options.game_name + "&7] " + message);
    }

    public static void sendWithPrefix(String message) {
        Bukkit.broadcastMessage(t("&7[" + options.game_name + "&7] " + message));
    }

    public static void sendTitle(Player player, String title, String subtitle, int duration) {
        player.sendTitle(MessageCreator.t(title), MessageCreator.t(subtitle), 10, duration, 20);
    }

    public static void sendTitle(Player player, String title, String subtitle, int duration, boolean fade) {
        player.sendTitle(MessageCreator.t(title), MessageCreator.t(subtitle), 0, duration, 0);
    }
}
