package link.ignyte.plugins.bedwars.Utils;

import com.google.gson.annotations.Expose;
import link.ignyte.plugins.bedwars.BedWars;
import link.ignyte.plugins.bedwars.Inventories.Setup.SetupInventories;
import link.ignyte.plugins.bedwars.Management.GameManager;
import link.ignyte.plugins.bedwars.Management.GameState;
import link.ignyte.plugins.bedwars.Management.PlayType;
import link.ignyte.plugins.bedwars.Management.ScoreboardManager;
import link.ignyte.plugins.bedwars.Maps.GameMap;
import link.ignyte.plugins.bedwars.Maps.MapManager;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.Random;

public class AutoMode {
    @Expose
    private boolean enabled;
    @Expose
    private boolean forceMap;
    @Expose
    private String forcedMap;
    @Expose
    private boolean restart;
    @Expose
    private boolean kick;


    public AutoMode() {
        this.enabled = false;
        this.forceMap = false;
        this.forcedMap = "";
        this.restart = false;
        this.kick = false;
        //update(false);
    }

    public void start() {
        if (enabled) {
            BedWars.getLogging().warning("Auto-mode is enabled - selecting maps and starting...");
            GameMap toSet = null;
            if (forceMap) {
                toSet = MapManager.getMapByName(forcedMap);
            }
            if (toSet == null) {
                int available = 0;
                GameMap lastAvailable = null;
                for (GameMap map : MapManager.get(MapManager.class).maps) {
                    if (map.available) {
                        available++;
                        lastAvailable = map;
                    }
                }
                if (available == 0) {
                    enabled = false;
                    return;
                } else if (available == 1) {
                    toSet = lastAvailable;
                } else {
                    ArrayList<GameMap> avialible = new ArrayList<>();
                    MapManager.get(MapManager.class).maps.forEach(gameMap -> {
                        if (gameMap.available) avialible.add(gameMap);
                    });
                    toSet = MapManager.get(MapManager.class).maps.get(new Random().nextInt(avialible.size()));

                }
                if (toSet == null) {
                    return;
                }
            }
            GameManager.setGameMap(toSet);
            GameManager.setPlayType(PlayType.PLAYING);
            GameManager.setState(GameState.COUNTDOWN);
            SetupInventories.mapInvFinished();
            ScoreboardManager.now();
        }
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
        update(enabled);
    }

    public boolean isForceMap() {
        return forceMap;
    }

    public void setForceMap(boolean forceMap) {
        this.forceMap = forceMap;
        update(false);
    }

    public String getForcedMap() {
        return forcedMap;
    }

    public void setForcedMap(String forcedMap) {
        this.forcedMap = forcedMap;
        update(false);
    }

    public boolean isRestart() {
        return restart;
    }

    public void setRestart(boolean restart) {
        this.restart = restart;
        update(false);
    }

    public boolean isKick() {
        return kick;
    }

    public void setKick(boolean kick) {
        this.kick = kick;
        update(false);
    }

    public void update(boolean newEnabled) {
        if (newEnabled) {
            Bukkit.broadcastMessage(MessageCreator.t("&7[&bAUTO-MODE&7] &aStarting auto mode - reloading.."));
            BedWars.getLogging().info("Saving options...");
            Options.save(Options.class);
            BedWars.getInstance().reload();
        } else {
            BedWars.getLogging().info("Saving options...");
            Options.save(Options.class);
            Bukkit.broadcastMessage(MessageCreator.t("&7[&bAUTO-MODE&7] &aChanges set."));
        }
    }
}
