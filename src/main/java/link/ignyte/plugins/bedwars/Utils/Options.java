package link.ignyte.plugins.bedwars.Utils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import link.ignyte.plugins.bedwars.Maps.SerializableLocation;
import link.ignyte.utils.JsonSingleton.JsonPath;
import link.ignyte.utils.JsonSingleton.JsonSerializable;
import org.bukkit.ChatColor;

@JsonPath("options.json")
public class Options extends JsonSerializable {

    public static final int CURRENT_OPTIONS_VERSION = 1;
    @Expose
    public final String network_name;
    @Expose
    public final String game_name;
    @Expose
    public final AutoMode auto_mode;
    @Expose
    public int version;
    @SerializedName(value = "developer_mode", alternate = {"dev_version", "dev_mode"})
    @Expose
    public boolean dev_mode;
    @Expose
    public SerializableLocation spawn;
    @Expose
    public int spawn_worldborder_size;
    @Expose
    public int lobby_time;
    @Expose
    public boolean put_spectators_into_teams;
    @Expose
    public boolean put_dead_players_into_teams;


    public Options(boolean generateDefaults) {
        put_dead_players_into_teams = false;
        put_spectators_into_teams = true;
        version = 0;
        spawn_worldborder_size = 150;
        lobby_time = 12500;
        spawn = new SerializableLocation(0.5, 64.815, 0.5, 90, 0);
        dev_mode = false;
        network_name = "Configure me in options.json!";
        game_name = "&cSome version of bedwars (unconfigured)";
        auto_mode = new AutoMode();
    }

    public String getGameNameNoColors() {
        return ChatColor.stripColor(MessageCreator.t(game_name)).replace(' ', '_');
    }

}
