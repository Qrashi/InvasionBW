package link.ignyte.plugins.bedwars.Utils.error;

public enum ErrorSeverity {
    LOWEST,
    LOW,
    MIDDLE,
    HIGH,
    ALARM
}
