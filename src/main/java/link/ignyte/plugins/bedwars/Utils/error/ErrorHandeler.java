package link.ignyte.plugins.bedwars.Utils.error;

import link.ignyte.plugins.bedwars.BedWars;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import org.bukkit.Bukkit;

public class ErrorHandeler {

    public static void error(ErrorCode code) {
        ErrorSeverity severity = code.getSeverity();
        String message = "ERROR: CODE: " + code + " SEVERITY: " + severity.toString() + "\nERROR: " + getAdvice(severity);
        switch (severity) {
            case LOWEST -> BedWars.getLogging().info(message);
            case LOW -> BedWars.getLogging().warning(message);
            case MIDDLE -> {
                BedWars.getLogging().warning(message);
                Bukkit.broadcastMessage(MessageCreator.t("&cAn error has been detected. Please read the command line"));
                Bukkit.broadcastMessage(getAdvice(severity));
            }
            case HIGH -> {
                BedWars.getLogging().severe(message);
                Bukkit.broadcastMessage("&cAn error occured while attempting to perform this action.\n&cDetailed error information found in console.");
                Bukkit.broadcastMessage(MessageCreator.t("&cBedWars may be broken. Try reloading it using &a/endgame"));
            }
            case ALARM -> {
                BedWars.getLogging().severe(message);
                Bukkit.broadcastMessage(MessageCreator.t("&c" + message));
                Bukkit.broadcastMessage(MessageCreator.t("&cBedWars will likely be broken. Try reloading it using &a/endgame"));
            }
        }
    }

    private static String getAdvice(ErrorSeverity severity) {
        return switch (severity) {
            case LOWEST -> "This error is very small and can probably be ignored.";
            case LOW -> "There has been an error that might already affect gameplay in slight ways.";
            case MIDDLE -> "This error has impacted the game in some way, but it is not disabling a major feature.";
            case HIGH -> "There has been a severe error, the gameplay was affected and there may be some major problems.";
            case ALARM -> "This is a very rare and very difficult error, it WILL disable an important feature / disable major aspects of the gameplay!";
        };
    }
}
