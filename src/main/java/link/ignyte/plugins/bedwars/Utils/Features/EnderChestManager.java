package link.ignyte.plugins.bedwars.Utils.Features;

import link.ignyte.plugins.bedwars.Inventories.InvOpener;
import link.ignyte.plugins.bedwars.Management.GameManager;
import link.ignyte.plugins.bedwars.Maps.Teams.Team;
import link.ignyte.plugins.bedwars.Players.PlayerData;
import link.ignyte.plugins.bedwars.Players.PlayerDataManager;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;

import java.util.HashMap;

public class EnderChestManager {

    private static final HashMap<Team, Inventory> storage = new HashMap<>();

    public static void start() {
        storage.clear();
        for (Team team : GameManager.getMap().teamManager.teams) {
            storage.put(team, Bukkit.createInventory(null, InventoryType.CHEST, MessageCreator.t("&7TeamChest of " + team.col.teamName)));
        }
    }

    public static void open(Player player) {
        PlayerData data = PlayerDataManager.getData(player);
        if (data.getTeam() != null) {
            InvOpener.openDelay(player, storage.get(data.getTeam()));
        }
    }
}
