package link.ignyte.plugins.bedwars.Utils.error;

public enum ErrorEffect {
    CORE,
    BUILDING,
    MAP_FEATURES,
    PREPARATION
}
