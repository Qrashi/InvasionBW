package link.ignyte.plugins.bedwars.Utils.Features;

import link.ignyte.plugins.bedwars.BedWars;
import link.ignyte.plugins.bedwars.Maps.SerializableLocation;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class RescuePlattformManager {

    private static final HashMap<Player, Boolean> isAvialible = new HashMap<>();
    private static final List<BlockFace> faces = Arrays.asList(BlockFace.EAST, BlockFace.NORTH, BlockFace.SOUTH, BlockFace.WEST);

    private static boolean get(Player player) {
        if (isAvialible.get(player) == null) {
            return true;
        } else {
            return isAvialible.get(player);
        }
    }

    private static void set(Player player, boolean toSet) {
        if (isAvialible.get(player) == null) {
            isAvialible.put(player, toSet);
        } else {
            isAvialible.put(player, toSet);
        }
    }

    public static void execute(Player player, ItemStack holding) {
        if (get(player)) {
            set(player, false);
            new BukkitRunnable() {
                @Override
                public void run() {
                    set(player, true);
                    send(MessageCreator.t("&aRescue Platform can be used again!"), player);
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 1);
                }
            }.runTaskLater(BedWars.getInstance(), 100);
            SerializableLocation ploc = new SerializableLocation(player).add(0, -1, 0);
            Block mainBlock = ploc.getBlock();
            int filled = 0;
            if (!mainBlock.getType().toString().endsWith("AIR")) filled = 100;
            for (BlockFace face : faces) {
                if (!mainBlock.getRelative(face).getType().toString().endsWith("AIR")) filled++;
                if (!mainBlock.getRelative(face).getRelative(face).getType().toString().endsWith("AIR")) filled++;
                if (!mainBlock.getRelative(getNextDir(face)).getRelative(face).getType().toString().endsWith("AIR"))
                    filled++;
                if (!mainBlock.getRelative(getNextDir(face)).getRelative(face).getRelative(face).getType().toString().endsWith("AIR"))
                    filled++;
                if (!mainBlock.getRelative(getPrevDir(face)).getRelative(face).getType().toString().endsWith("AIR"))
                    filled++;
                if (!mainBlock.getRelative(getPrevDir(face)).getRelative(face).getRelative(face).getType().toString().endsWith("AIR"))
                    filled++;
            }
            if (!mainBlock.getRelative(BlockFace.UP).getType().toString().endsWith("AIR")) filled = 100;
            if (!mainBlock.getRelative(BlockFace.UP).getRelative(BlockFace.UP).getType().toString().endsWith("AIR"))
                filled = 100;
            if (filled > 6) {
                player.sendMessage(MessageCreator.t("&cYou can't use this item here!"));
            } else {
                if (mainBlock.getType().toString().endsWith("AIR")) mainBlock.setType(Material.SMOOTH_SANDSTONE);
                for (BlockFace face : faces) {
                    Block checking = mainBlock.getRelative(face);
                    if (checking.getType().toString().endsWith("AIR")) checking.setType(Material.SMOOTH_SANDSTONE);
                    checking = mainBlock.getRelative(face).getRelative(face);
                    if (checking.getType().toString().endsWith("AIR")) checking.setType(Material.SMOOTH_SANDSTONE);
                    checking = mainBlock.getRelative(getNextDir(face)).getRelative(face);
                    if (checking.getType().toString().endsWith("AIR")) checking.setType(Material.SMOOTH_SANDSTONE);
                    checking = mainBlock.getRelative(getNextDir(face)).getRelative(face).getRelative(face);
                    if (checking.getType().toString().endsWith("AIR")) checking.setType(Material.SMOOTH_SANDSTONE);
                    checking = mainBlock.getRelative(getPrevDir(face)).getRelative(face);
                    if (checking.getType().toString().endsWith("AIR")) checking.setType(Material.SMOOTH_SANDSTONE);
                    checking = mainBlock.getRelative(getPrevDir(face)).getRelative(face).getRelative(face);
                    if (checking.getType().toString().endsWith("AIR")) checking.setType(Material.SMOOTH_SANDSTONE);
                }
                holding.setAmount(holding.getAmount() - 1);
                player.sendMessage(MessageCreator.t("&aSaved you!"));
                player.teleport(new SerializableLocation(mainBlock).getCenter().add(0, 1, 0).setDirection(player.getLocation().getDirection()));
                player.setFallDistance(0);
            }
        } else {
            player.playSound(player.getLocation(), Sound.ITEM_SHIELD_BREAK, 1, 1);
            send(MessageCreator.t("&cPlease wait 5 seconds to use this item again"), player);
        }

    }

    private static BlockFace getNextDir(BlockFace face) {
        return switch (face) {
            case SOUTH -> BlockFace.WEST;
            case WEST -> BlockFace.NORTH;
            case NORTH -> BlockFace.EAST;
            case EAST -> BlockFace.SOUTH;
            default -> null;
        };
    }

    private static BlockFace getPrevDir(BlockFace face) {
        return switch (face) {
            case SOUTH -> BlockFace.EAST;
            case WEST -> BlockFace.SOUTH;
            case NORTH -> BlockFace.WEST;
            case EAST -> BlockFace.NORTH;
            default -> null;
        };
    }

    public static void send(String bar, Player toSendTo) {
        TextComponent toSend = MessageCreator.generateComponent(bar);
        toSendTo.spigot().sendMessage(ChatMessageType.ACTION_BAR, toSend);
    }

}
