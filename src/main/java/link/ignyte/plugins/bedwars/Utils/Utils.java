package link.ignyte.plugins.bedwars.Utils;

import link.ignyte.plugins.bedwars.BedWars;
import link.ignyte.plugins.bedwars.Inventories.ItemStacks;
import link.ignyte.plugins.bedwars.Maps.Spawners.SpawnerType;
import link.ignyte.plugins.bedwars.Maps.Teams.TeamColor;
import link.ignyte.plugins.bedwars.Utils.error.*;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.FireworkMeta;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public final class Utils {
    private static final List<Material> armor = Arrays.asList(Material.LEATHER_BOOTS, Material.LEATHER_HELMET, Material.LEATHER_LEGGINGS, Material.CHAINMAIL_CHESTPLATE, Material.GOLDEN_BOOTS);
    private static final List<Material> beds = Arrays.asList(Material.BLACK_BED, Material.BLUE_BED, Material.RED_BED, Material.CYAN_BED, Material.BROWN_BED, Material.YELLOW_BED, Material.GREEN_BED, Material.LIGHT_BLUE_BED, Material.LIGHT_GRAY_BED, Material.GRAY_BED, Material.PURPLE_BED, Material.MAGENTA_BED, Material.ORANGE_BED, Material.WHITE_BED, Material.RED_BED, Material.PINK_BED);

    private static final List<Material> materialList = Arrays.asList(Material.LADDER, Material.CAKE, Material.SMOOTH_SANDSTONE, Material.COBWEB, Material.CHEST, Material.ENDER_CHEST, Material.TNT, Material.END_STONE, Material.IRON_BLOCK, Material.RED_BED);
    private static final List<Color> colors = Arrays.asList(Color.WHITE, Color.PURPLE, Color.RED, Color.GREEN, Color.AQUA, Color.BLUE, Color.BLUE, Color.FUCHSIA, Color.GRAY, Color.GRAY, Color.LIME, Color.MAROON, Color.YELLOW, Color.SILVER, Color.TEAL, Color.ORANGE, Color.OLIVE, Color.NAVY, Color.BLACK);

    public static boolean isBed(Material mat) {
        return beds.contains(mat);
    }

    public static int getMax_teams() {
        return TeamColor.values().length;
    }

    public static List<Material> getMaterialList() {
        return materialList;
    }

    public static void setArmor(Material material, ItemStack toSet, PlayerInventory toAddTo) {

        switch (material) {
            case GOLDEN_BOOTS, LEATHER_BOOTS -> {
                if (toAddTo.getBoots() != null) {
                    if (toAddTo.getBoots().getType() != Material.AIR) {
                        toAddTo.addItem(toSet);
                        return;
                    }
                }
                toAddTo.setBoots(toSet);
            }
            case CHAINMAIL_CHESTPLATE -> {
                if (toAddTo.getChestplate() != null) {
                    if (toAddTo.getChestplate().getType() != Material.AIR) {
                        toAddTo.addItem(toSet);
                        return;
                    }
                }
                toAddTo.setChestplate(toSet);
            }
            case LEATHER_LEGGINGS -> {
                if (toAddTo.getLeggings() != null) {
                    if (toAddTo.getLeggings().getType() != Material.AIR) {
                        toAddTo.addItem(toSet);
                        return;
                    }
                }
                toAddTo.setLeggings(toSet);
            }
            case LEATHER_HELMET -> {
                if (toAddTo.getHelmet() != null) {
                    if (toAddTo.getHelmet().getType() != Material.AIR) {
                        toAddTo.addItem(toSet);
                        return;
                    }
                }
                toAddTo.setHelmet(toSet);
            }
        }
    }

    public static String getSpawnMaterialName(SpawnerType type) {
        switch (type) {
            case BRONZE:
                return "&cBronze";
            case IRON:
                return "&fIron";
            case GOLD:
                return "&6Gold";
            default:
                ErrorHandeler.error(new ErrorCode(ErrorEffect.MAP_FEATURES, ErrorSeverity.MIDDLE, ErrorRelation.ITEM, "Utils:MaterialName:NotKnown"));
                return "&4ERR_STYPE_NULL";
        }
    }

    public static void spawnFirework(Player player) {
        Firework fw = (Firework) BedWars.getWorld().spawnEntity(player.getLocation(), EntityType.FIREWORK);
        FireworkMeta meta = fw.getFireworkMeta();
        Random random = new Random();
        FireworkEffect.Type type = FireworkEffect.Type.BALL;
        int random_effect = random.nextInt(5) + 1;
        switch (random_effect) {
            //case 1 = already assigned
            case 2 -> type = FireworkEffect.Type.BALL_LARGE;
            case 3 -> type = FireworkEffect.Type.BURST;
            case 4 -> type = FireworkEffect.Type.CREEPER;
            case 5 -> type = FireworkEffect.Type.STAR;
        }
        int size = colors.size();
        Color c1 = colors.get(random.nextInt(size));
        Color c2 = colors.get(random.nextInt(size));

        FireworkEffect effect = FireworkEffect.builder().with(type).flicker(random.nextBoolean()).withColor(c1).withFade(c2).trail(random.nextBoolean()).build();
        meta.addEffect(effect);

        meta.setPower(1);
        fw.setFireworkMeta(meta);
    }

    public static TeamColor numToCol(int num) {
        return switch (num) {
            case 1 -> TeamColor.CYAN;
            case 2 -> TeamColor.GOLD;
            case 3 -> TeamColor.WHITE;
            case 4 -> TeamColor.BLACK;
            case 5 -> TeamColor.PINK;
            case 6 -> TeamColor.RED;
            case 7 -> TeamColor.GREEN;
            case 8 -> TeamColor.YELLOW;
            default -> TeamColor.BLUE;
        };
    }

    public static List<Material> getArmorMaterials() {
        return armor;
    }

    public static Material spawnerTypeToMaterial(SpawnerType type) {
        return switch (type) {
            case GOLD -> Material.GOLD_INGOT;
            case IRON -> Material.IRON_INGOT;
            case BRONZE -> Material.BRICK;
        };
    }

    public static ItemStack getItemFromType(SpawnerType type) {
        return switch (type) {
            case GOLD -> ItemStacks.gold;
            case IRON -> ItemStacks.iron;
            case BRONZE -> ItemStacks.bronze;
        };
    }
}
