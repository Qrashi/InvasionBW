package link.ignyte.plugins.bedwars.Utils;

import link.ignyte.plugins.bedwars.BedWars;
import org.bukkit.NamespacedKey;
import org.bukkit.plugin.Plugin;

public class KeyAssistant {

    private static final Plugin instance = BedWars.getInstance();
    private static final String no_col_name = MessageCreator.gameNameNoColors();

    public static NamespacedKey getKey(String key) {
        return new NamespacedKey(instance, "BedWars/" + key);
    }

}
