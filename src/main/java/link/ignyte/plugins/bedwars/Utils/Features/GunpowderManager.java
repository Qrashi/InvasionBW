package link.ignyte.plugins.bedwars.Utils.Features;

import link.ignyte.plugins.bedwars.BedWars;
import link.ignyte.plugins.bedwars.Management.GameManager;
import link.ignyte.plugins.bedwars.Players.PlayerData;
import link.ignyte.plugins.bedwars.Players.PlayerDataManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

public class GunpowderManager {

    public static void onClick(Player player, ItemStack stack) {
        PlayerData data = PlayerDataManager.getData(player);
        if (data.getTeam() == null) return;
        Location loc = player.getLocation();
        ItemStack og = stack.clone();
        stack.setAmount(stack.getAmount() - 1);
        og.setType(Material.GLOWSTONE_DUST);
        og.setAmount(1);
        player.getInventory().addItem(og);
        final int[] secs = {6};
        player.setLevel(secs[0]);
        new BukkitRunnable() {
            @Override
            public void run() {
                if (player.isOnline()) {
                    if (player.getLocation().distance(loc) < 0.5) {
                        if (secs[0] == 1) {
                            Vector dir = GameManager.getMap().bbox.getMiddle().getLocation().add(0, data.getTeam().getSpawn().getY(), 0).toVector().subtract(data.getTeam().getSpawn().getLocationNoYawPitch().toVector());
                            player.teleport(data.getTeam().getSpawn().getCenter().setDirection(dir));
                            playSound(Sound.ENTITY_PLAYER_LEVELUP, 2);
                            player.getInventory().clear(player.getInventory().first(Material.GLOWSTONE_DUST));
                            cancel();
                        } else {
                            secs[0]--;
                            player.setLevel(secs[0]);
                            playSound(Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 0.1);
                            return;
                        }
                    }
                    player.setLevel(0);
                    player.playSound(player.getLocation(), Sound.ITEM_SHIELD_BREAK, 1, 1);
                    player.getInventory().clear(player.getInventory().first(Material.GLOWSTONE_DUST));
                    stack.setAmount(stack.getAmount() + 1);
                    cancel();
                }
            }
        }.runTaskTimer(BedWars.getInstance(), 0, 20);
    }

    private static void playSound(Sound toPlay, double pitch) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            player.playSound(player.getLocation(), toPlay, 1, (float) pitch);
        }
    }
}
