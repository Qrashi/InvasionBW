package link.ignyte.plugins.bedwars.Maps;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import link.ignyte.plugins.bedwars.Maps.Spawners.Spawner;
import link.ignyte.plugins.bedwars.Maps.Spawners.SpawnerType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LocationSaver {

    @Expose
    private final ArrayList<Spawner> spawners = new ArrayList<>();
    @SerializedName(value = "spectator_spawn", alternate = {"specspawn"})
    @Expose
    public SerializableLocation spectator_spawn;
    @SerializedName(value = "build_mode_spawn", alternate = {"lobbyspawn"})
    @Expose
    public SerializableLocation build_mode_spawn;

    public LocationSaver(SerializableLocation startloc) {
        spectator_spawn = startloc;
        build_mode_spawn = startloc;
    }


    public void addSpawner(SpawnerType type, SerializableLocation loc) {
        spawners.add(new Spawner(type, loc));

    }

    public List<Spawner> getSpawners() {
        return spawners;
    }

    public List<Spawner> getSpawners(SpawnerType type) {
        List<Spawner> toReturn = new ArrayList<>(Collections.emptyList());
        for (Spawner spawner : spawners) {
            if (spawner.getType() == type) toReturn.add(spawner);
        }
        return toReturn;
    }

    public SerializableLocation getBuild_mode_spawn() {
        return build_mode_spawn;
    }

    public void setBuild_mode_spawn(SerializableLocation build_mode_spawn) {
        this.build_mode_spawn = build_mode_spawn;
    }

    public Spawner getSpawnerAt(SerializableLocation toSearch) {
        for (Spawner spawner : spawners) {
            if (spawner.getLoc().equals(toSearch)) {
                return spawner;
            }
        }
        return null;
    }

    public SerializableLocation getSpectator_spawn() {
        return spectator_spawn;
    }

    public void setSpectator_spawn(SerializableLocation spectator_spawn) {
        this.spectator_spawn = spectator_spawn;
    }

    public void removeSpawner(Spawner spawner) {
        spawners.remove(spawner);
    }
}
