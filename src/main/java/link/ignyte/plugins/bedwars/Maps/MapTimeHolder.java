package link.ignyte.plugins.bedwars.Maps;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import link.ignyte.plugins.bedwars.BedWars;
import link.ignyte.plugins.bedwars.Management.ScoreboardManager;

public class MapTimeHolder {
    @SerializedName(value = "map_time", alternate = {"time"})
    @Expose
    private int time;

    public MapTimeHolder() {
        time = 0;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int toSet) {
        ScoreboardManager.now();
        time = toSet;
    }

    public void set() {
        BedWars.getWorld().setTime(time);
    }
}
