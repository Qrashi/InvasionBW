package link.ignyte.plugins.bedwars.Maps.Teams;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import link.ignyte.plugins.bedwars.BedWars;
import link.ignyte.plugins.bedwars.Management.ScoreboardManager;
import link.ignyte.plugins.bedwars.Maps.SerializableLocation;
import link.ignyte.plugins.bedwars.Players.PeresistentPlayerData;
import link.ignyte.plugins.bedwars.Players.PlayerData;
import link.ignyte.plugins.bedwars.Players.PlayerDataManager;
import link.ignyte.plugins.bedwars.Utils.Advancements;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.type.Bed;
import org.bukkit.entity.Player;

public class RespawnBlock {

    @Expose
    private final BlockFace face;
    @SerializedName(value = "location", alternate = {"loc"})
    @Expose
    public SerializableLocation loc;
    @Expose(serialize = false)
    public boolean destroyed;

    public RespawnBlock(SerializableLocation location, BlockFace face) {
        loc = location;
        this.face = face;
    }

    public void destroy(TeamColor team, Player destroyer) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            player.playSound(player.getLocation(), Sound.ENTITY_WITHER_DEATH, 100000000, 1);
            if (PlayerData.get(player).getTeam() == null) continue;
            if (PlayerDataManager.getData(player).getTeam().col != team) continue;
            MessageCreator.sendTitle(player, "&cYour bed was destroyed", "&7You can no longer respawn", 60);
        }
        MessageCreator.sendWithPrefix("&cThe bed of " + team.teamName + " &7was &cdestroyed by " + destroyer.getName() + "!");
        loc.getBlock().getRelative(face).setType(Material.AIR);
        loc.getBlock().setType(Material.AIR);
        destroyed = true;

        PeresistentPlayerData.PeresistentData peresistentData = PeresistentPlayerData.get(destroyer);
        if (peresistentData.beds_destroyed == 0) {
            Advancements.award(destroyer, "bedwars/root/play/destroy_bed");
        } else if (peresistentData.beds_destroyed == 99) {
            Advancements.award(destroyer, "bedwars/root/play/destroy_bed/100");
        }
        peresistentData.beds_destroyed++;
        ScoreboardManager.now();
    }

    public void destroy(TeamColor team, String reason) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            //player.playSound(player.getLocation(), Sound.ENTITY_WITHER_DEATH, 100000000, 1);
            if (PlayerData.get(player).getTeam() == null) continue;
            if (PlayerDataManager.getData(player).getTeam().col != team) continue;
            MessageCreator.sendTitle(player, "&cYour bed was destroyed", "&7You can no longer respawn", 60);
        }
        MessageCreator.sendWithPrefix("&cThe bed of " + team.teamName + " &7was &cdestroyed because " + reason + "!");
        loc.getBlock().getRelative(face).setType(Material.AIR, false);
        loc.getBlock().setType(Material.AIR, false);
        destroyed = true;
        ScoreboardManager.now();
    }

    public String getStatus() {
        if (destroyed) {
            return "&c⨯";
        }
        return "&a✓";
    }

    public boolean getDestroyed() {
        return destroyed;
    }

    public void setSpawn(SerializableLocation loc) {
        this.loc = loc;
    }

    public SerializableLocation getLoc() {
        return loc;
    }

    public void revive() {
        destroyed = false;
        BedWars.getWorld().getBlockAt(loc.getLocationNoYawPitch()).setType(Material.RED_BED);
        ScoreboardManager.now();
    }

    public BlockFace getFace() {
        return face;
    }

    public void redo() {
        if (face == null) return;
        Block block = loc.getBlock().getRelative(face);
        for (Bed.Part part : Bed.Part.values()) {
            block.setBlockData(Bukkit.createBlockData(Material.RED_BED, (data) -> {
                ((Bed) data).setPart(part);
                ((Bed) data).setFacing(face);
            }));
            block = block.getRelative(face.getOppositeFace());
        }
    }

    public void reset() {
        destroyed = false;
    }

}
