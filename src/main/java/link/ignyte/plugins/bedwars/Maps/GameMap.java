package link.ignyte.plugins.bedwars.Maps;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import link.ignyte.plugins.bedwars.Maps.Teams.RespawnBlock;
import link.ignyte.plugins.bedwars.Maps.Teams.Team;
import link.ignyte.plugins.bedwars.Maps.Teams.TeamManager;
import link.ignyte.plugins.bedwars.Players.PlayerDataManager;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import link.ignyte.plugins.bedwars.Utils.Utils;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class GameMap {
    @SerializedName(value = "teams", alternate = {"teamManager"})
    @Expose
    public final TeamManager teamManager;
    @SerializedName(value = "bounding_box", alternate = {"bbox"})
    @Expose
    public BoundingBox bbox;
    @Expose
    public String name;
    @Expose
    public boolean available;
    @SerializedName(value = "locations", alternate = {"locs"})
    @Expose
    public LocationSaver locs;
    @SerializedName(value = "time", alternate = {"timeHolder"})
    @Expose
    public MapTimeHolder timeHolder;
    @Expose(serialize = false)
    private ClearState clear; // Deprecated!

    public GameMap(String name, int teamNum, BoundingBox box, SerializableLocation toStart) {
        this.name = name;
        this.bbox = box;
        List<Team> teamList = new ArrayList<>();
        IntStream.range(0, teamNum).forEachOrdered(n -> teamList.add(new Team(Utils.numToCol(n))));
        this.teamManager = new TeamManager(teamList);
        this.available = false;
        locs = new LocationSaver(toStart);
        timeHolder = new MapTimeHolder();
    }

    public boolean destroyRespawnBlockThere(Block block, Player player) {
        SerializableLocation toCheck = new SerializableLocation(block);
        Team playerTeam = PlayerDataManager.getData(player).getTeam();
        for (Team team : teamManager.getTeamList()) {
            RespawnBlock respawnBlock = team.getRespawnBlock();
            if (respawnBlock.getLoc().equals(toCheck)) {
                if (!team.equals(playerTeam)) {
                    respawnBlock.destroy(team.col, player);
                } else {
                    player.sendMessage(MessageCreator.withPrefix("&cYou can't destroy your own bed!"));
                }
                return true;
            }
            if (new SerializableLocation(respawnBlock.getLoc().getBlock().getRelative(respawnBlock.getFace())).equals(toCheck)) {
                if (!team.equals(playerTeam)) {
                    respawnBlock.destroy(team.col, player);
                } else {
                    player.sendMessage(MessageCreator.withPrefix("&cYou can't destroy your own bed!"));
                }
                return true;
            }
        }
        return false;
    }
}
