package link.ignyte.plugins.bedwars.Maps.Teams;

import com.google.gson.annotations.Expose;
import link.ignyte.plugins.bedwars.Management.ScoreboardManager;
import link.ignyte.plugins.bedwars.Players.PlayerData;
import link.ignyte.plugins.bedwars.Players.PlayerManager;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import link.ignyte.plugins.bedwars.Utils.Options;
import link.ignyte.plugins.bedwars.Utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.*;
import java.util.stream.IntStream;

public class TeamManager {

    @Expose
    public List<Team> teams;
    @Expose
    public int team_size;
    private HashMap<TeamColor, Team> teamColorStorage;
    private Queue<Player> teamJoinQueue;

    public TeamManager(List<Team> teamList) {
        teams = teamList;
    }


    public void init() {
        // Wont be called after reload or is not even called in the first place :(
        for (Team team : teams) {
            team.init();
        }
        teamJoinQueue = new PriorityQueue<>();
        initTeamColorStorage();
    }

    private void initTeamColorStorage() {
        teamColorStorage = new HashMap<>();
        teams.forEach(team -> teamColorStorage.put(team.getCol(), team));
    }

    public List<Team> save() {
        return new ArrayList<>(teams);
    }

    public Team getTeamByColor(TeamColor toSearch) {
        return teamColorStorage.get(toSearch);
    }

    public int getNumberOfTeams() {
        return teams.size();
    }

    public void setNumberOfTeams(int teamNum) {
        teams.clear();
        teamNum = Math.min(teamNum, Utils.getMax_teams());
        IntStream.range(0, teamNum).forEachOrdered(n -> teams.add(new Team(Utils.numToCol(n))));
        initTeamColorStorage();
        ScoreboardManager.now();
    }

    public int getTeamSize() {
        return team_size;
    }

    public void setTeamSize(int newSize) {
        this.team_size = newSize;
        ScoreboardManager.now();
    }

    public List<Team> getTeamList() {
        return teams;
    }

    public void redoBeds() {
        for (Team team : teams) {
            if (team.getRespawnBlock() == null) return;
            team.getRespawnBlock().redo();
        }
    }

    public boolean putInJoinQueue(Player player) {
        if (!Options.get(Options.class).put_spectators_into_teams) return false;
        if (teamJoinQueue.contains(player)) return false;
        if (PlayerData.get(player).played && !Options.get(Options.class).put_dead_players_into_teams)
            return false;
        if (teamJoinQueue.size() == 0) {
            // Join queue is empty so recalculate possible positions
            for (Team team : teams) {
                if (!team.respawnBlock.destroyed) {
                    // CAN join
                    if (team.memberCount() != team_size) {
                        // Can join
                        join(team, player);
                        MessageCreator.sendTitle(player, "&aYou joined " + team.col.teamName, "", 60);
                        Bukkit.broadcastMessage(MessageCreator.withPrefix(player.getName() + "&a joined " + team.col.teamName + "!"));
                        PlayerManager.fixPlayer(player, false);
                        return true;
                    }
                }
            }
        } else {
            teamJoinQueue.add(player);
        }
        return false;
    }

    public void spaceFree(Team team) {
        if (!Options.get(Options.class).put_spectators_into_teams) return;
        if (teamJoinQueue.size() != 0) {
            Player next = teamJoinQueue.poll();
            team.join(next);
            MessageCreator.sendTitle(next, "&aYou joined " + team.col.teamName, "", 60);
            Bukkit.broadcastMessage(MessageCreator.withPrefix(next.getName() + "&a joined " + team.col.teamName + "!"));
            PlayerManager.fixPlayer(next, false);
        }
    }

    public void endTeam(Team team) {
        team.members().forEach(player -> PlayerData.get(player).setTeam(null));
        team.members().clear();
    }

    public void join(Team team, Player player) {
        PlayerData.get(player).setTeam(team);
        team.join(player);
        player.sendMessage(ChatColor.GREEN + "You joined team " + team.col.teamName);
    }

    public void leave(Player player) {
        PlayerData.get(player).setTeam(null);
        teams.forEach(team -> team.leave(player));
    }

}
