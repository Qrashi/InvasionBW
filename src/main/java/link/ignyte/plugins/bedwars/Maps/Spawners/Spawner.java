package link.ignyte.plugins.bedwars.Maps.Spawners;

import com.google.gson.annotations.Expose;
import link.ignyte.plugins.bedwars.BedWars;
import link.ignyte.plugins.bedwars.Maps.SerializableLocation;
import link.ignyte.plugins.bedwars.Utils.Utils;

import java.util.Objects;

public class Spawner {
    @Expose
    private SpawnerType type;
    @Expose
    private SerializableLocation loc;

    public Spawner(SpawnerType type, SerializableLocation loc) {
        this.type = type;
        this.loc = loc;
    }

    public Spawner(SpawnerType type, int x, int y, int z) {
        this.type = type;
        loc = new SerializableLocation(x, y, z);
    }

    public void spawn() {
        BedWars.getWorld().dropItem(loc.getTpLocation(), Objects.requireNonNull(Utils.getItemFromType(type)));
    }

    public SerializableLocation getLoc() {
        return loc;
    }

    public void setLoc(SerializableLocation loc) {
        this.loc = loc;
    }

    public SpawnerType getType() {
        return type;
    }

    public void setType(SpawnerType type) {
        this.type = type;
    }
}
