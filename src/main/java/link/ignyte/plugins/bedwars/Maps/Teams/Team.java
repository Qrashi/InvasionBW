package link.ignyte.plugins.bedwars.Maps.Teams;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import link.ignyte.plugins.bedwars.Management.GameManager;
import link.ignyte.plugins.bedwars.Management.ScoreboardManager;
import link.ignyte.plugins.bedwars.Management.StatusMessenger;
import link.ignyte.plugins.bedwars.Maps.SerializableLocation;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class Team {


    /*
        Team general idea
        If respawn block is present - offline players are ALLOWED and can rejoin.
        If respawn block is destroyed - all offline players get removed!
    */

    @SerializedName(value = "colors", alternate = {"col"})
    @Expose
    public final TeamColor col;
    @Deprecated
    @Expose(serialize = false)
    public int teamSize;
    @SerializedName(value = "bed", alternate = {"respawnBlock"})
    @Expose
    public RespawnBlock respawnBlock;
    @SerializedName(value = "spawn", alternate = {"spawnLoc"})
    @Expose
    public SerializableLocation spawnLoc = null;
    private ArrayList<Player> playerList = new ArrayList<>();

    public Team(TeamColor color, SerializableLocation spawn) {
        col = color;
        spawnLoc = spawn;
    }

    public Team(TeamColor teamColor) {
        col = teamColor;
    }

    public ArrayList<String> memberList() {
        ArrayList<String> members = new ArrayList<>();
        playerList.forEach(player -> members.add("> " + player.getName()));
        return members;
    }

    public ArrayList<Player> members() {
        return playerList;
    }

    public TeamColor getCol() {
        return col;
    }


    public void updateBed(RespawnBlock respawnBlock) {
        this.respawnBlock = respawnBlock;
    }

    public RespawnBlock getRespawnBlock() {
        return respawnBlock;
    }

    public SerializableLocation getSpawn() {
        return spawnLoc;
    }

    public void setSpawn(SerializableLocation newloc) {
        spawnLoc = newloc;
    }

    public int freeSpace() {
        return GameManager.getMap().teamManager.getNumberOfTeams() - memberCount();
    }

    public int memberCount() {
        return playerList.size();
    }

    //Will put player into team BUT not into PlayerData
    public void join(Player player) {
        player.setDisplayName(MessageCreator.t(this.getCol().colorCode + player.getName()));
        playerList.add(player);
        ScoreboardManager.now();
        StatusMessenger.update();
    }

    public boolean hasSpace() {
        return freeSpace() > 0;
    }

    public void init() {
        playerList = new ArrayList<>();
    }

    //Will remove player from team BUT not from PlayerData
    public void leave(Player player) {
        playerList.remove(player);
        ScoreboardManager.now();
        StatusMessenger.update();
    }

}
