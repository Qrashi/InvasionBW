package link.ignyte.plugins.bedwars.Maps;

import com.google.gson.annotations.Expose;
import link.ignyte.plugins.bedwars.BedWars;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class SerializableLocation {

    @Expose
    private double x;
    @Expose
    private double y;
    @Expose
    private double z;
    @Expose
    private int yaw = 0;
    @Expose
    private int pitch = 0;

    public SerializableLocation(Location loc) {
        x = loc.getX();
        y = loc.getY();
        z = loc.getZ();
    }

    public SerializableLocation(Player ploc) {
        Location loc = ploc.getLocation();
        x = loc.getX();
        y = loc.getY();
        z = loc.getZ();
        yaw = Math.round(loc.getYaw());
        pitch = Math.round(loc.getPitch());
    }

    public SerializableLocation(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public SerializableLocation(int x, int y, int z, int yaw, int pitch) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.yaw = yaw;
        this.pitch = pitch;
    }

    public SerializableLocation(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public SerializableLocation(double x, double y, double z, int yaw, int pitch) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.yaw = yaw;
        this.pitch = pitch;
    }

    public SerializableLocation(Block block) {
        x = block.getX();
        y = block.getY();
        z = block.getZ();
    }

    public boolean isTpAble() {
        return (getBlock().getType() != Material.AIR && getCopy().add(0, 1, 0).getBlock().getType() != Material.AIR);
    }

    public SerializableLocation add(double deltaX, double deltaY, double deltaZ) {
        x = x + deltaX;
        y = y + deltaY;
        z = z + deltaZ;
        return this;
    }

    public SerializableLocation setPitch(int pitch) {
        this.pitch = pitch;
        return this;
    }

    public SerializableLocation setYaw(int yaw) {
        this.yaw = yaw;
        return this;
    }

    public void teleport(Player player) {
        player.teleport(getTpLocation());
        player.setVelocity(new Vector());
        player.setFallDistance(0);
    }

    public double getX() {
        return x;
    }

    public SerializableLocation setX(int x) {
        this.x = x;
        return this;
    }

    public SerializableLocation setX(double x) {
        this.x = x;
        return this;
    }

    public double getY() {
        return y;
    }

    public SerializableLocation setY(int y) {
        this.y = y;
        return this;
    }

    public SerializableLocation setY(double y) {
        this.y = y;
        return this;
    }

    public double getZ() {
        return z;
    }

    public SerializableLocation setZ(int z) {
        this.z = z;
        return this;
    }

    public SerializableLocation setZ(double z) {
        this.z = z;
        return this;
    }

    public Location getLocation() {
        return new Location(BedWars.getWorld(), x, y, z, yaw, pitch);
    }

    public Location getLocationNoYawPitch() {
        return new Location(BedWars.getWorld(), x, y, z);
    }

    public Location getTpLocation() {
        return new Location(BedWars.getWorld(), x, y, z, yaw, pitch);
    }

    public boolean equals(SerializableLocation loc) {
        return (x == loc.getX() && y == loc.getY() && z == loc.getZ());
    }

    public SerializableLocation getCopy() {
        return new SerializableLocation(x, y, z, yaw, pitch);
    }

    public Block getBlock() {
        return BedWars.getWorld().getBlockAt(getLocationNoYawPitch());
    }

    public Location getCenter() {
        return new Location(BedWars.getWorld(),
                getRelativeCoord(x),
                y,
                getRelativeCoord(z));
    }

    private double getRelativeCoord(double i) {
        double d = i;
        d = d < 0 ? d - .5 : d + .5;
        return d;
    }
}
