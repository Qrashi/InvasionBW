package link.ignyte.plugins.bedwars.Maps;

import com.google.gson.annotations.Expose;
import link.ignyte.plugins.bedwars.BedWars;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import link.ignyte.plugins.bedwars.Utils.Options;
import link.ignyte.plugins.bedwars.Utils.error.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;

import java.text.DecimalFormat;
import java.util.List;

import static java.lang.Math.abs;

public class BoundingBox {
    @Expose
    private final int x1;
    @Expose
    private final int y1;
    @Expose
    private final int z1;
    @Expose
    private final int x2; // xyz2 is ALWAYS BIGGER
    @Expose
    private final int y2;
    @Expose
    private final int z2;
    @Expose
    public ClearState state;


    public BoundingBox(int x1, int y1, int z1, int x2, int y2, int z2) {
        this.x2 = Math.max(x1, x2);
        this.x1 = Math.min(x1, x2);
        this.y2 = Math.max(y1, y2);
        this.y1 = Math.min(y1, y2);
        this.z2 = Math.max(z1, z2);
        this.z1 = Math.min(z1, z2);
        this.state = ClearState.NOT_CLEARED;
    }

    public BoundingBox(SerializableLocation loc1, SerializableLocation loc2) {
        this.x2 = (int) Math.max(loc1.getX(), loc2.getX());
        this.x1 = (int) Math.min(loc1.getX(), loc2.getX()); // x1 < x2!
        this.y2 = (int) Math.max(loc1.getY(), loc2.getY());
        this.y1 = (int) Math.min(loc1.getY(), loc2.getY());
        this.z2 = (int) Math.max(loc1.getZ(), loc2.getZ());
        this.z1 = (int) Math.min(loc1.getZ(), loc2.getZ());
        this.state = ClearState.NOT_CLEARED;
    }

    public BoundingBox(SerializableLocation middle, int radius, int height) {
        /*
        Returns quadratic BoundingBox
         */
        y1 = 0;
        y2 = height;
        x1 = (int) (middle.getX() - radius);
        x2 = (int) (middle.getX() + radius); // Again x1 < x2!
        z1 = (int) (middle.getZ() - radius);
        z2 = (int) (middle.getZ() + radius);
        state = ClearState.NOT_CLEARED;
    }

    /*public double getX1() {
        return this.x1;
    }
    public double getX2() {
        return this.x2;
    }
    public double getY1() {
        return this.y1;
    }
    public double getY2() {
        return this.y2;
    }
    public double getZ1() {
        return this.z1;
    }
    public double getZ2() {
        return this.z2;
    }*/

    private static void sendProgress(int goal, int current, String message) {
        int done = goal - current;
        double progress = (1 - ((double) done / (double) goal)) * 100;
        DecimalFormat formatter = new DecimalFormat("###");
        Bukkit.getOnlinePlayers().forEach(player -> MessageCreator.sendTitle(player, "&a" + formatter.format(progress) + "%", message, 60, false));
    }

    public SerializableLocation getMiddle() {
        return new SerializableLocation(((x2 - x1) / 2) + x1, 0, ((z2 - z1) / 2) + z1);
    }

    public double getMaxRadiusFromMiddle() {
        return abs(Math.max(((x2 - x1) / 2), ((z2 - z1) / 2))) + 5;
    }

    public boolean isOutside(SerializableLocation loc) {
        return ((!(x1 < loc.getX()) || !(loc.getX() < x2)) || (!(y1 < loc.getY()) || !(loc.getY() < y2)) || (!(z1 < loc.getZ()) || !(loc.getZ() < z2)));
    }

    public void setToAir(List<Material> setToAir) {
        if (state == ClearState.CLEARED) {
            MessageCreator.sendPlayersWithPermission("&aCleaning skipped!", "BedWars.dev");
            return;
        }
        if (Options.get(Options.class).dev_mode) {
            Bukkit.broadcastMessage(MessageCreator.t("&7[&bdev-mode&7] Skipped cleaning..."));
            return;
        }
        long start = System.nanoTime();
        double vol = (x2 - x1 + 1) * (y2 - y1 + 1) * (z2 - z1 + 1);
        MessageCreator.sendPlayersWithPermission("Cleaning " + vol + " blocks", "BedWars.dev");
        int cleaned = 0;
        int checked = 0;
        int diff = y2 - y1;
        Location location = new Location(BedWars.getWorld(), x1, y1, z1);
        for (int y = y1; y <= y2; y++) {
            location.setY(y);
            sendProgress(diff, y, "&7Cleaning map");
            for (int x = x1; x <= x2; x++) {
                location.setX(x);
                for (int z = z1; z <= z2; z++) {
                    location.setZ(z);
                    checked++;
                    Block block = location.getBlock();
                    if (setToAir.contains(block.getType())) {
                        block.setType(Material.AIR);
                        cleaned++;
                    }
                }

            }

        }
        long finish = System.nanoTime();
        long duration = (finish - start) / 1000000;
        if (checked != vol) {
            MessageCreator.sendWithPrefix("&cERROR: &7Checked blocks &7(" + checked + ") &cdo not align with calculated &7(" + vol + ") &cblocks!");
            ErrorHandeler.error(new ErrorCode(ErrorEffect.CORE, ErrorSeverity.HIGH, ErrorRelation.BLOCK, "bbx:clearbox_result_not_target"));
        }
        MessageCreator.sendPlayersWithPermission("&7Checked &a" + checked + "&7 blocks, cleaned &a" + cleaned + "&7 blocks, duration: " + duration + "ms. &a✓", "BedWars.dev");
        state = ClearState.CLEARED;
    }

    // BoundingBoxActions
    public int checkEmpty() {
        if (Options.get(Options.class).dev_mode) {
            Bukkit.broadcastMessage(MessageCreator.t("&7[&bdev-mode&7] Skipped CHECK_EMPTY..."));
            return 0;
        }
        long start = System.nanoTime();
        double vol = (((x2 - x1) + 1) * (y2 - y1 + 1) * ((z2 - z1) + 1));
        if (Bukkit.getOnlinePlayers().size() == 0) {
            BedWars.getInstance().reload();
            return 0;
        }
        MessageCreator.sendPlayersWithPermission("&7Checking " + vol + " blocks...", "BedWars.dev");
        int checked_blocks = 0;
        int notAir = 0;
        Location location = new Location(BedWars.getWorld(), x1, y1, z1);
        int diff = y2 - y1; //y2 > y1
        for (int y = y1; y <= y2; y++) {
            location.setY(y);
            sendProgress(diff, y, "&7Checking map contents");
            for (int x = x1; x <= x2; x++) {
                location.setX(x);
                for (int z = z1; z <= z2; z++) {
                    location.setZ(z);
                    checked_blocks++;
                    if (location.getBlock().getType() != Material.AIR) {
                        notAir++;
                    }
                }

            }
        }
        long finish = System.nanoTime();
        long duration = (finish - start) / 1000000;
        if (checked_blocks != vol) {
            MessageCreator.sendWithPrefix("&cERROR: &7Checked blocks &7(" + checked_blocks + ") &cdo not align with calculated &7(" + vol + ") &cblocks!");
            ErrorHandeler.error(new ErrorCode(ErrorEffect.MAP_FEATURES, ErrorSeverity.HIGH, ErrorRelation.BLOCK, "bbx:checkblock_result_not_target"));
        }
        MessageCreator.sendPlayersWithPermission("&7Checked &a" + checked_blocks + "&7 blocks, found &c" + notAir + "&7 non-air blocks, duration: " + duration + "ms &a✓", "BedWars.dev");
        return notAir;
    }

    public void deleteAllBlocks() {
        if (Options.get(Options.class).dev_mode) {
            Bukkit.broadcastMessage(MessageCreator.t("&7[&bdev-mode&7] Skipped deleting..."));
            return;
        }
        long start = System.nanoTime();
        double vol = (x2 - x1 + 1) * (y2 - y1 + 1) * (z2 - z1 + 1);
        MessageCreator.sendPlayersWithPermission("Deleting " + vol + " blocks...", "BedWars.dev");
        Location location = new Location(BedWars.getWorld(), x1, y1, z1);
        double times = 0;
        int diff = y2 - y1;
        for (int y = y1; y <= y2; y++) {
            location.setY(y);
            sendProgress(diff, y, "&7Cleaning map contents");
            for (int x = x1; x <= x2; x++) {
                location.setX(x);
                for (int z = z1; z <= z2; z++) {
                    location.setZ(z);
                    times++;
                    location.getBlock().setType(Material.AIR);
                }

            }
        }
        long finish = System.nanoTime();
        long duration = (finish - start) / 1000000;
        if (times != vol) {
            MessageCreator.sendWithPrefix("&cERROR: &7Deleted blocks &7(" + times + ") &cdo not align with calculated &7(" + vol + ") &cblocks!");
            ErrorHandeler.error(new ErrorCode(ErrorEffect.MAP_FEATURES, ErrorSeverity.HIGH, ErrorRelation.BLOCK, "bbx:delblock_result_not_target"));
        }
        MessageCreator.sendPlayersWithPermission("&7Deleted &a" + times + "&7 blocks, duration: " + duration + "ms. &a✓", "BedWars.dev");
    }

}
