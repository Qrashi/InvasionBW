package link.ignyte.plugins.bedwars.Maps;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import link.ignyte.plugins.bedwars.Inventories.Setup.MapSpectateManager;
import link.ignyte.plugins.bedwars.Management.GameManager;
import link.ignyte.plugins.bedwars.Management.GameState;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import link.ignyte.utils.JsonSingleton.JsonPath;
import link.ignyte.utils.JsonSingleton.JsonSerializable;
import org.bukkit.Material;

import java.util.ArrayList;
import java.util.HashMap;

@JsonPath("maps.json")
public class MapManager extends JsonSerializable {

    public static final int CURRENT_MAPS_VERSION = 1;

    public static HashMap<String, GameMap> map_name_list = new HashMap<>();

    @SerializedName(value = "maps", alternate = {"mapList"})
    @Expose
    public ArrayList<GameMap> maps;
    @Expose
    public int version;

    public MapManager(boolean generateDefaults) {
        if (generateDefaults) {
            version = CURRENT_MAPS_VERSION;
            maps = new ArrayList<>();
        }
    }

    public static void loadNames() {
        MapManager.get(MapManager.class).maps.forEach(gameMap -> map_name_list.put(gameMap.name, gameMap));
    }

    public static GameMap getMapByName(String name) {
        return map_name_list.get(name);
    }

    public static boolean exists(String name) {
        return map_name_list.containsKey(name);
    }

    public static void newMap(String name) {
        /*
        Every map has got 1000 blocks of x space and 1000 blocks in y direction.
         */
        ArrayList<GameMap> maps = get(MapManager.class).maps;
        int mapIndex = maps.size() + 2;
        SerializableLocation center = new SerializableLocation(mapIndex * 1000, 100, 0);
        BoundingBox boundingBox = new BoundingBox(center, 450, 200);
        int notAir = boundingBox.checkEmpty();
        if (notAir > 0) {
            if (notAir > 50) {
                MessageCreator.sendWithPrefix("&cError: Found " + notAir + " not air blocks in new maps location, aborting...");
                MessageCreator.sendWithPrefix("&cPlease contact an admin!");
                MessageCreator.sendWithPrefix("&aIf you want to solve this issue, search for not air blocks in this region and maybe destroy them.");
                MessageCreator.sendWithPrefix("If you don't want to destroy these blocks, you may have to move them using worldedit.");
                GameManager.setState(GameState.IN_GAME);
                MapSpectateManager.spectate(new GameMap("ERROR - read chat", 1, boundingBox, center), true);
            }
            MessageCreator.sendWithPrefix("&cError: Found " + notAir + " non-air blocks in new maps location.");
            MessageCreator.sendWithPrefix("&cIgnoring and &4&lcleaning!");
            boundingBox.deleteAllBlocks();
        }
        GameMap map = new GameMap(name, 2, boundingBox, center);
        maps.add(map);
        center.getLocationNoYawPitch().getBlock().setType(Material.DIAMOND_BLOCK);
        save(MapManager.class);
        GameManager.setGameMap(map);
    }

}
