package link.ignyte.plugins.bedwars.Maps.Teams;

import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import org.bukkit.Material;

public enum TeamColor {
    RED("&c", Material.RED_BED, Material.RED_TERRACOTTA),
    BLUE("&9", Material.BLUE_BED, Material.BLUE_TERRACOTTA),
    YELLOW("&e", Material.YELLOW_BED, Material.YELLOW_TERRACOTTA),
    GREEN("&a", Material.GREEN_BED, Material.GREEN_TERRACOTTA),
    PINK("&d", Material.PINK_BED, Material.PINK_TERRACOTTA),
    BLACK("&0", Material.BLACK_BED, Material.BLACK_TERRACOTTA),
    GOLD("&6", Material.YELLOW_BED, Material.GOLD_BLOCK),
    WHITE("&f", Material.WHITE_BED, Material.WHITE_TERRACOTTA),
    CYAN("&b", Material.CYAN_BED, Material.CYAN_TERRACOTTA),
    ;

    public final String colorCode;
    public final String teamName;
    public final Material bed;
    public final Material gui_material;

    TeamColor(final String colorCode, Material bed, Material gui_material) {
        this.colorCode = MessageCreator.t(colorCode);
        this.teamName = MessageCreator.t(colorCode + "Team " + toString().toLowerCase());
        this.bed = bed;
        this.gui_material = gui_material;
    }
}
