package link.ignyte.plugins.bedwars.Commands;

import link.ignyte.plugins.bedwars.BedWars;
import link.ignyte.plugins.bedwars.Inventories.EndInventory;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class EndCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player player) {
            player.openInventory(EndInventory.getInv());
        } else {
            if (EndInventory.isConfirmed()) {
                sender.sendMessage("Ending the game...");
                EndInventory.endGame();
            } else {
                if (args.length > 0) {
                    if (args[0].equals("confirm")) {
                        sender.sendMessage("You can end the game using endgame now. This will expire in 10 seconds");
                        EndInventory.setConfirmed(true);
                        EndInventory.active = true;
                        EndInventory.task = new BukkitRunnable() {
                            @Override
                            public void run() {
                                if (EndInventory.isConfirmed()) {
                                    EndInventory.setConfirmed(false);
                                }
                                EndInventory.active = false;
                            }
                        };
                        EndInventory.task.runTaskLater(BedWars.getInstance(), 200);
                        return true;
                    }
                }
                sender.sendMessage("Please confirm that you want to end the game using endgame confirm");
            }
        }
        return true;
    }
}
