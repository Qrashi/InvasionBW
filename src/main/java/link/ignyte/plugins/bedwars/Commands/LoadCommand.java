package link.ignyte.plugins.bedwars.Commands;

import link.ignyte.plugins.bedwars.BedWars;
import link.ignyte.plugins.bedwars.Management.GameManager;
import link.ignyte.plugins.bedwars.Management.GameState;
import link.ignyte.plugins.bedwars.Management.PlayType;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import link.ignyte.utils.JsonSingleton.GsonMode;
import link.ignyte.utils.JsonSingleton.JsonSingletons;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class LoadCommand implements CommandExecutor {

    boolean confirmed = false;
    boolean canLoad = true;

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (canLoad) {
            if (confirmed) {
                JsonSingletons.initialize(GsonMode.ONLY_EXPOSE, BedWars.getFolder());
                MessageCreator.sendWithPrefix("&aLoaded all map data and configurations.");
                if (GameManager.getState() == GameState.COUNTDOWN || GameManager.getState() == GameState.IN_GAME && GameManager.getPlayType() != PlayType.EDIT_LOBBY) {
                    for (Player player : Bukkit.getOnlinePlayers()) {
                        MessageCreator.sendTitle(player, "&cWarning", "&7Loaded maps, plugin is now unstable", 100);
                        player.sendMessage(MessageCreator.withPrefix("&cMaps were &lforceloaded&r&c. There may occur issues if you are playing / editing a map! Use /endgame to restart bedwars!"));
                    }
                }
                canLoad = false;
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        canLoad = true;
                    }
                }.runTaskLater(BedWars.getInstance(), 300);
                confirmed = false;
            } else {
                commandSender.sendMessage(MessageCreator.withPrefix("&cIn order to not overwrite internal map data you have to use the /load command again. This is just for safety."));
                confirmed = true;
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        confirmed = false;
                    }
                }.runTaskLater(BedWars.getInstance(), 300);
            }
        } else {
            commandSender.sendMessage(MessageCreator.withPrefix("&cPlease wait a bit before loading again!"));
        }
        return true;
    }
}
