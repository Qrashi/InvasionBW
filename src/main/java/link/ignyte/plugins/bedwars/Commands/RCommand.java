package link.ignyte.plugins.bedwars.Commands;

import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import link.ignyte.plugins.bedwars.Utils.Options;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (Options.get(Options.class).dev_mode) {
            for (Player player : Bukkit.getOnlinePlayers())
                MessageCreator.sendTitle(player, "", "&cReloading...", 600, false);
            Bukkit.broadcastMessage(MessageCreator.t("&cWARNING: Reloading plugins, please wait!"));
            Bukkit.reload();
        } else {
            commandSender.sendMessage(MessageCreator.t("&cPlease enable dev-mode in options.json."));
        }
        return true;
    }
}
