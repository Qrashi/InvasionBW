package link.ignyte.plugins.bedwars.Commands;

import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.hover.content.Text;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class ErrorCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (strings.length == 1) {
            commandSender.sendMessage("stuuub aa");
        } else {
            commandSender.sendMessage(MessageCreator.t("&aThanks for reporting your issue!"));
            TextComponent clickme = new TextComponent(ChatColor.translateAlternateColorCodes('&', MessageCreator.withPrefix("&7Please report any issues on GitLab &a[Click me]")));
            clickme.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://gitlab.com/Qrashi/ignyteLinkBW/issues/new"));
            clickme.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                    new Text(new ComponentBuilder("Click to open the a new issue on GitLab").color(net.md_5.bungee.api.ChatColor.GREEN).create())));
            commandSender.spigot().sendMessage(clickme);
            commandSender.sendMessage(MessageCreator.t("&aWould you like to get information on a specific error?\n&aTry /error <ERROR CODE>"));
            return true;
        }
        return true;
    }
}
