package link.ignyte.plugins.bedwars.Commands;

import link.ignyte.plugins.bedwars.BuildMode.BuildInv;
import link.ignyte.plugins.bedwars.BuildMode.BuildModeManager;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class BuildCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (BuildModeManager.isInBuild()) {
            if (commandSender instanceof Player player) {
                player.openInventory(BuildInv.getBuildInv());
            } else {
                commandSender.sendMessage("Not available in the console");
            }
            return true;
        }
        commandSender.sendMessage(MessageCreator.withPrefix("Build mode isn't running!"));
        return true;
    }
}
