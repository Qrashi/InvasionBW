package link.ignyte.plugins.bedwars.Commands;

import link.ignyte.plugins.bedwars.Maps.MapManager;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import link.ignyte.plugins.bedwars.Utils.Options;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class AutoCommand implements CommandExecutor, TabCompleter {

    private static final List<String> autocomplete = Arrays.asList("enable", "on", "disable", "off", "map", "kick", "restart");
    private static final List<String> mapcomplete = Arrays.asList("force", "random");
    private static final List<String> kickcomplete = Arrays.asList("enable", "on", "disable", "off");
    private static final List<String> restart = Arrays.asList("enable", "on", "disable", "off");

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        switch (strings.length) {
            case 1:
                switch (strings[0]) {
                    case "enable":
                    case "on":
                        Options.get(Options.class).auto_mode.setEnabled(true);
                        commandSender.sendMessage(MessageCreator.t("&aENABLED&7 auto mode (headless)"));
                        return true;
                    case "disable":
                    case "off":
                        Options.get(Options.class).auto_mode.setEnabled(false);
                        commandSender.sendMessage(MessageCreator.t("&cDISABLED&7 auto mode (headless)"));
                        return true;
                    default:
                        commandSender.sendMessage(MessageCreator.t("&cNot enough arguments!"));
                        return true;
                }
            case 2:
                switch (strings[0]) {
                    case "map":
                        if ("random".equals(strings[1])) {
                            Options.get(Options.class).auto_mode.setForceMap(false);
                            commandSender.sendMessage(MessageCreator.t("&aENABLED&7 random_map"));
                            return true;
                        }
                        commandSender.sendMessage(MessageCreator.t("&cNot enough arguments!"));
                        return true;
                    case "kick":
                        switch (strings[1]) {
                            case "enable":
                            case "on":
                                Options.get(Options.class).auto_mode.setKick(true);
                                commandSender.sendMessage(MessageCreator.t("&aENABLED&7 kick"));
                                return true;
                            case "disable":
                            case "off":
                                Options.get(Options.class).auto_mode.setKick(false);
                                commandSender.sendMessage(MessageCreator.t("&cDISABLED&7 kick"));
                                return true;
                            default:
                                commandSender.sendMessage(MessageCreator.t("&cUse enable / disable / on / off!"));
                                return true;
                        }
                    case "restart":
                        switch (strings[1]) {
                            case "enable":
                            case "on":
                                Options.get(Options.class).auto_mode.setRestart(true);
                                commandSender.sendMessage(MessageCreator.t("&aENABLED&7 restart"));
                                return true;
                            case "disable":
                            case "off":
                                Options.get(Options.class).auto_mode.setRestart(false);
                                commandSender.sendMessage(MessageCreator.t("&cDISABLED&7 restart"));
                                return true;
                            default:
                                commandSender.sendMessage(MessageCreator.t("&cUse enable / disable / on / off!"));
                                return true;
                        }
                    default:
                        commandSender.sendMessage(MessageCreator.t("&cNot enough arguments!"));
                        return true;
                }
            case 3:
                if (strings[0].equals("map") && strings[1].equals("force")) {
                    if (MapManager.exists(strings[2])) {
                        Options.get(Options.class).auto_mode.setForcedMap(strings[2]);
                        commandSender.sendMessage(MessageCreator.t("&aSet map to &7" + strings[2] + "&a!"));
                    } else {
                        commandSender.sendMessage(MessageCreator.t("&cMap not found!"));
                    }
                    return true;
                }
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] strings) {
        switch (strings.length) {
            case 1:
                return getList(autocomplete, strings[0]);
            case 2:
                switch (strings[0]) {
                    case "map":
                        return getList(mapcomplete, strings[1]);
                    case "kick":
                        return getList(kickcomplete, strings[1]);
                    case "restart":
                        return getList(restart, strings[1]);
                }
            case 3:
                if (strings[0].equals("map") && strings[1].equals("force")) {
                    return getList(new ArrayList<>(MapManager.map_name_list.keySet()), strings[2]);
                }
        }
        return Collections.singletonList("Could not find any smart completes!");
    }

    private List<String> getList(List<String> chooseFrom, String typed) {
        List<String> result = new ArrayList<>();
        for (String a : chooseFrom) {
            if (a.toLowerCase().startsWith(typed.toLowerCase())) {
                result.add(a);
            }
        }
        return result;
    }
}
