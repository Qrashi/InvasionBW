package link.ignyte.plugins.bedwars.Commands;

import link.ignyte.plugins.bedwars.Players.PlayerManager;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class FixCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (strings.length == 1) {
            Player toFix = Bukkit.getPlayer(strings[0]);
            if (toFix == null) {
                commandSender.sendMessage(MessageCreator.withPrefix("&cCould not find player " + strings[0]));
                return true;
            }
            PlayerManager.fixPlayer(toFix, false);
            commandSender.sendMessage(MessageCreator.withPrefix("&aFixed " + strings[0] + "!"));
        } else if (strings.length == 0) {
            if (commandSender instanceof Player player) {
                PlayerManager.fixPlayer(player, false);
                commandSender.sendMessage(MessageCreator.withPrefix("&aFixed you!"));
            } else commandSender.sendMessage(MessageCreator.withPrefix("&cYou can't fix the console!"));
        }
        return true;
    }
}
