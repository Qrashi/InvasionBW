package link.ignyte.plugins.bedwars.Commands;

import link.ignyte.plugins.bedwars.Management.Countdown.CountdownManager;
import link.ignyte.plugins.bedwars.Management.EndCredits;
import link.ignyte.plugins.bedwars.Management.GameManager;
import link.ignyte.plugins.bedwars.Management.GameState;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class SkipCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (GameManager.getState() == GameState.END_PHASE) {
            MessageCreator.sendWithPrefix("&cSkipping...");
            EndCredits.forceEnd();
        } else if (GameManager.getState() == GameState.COUNTDOWN) {
            if (CountdownManager.skip()) {
                commandSender.sendMessage(MessageCreator.withPrefix("&aSkipped the countdown!"));
            } else {
                commandSender.sendMessage(MessageCreator.withPrefix("&cCould not skip the countdown!"));
            }
            return true;
        }
        commandSender.sendMessage(MessageCreator.withPrefix("&cNothing to skip!"));
        return true;
    }
}
