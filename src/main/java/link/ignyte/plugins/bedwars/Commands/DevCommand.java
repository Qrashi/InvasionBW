package link.ignyte.plugins.bedwars.Commands;

import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import link.ignyte.plugins.bedwars.Utils.Options;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DevCommand implements CommandExecutor, TabCompleter {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (strings.length == 0) {
            if (Options.get(Options.class).dev_mode) {
                commandSender.sendMessage(MessageCreator.t("&7[&bDEV-MODE&7] &aDev mode is &lenabled&a. Happy coding!"));
            } else {
                commandSender.sendMessage(MessageCreator.t("&7[&bDEV-MODE&7] &cDev mode is &adisabled&c. Temporarily enable it using /dev on"));
            }
        } else if (strings.length == 1) {
            switch (strings[0]) {
                case "on" -> {
                    Options.get(Options.class).dev_mode = true;
                    commandSender.sendMessage(MessageCreator.t("&7[&bDEV-MODE&7] &aEnabled &bDEV &amode. To enable it forever use /save."));
                }
                case "off" -> {
                    Options.get(Options.class).dev_mode = false;
                    commandSender.sendMessage(MessageCreator.t("&7[&bDEV-MODE&7] &cDisabled &bDEV &amode. To disable it forever use /save."));
                }
                case "info" -> commandSender.sendMessage(MessageCreator.t("&7[&bDEV-MODE&7] &fInformation on &bDEV-MODE&f:\n \n&aDev mode is a special mode which allows the execution of &7commands &alike &b/r, /qstart &aand &cdisables the game end&a. Those commands are designed to help &cDEVELOPERS &aso if you aren't a developer you should LEAVE IT OFF!"));
                default -> commandSender.sendMessage(MessageCreator.t("&7[&bDEV-MODE&7] &cUnknown subcommand. Try /dev on | off."));
            }
        } else {
            commandSender.sendMessage(MessageCreator.t("&7[&bDEV-MODE&7] &cMax 1 argument! Try /dev on | off."));
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] strings) {
        if (strings.length == 1) {
            return getList(Arrays.asList("on", "off", "info"), strings[0]);
        }
        return null;
    }

    private List<String> getList(List<String> chooseFrom, String typed) {
        List<String> result = new ArrayList<>();
        for (String a : chooseFrom) {
            if (a.toLowerCase().startsWith(typed.toLowerCase())) {
                result.add(a);
            }
        }
        return result;
    }
}
