package link.ignyte.plugins.bedwars.Commands;

import link.ignyte.plugins.bedwars.BedWars;
import link.ignyte.plugins.bedwars.Management.Countdown.CountdownManager;
import link.ignyte.plugins.bedwars.Management.GameManager;
import link.ignyte.plugins.bedwars.Management.GameState;
import link.ignyte.plugins.bedwars.Management.PlayType;
import link.ignyte.plugins.bedwars.Maps.GameMap;
import link.ignyte.plugins.bedwars.Maps.MapManager;
import link.ignyte.plugins.bedwars.Maps.Spawners.SpawnerType;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import link.ignyte.plugins.bedwars.Utils.Options;
import link.ignyte.plugins.bedwars.Utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

public class QStartCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (Options.get(Options.class).dev_mode) {
            if (GameManager.getState() == GameState.END_PHASE) {
                commandSender.sendMessage("&7[&bdev-tools&b] &cPlease wait until the game ended to quickStart!");
                return true;
            }
            boolean available = false;
            GameMap choosen = null;
            for (GameMap map : MapManager.get(MapManager.class).maps) {
                if (map.available) {
                    choosen = map;
                    available = true;
                    break;
                }
            }
            if (available) {
                Bukkit.broadcastMessage(MessageCreator.t("&7[&bdev-tools&7] &aQuick-starting game!"));
                GameManager.setGameMap(choosen);
                choosen.teamManager.redoBeds();
                choosen.teamManager.init();
                GameManager.setPlayType(PlayType.PLAYING);
                GameManager.setState(GameState.IN_GAME);
                CountdownManager.joinAll();
                for (Player player : Bukkit.getOnlinePlayers()) {
                    player.setGameMode(GameMode.SURVIVAL);
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            for (SpawnerType type : SpawnerType.values()) {
                                ItemStack to_add = Utils.getItemFromType(type);
                                to_add.setAmount(64);
                                player.getInventory().addItem(to_add);
                            }
                        }
                    }.runTaskLater(BedWars.getInstance(), 10);
                }
                GameManager.startGame();
            } else {
                commandSender.sendMessage(MessageCreator.t("&7[&bdev-tools&b] &cNo available map found!"));
            }
        } else {
            commandSender.sendMessage(MessageCreator.t("&7[&bdev-tools&b] &cPlease enable dev-mode in options.json."));
        }
        return true;
    }
}
