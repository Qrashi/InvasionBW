package link.ignyte.plugins.bedwars.Commands;

import link.ignyte.plugins.bedwars.BedWars;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import link.ignyte.utils.JsonSingleton.JsonSingletons;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.scheduler.BukkitRunnable;

public class SaveCommand implements CommandExecutor {

    private boolean canSave = true;

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (canSave) {
            JsonSingletons.saveAll();
            MessageCreator.sendWithPrefix("&aSaved all map data and configurations.");
            canSave = false;
            new BukkitRunnable() {
                @Override
                public void run() {
                    canSave = true;
                }
            }.runTaskLater(BedWars.getInstance(), 300);
        } else {
            commandSender.sendMessage(MessageCreator.withPrefix("&cPlease wait a bit before saving again!"));
        }
        return true;
    }
}
