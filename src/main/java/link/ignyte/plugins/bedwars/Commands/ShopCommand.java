package link.ignyte.plugins.bedwars.Commands;

import link.ignyte.plugins.bedwars.Inventories.Shop.ShopManager;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ShopCommand implements CommandExecutor, TabCompleter {

    private final List<String> returner = Arrays.asList("load", "save", "set");

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (strings.length < 1) {
            commandSender.sendMessage(MessageCreator.t("&7[&aShopManager&7] &aSelected shop: &7" + ShopManager.get(ShopManager.class).selected()));
        } else {
            if (strings.length == 1) {
                switch (strings[0]) {
                    case "load" -> {
                        ShopManager.load();
                        commandSender.sendMessage(MessageCreator.t("&7[&aShopManager&7] &aLoaded shops!"));
                        for (Player player : Bukkit.getOnlinePlayers()) {
                            MessageCreator.sendTitle(player, "&cWarning", "&7Loaded shops, plugin is now unstable", 100);
                            player.sendMessage(MessageCreator.withPrefix("&cShops were forceloaded. There may occur issues if you are playing bedwars! Use /endgame to fix this."));
                        }
                    }
                    case "save" -> {
                        ShopManager.save(ShopManager.class);
                        commandSender.sendMessage(MessageCreator.t("&7[&aShopManager&7] &aSaved shops!"));
                    }
                    case "set" -> commandSender.sendMessage(MessageCreator.t("&7[&aShopManager&7] &aSelected shop: " + ShopManager.get(ShopManager.class).selected()));
                    default -> commandSender.sendMessage(MessageCreator.t("&7[&aShopManager&7] &cInvalid operation!"));
                }
            } else if (strings[0].equals("set")) {
                StringBuilder shop = new StringBuilder();
                String[] args_shop = Arrays.copyOfRange(strings, 1, strings.length);
                for (String arg : args_shop) {
                    shop.append(arg);
                }
                if (ShopManager.exists(shop.toString())) {
                    ShopManager.select(shop.toString());
                } else {
                    commandSender.sendMessage(MessageCreator.t("&7[&aShopManager&7] &cShop not found!"));
                }
                return true;
            } else {
                commandSender.sendMessage(MessageCreator.t("&7[&aShopManager&7] &cInvalid options!"));
            }
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] strings) {
        List<String> result = new ArrayList<>();
        if (strings.length == 1) {
            for (String a : returner) {
                if (a.toLowerCase().startsWith(strings[0].toLowerCase())) {
                    result.add(a);
                }
            }
            return result;
        } else if (strings.length > 1 && strings[0].equals("set")) {
            for (String str : ShopManager.get(ShopManager.class).shopNames()) {
                if (str.toLowerCase().startsWith(strings[1].toLowerCase())) {
                    result.add(str);
                }
            }
            return result;
        }
        return null;
    }
}
