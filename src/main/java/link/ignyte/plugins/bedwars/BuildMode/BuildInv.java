package link.ignyte.plugins.bedwars.BuildMode;

import link.ignyte.plugins.bedwars.BedWars;
import link.ignyte.plugins.bedwars.Inventories.BetterItem.BetterItem;
import link.ignyte.plugins.bedwars.Inventories.BetterItem.BetterItemDescription;
import link.ignyte.plugins.bedwars.Inventories.InvOpener;
import link.ignyte.plugins.bedwars.Inventories.InventoryHandeler;
import link.ignyte.plugins.bedwars.Management.EndCredits;
import link.ignyte.plugins.bedwars.Management.GameManager;
import link.ignyte.plugins.bedwars.Maps.ClearState;
import link.ignyte.plugins.bedwars.Maps.GameMap;
import link.ignyte.plugins.bedwars.Maps.LocationSaver;
import link.ignyte.plugins.bedwars.Maps.SerializableLocation;
import link.ignyte.plugins.bedwars.Maps.Spawners.Spawner;
import link.ignyte.plugins.bedwars.Maps.Spawners.SpawnerType;
import link.ignyte.plugins.bedwars.Maps.Teams.Team;
import link.ignyte.plugins.bedwars.Maps.Teams.TeamManager;
import link.ignyte.plugins.bedwars.Utils.Advancements;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import link.ignyte.plugins.bedwars.Utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BuildInv {

    // TODO: rework

    public static Inventory getBuildInv() {
        Inventory inv = InventoryHandeler.createInventory("&aBuild tools");
        inv.setItem(13, getMapStatusItem());
        inv.setItem(20, new BetterItem((event) -> {
            if (BuildModeManager.isInBuild()) InvOpener.openDelay(event.getPlayer(), BuildInv.getTeamManager());
            return true;
        }, Material.RED_BED).setName("&aManage teams").create(new BetterItemDescription("Left click to open", Arrays.asList("&a&l+ &7Inspect team settings", "&f> &7Team size", "&f> &7Number of teams", "&f> &7Spawnpoints", "&f> &7Team &cbeds"))));

        //inv.setItem(20 ,InventoryHandeler.createStack(Material.RED_BED, "&aManage teams", Arrays.asList("&7Inspect team settings", "", "&7This includes:", "&f> &7Team size", "&f> &7How many teams there are", "&f> &7Team spawnpoints", "&f> &7Team beds", "", "&aLeft click to open"), "o(teamman)"));
        inv.setItem(22, new BetterItem((event) -> {
            if (BuildModeManager.isInBuild()) {
                MessageCreator.sendTitle(event.getPlayer(), "&7Editing spawners", "", 30);
                BuildModeManager.equipSpawners(event.getPlayer());
                InvOpener.closeDelay(event.getPlayer());
            }
            return true;
        }, Material.SPAWNER).setName("&6Manage spawners").create(new BetterItemDescription("Left click to open", Arrays.asList("&7Inspect spawner settings", "", "&7This includes:", "&7Setting the location of", "&6Gold spawners", "&7&lIron Spawners", "&cBronze spawners"))));

        //inv.setItem(22 ,InventoryHandeler.createStack(Material.GOLD_INGOT, "&6Manage spawners", Arrays.asList("&7Inspect spawner settings", "", "&7This includes:", "&f> &7Setting the location of", "&f> &6Gold spawners", "&f> &7&lIron Spawners", "&f> &cBronze spawners", "", "&aLeft click to open"), "b(spawner)"));
        inv.setItem(24, new BetterItem((event) -> {
            if (BuildModeManager.isInBuild()) {
                InvOpener.openDelay(event.getPlayer(), BuildInv.getSpawnPointInv());
            }
            return true;
        }, Material.CREEPER_HEAD).setName("&7Manage spawnpoints").create(new BetterItemDescription("Click to open", Arrays.asList("Manage spawnpoints of", "> Spectators", "> People that join in BuildMode", "&6&k- &aSet the time of the map &6&k-"))));

        //inv.setItem(24 ,InventoryHandeler.createStack(Material.CREEPER_SPAWN_EGG, "&7Manage spawnpoints", Arrays.asList("&7Inspect spawnpoint settings", "", "&7This includes:", "&f> &7Setting the spectator spawnpoint", "&f> &7Setting the map-spectating spawnpoint", "&f> &7Setting the &3inGame time", "", "&aLeft click to open"), "o(spawnpman)"));
        inv.setItem(31, new BetterItem((event) -> {
            if (BuildModeManager.isInBuild()) BuildModeManager.rename(event.getPlayer());
            return true;
        }, Material.ANVIL)
                .setName("&cRename this map")
                .create(
                        new BetterItemDescription("Rename this map", BetterItemDescription.ClickType.LEFT, Arrays.asList("Left click to rename this map", "> An Anvil inventory will be opened"))
                ));

        //inv.setItem(31, InventoryHandeler.createStack(Material.ANVIL, "&cRename map", Arrays.asList("&7Rename the current map.", "&aLeft click to open"), "b(rename)"));
        inv.setItem(36, new BetterItem((event) -> {
            InvOpener.closeDelay(event.getPlayer());
            return true;
        }, Material.RED_STAINED_GLASS_PANE)
                .setName("&f« &cClose")
                .create(
                        new BetterItemDescription("Closes this inventory", BetterItemDescription.ClickType.LEFT, List.of(""))
                ));

        //inv.setItem(36, InventoryHandeler.createStack(Material.RED_STAINED_GLASS_PANE, "&cClose", "o(close)"));
        return inv;
    }

    private static ItemStack getMapStatusItem() {
        Material status = Material.PURPLE_STAINED_GLASS_PANE;
        String name = "&cOops, something unexpected happened!";
        ArrayList<String> lore = new ArrayList<>();
        ArrayList<String> errors = new ArrayList<>();
        GameMap map = GameManager.getMap();
        TeamManager teamManager = map.teamManager;
        boolean spawner_ok = map.locs.getSpawners(SpawnerType.BRONZE).size() >= teamManager.getNumberOfTeams();
        boolean teams_ok = true;
        for (Team team : teamManager.getTeamList()) {
            if (team.getRespawnBlock() == null || team.getSpawn() == null) {
                teams_ok = false;
                errors.add("");
                errors.add("&f> &cUnfinished team: " + team.col.teamName);
                if (team.getRespawnBlock() == null) {
                    errors.add("&f>> &cBed not set!");
                }
                if (team.getSpawn() == null) {
                    errors.add("&f>> &cSpawnpoint not set!");
                }
                errors.add("");

            }
            if (team.getRespawnBlock() != null) {
                if (team.getRespawnBlock().getFace() == null) {
                    teams_ok = false;
                    errors.add("");
                    errors.add("&f> &cError with a bed");
                    errors.add("&f>> &cPlease replace bed of team " + team.col.teamName);
                    errors.add("");
                }
            }
        }
        if (!spawner_ok) {
            errors.add("");
            errors.add("&f> &cNot enough spawners!");
            errors.add("&f>> &cEvery team needs to have at LEAST &lone bronze spawner&c.");
            errors.add("&f>> &cCurrent bronze spawners: &l" + map.locs.getSpawners(SpawnerType.BRONZE).size());
            errors.add("");
        }
        if (spawner_ok && teams_ok) {
            status = Material.LIME_STAINED_GLASS_PANE;
            name = "&aReady to finish";
            lore.add("&7This map has been checked and was found to be &aready&7!");
            lore.add("");
            lore.add("&7You may now click here to mark this map as \"&aavialible&7\"!");
            lore.add("&cDoing so &lwill end the game&c and &lkick all players!");
            lore.add("&a&lThanks for building an awesome map!");
            return new BetterItem((event) -> {
                if (BuildModeManager.checkReady()) {
                    InvOpener.closeDelay(event.getPlayer());
                    GameManager.getMap().available = true;
                    GameManager.getMap().bbox.state = ClearState.NOT_CLEARED;
                    Bukkit.getOnlinePlayers().forEach(player -> Advancements.award(player, "bedwars/root/build/finish"));
                    EndCredits.end(new ArrayList<>(Bukkit.getOnlinePlayers()));
                }
                return true;
            }, status)
                    .setName(name)
                    .addLore(lore)
                    .setGlint(true)
                    .create();
        } else {
            status = Material.RED_STAINED_GLASS_PANE;
            name = "&cSome things are missing!";
            lore.add("&7This map has been checked and was found to be &cmissing some things&7!");
            lore.add("");
            lore.add("&cThings not ready yet:");
            lore.addAll(errors);
            lore.add("&cEnd of error report");
            return new BetterItem(event -> true, status).setName(name).addLore(lore).create()
                    ;
        }
    }

    public static Inventory getTeamManager() {
        Inventory inv = InventoryHandeler.createInventory("Team manager");
        inv.setItem(20, new BetterItem((event) -> {
            if (event.isRightClick()) {
                //Right click
                int teams = GameManager.getMap().teamManager.getNumberOfTeams();
                if (teams >= 2) {
                    GameManager.getMap().teamManager.setNumberOfTeams(--teams);
                    InvOpener.openDelay(event.getPlayer(), BuildInv.getTeamManager());
                }
            } else {
                int teams = GameManager.getMap().teamManager.getNumberOfTeams();
                if (Utils.getMax_teams() >= teams) {
                    GameManager.getMap().teamManager.setNumberOfTeams(++teams);
                    InvOpener.openDelay(event.getPlayer(), BuildInv.getTeamManager());
                }
                //Left click
            }
            return true;
        }, Material.PAPER)
                .setName("&7Team count")
                .create(
                        new BetterItemDescription("&aIncrease team count", "&cDecrease team count", Arrays.asList("&7Click on this item to manage the", "&7overall team count.", "", "&aCurrent team count:", "&3> &a" + GameManager.getMap().teamManager.getNumberOfTeams()))
                ));
        //inv.setItem(20, InventoryHandeler.createStack(Material.PAPER, "&7Team count", Arrays.asList("&7Current teams: &a" + GameManager.getCurrentMap().teamManager.getTeams(), "", "&a&l+ &aLeft click to increase &l+", "&c&l- &cRight click to decrease &l-"), "b(team+)", "b(team-)"));

        inv.setItem(22, new BetterItem((event) -> {
            if (event.isRightClick()) {
                //Right click
                int teams = GameManager.getMap().teamManager.getTeamSize();
                if (teams > 1) {
                    GameManager.getMap().teamManager.setTeamSize(--teams);
                    InvOpener.openDelay(event.getPlayer(), BuildInv.getTeamManager());
                }
            } else {
                int teams = GameManager.getMap().teamManager.getTeamSize();
                if (8 > teams) {
                    GameManager.getMap().teamManager.setTeamSize(++teams);
                    InvOpener.openDelay(event.getPlayer(), BuildInv.getTeamManager());
                }
                //Left click
            }
            return true;
        }, Material.PAPER)
                .setName("&7Team size")
                .create(
                        new BetterItemDescription("&aIncrease team size", "&cDecrease team size", Arrays.asList("&7Click on this item to manage the", "&7overall team size.", "", "&aCurrent team size:", "&3> &a" + GameManager.getMap().teamManager.getTeamSize()))
                ));
        //inv.setItem(22, InventoryHandeler.createStack(Material.PAPER, "&7Team size", Arrays.asList("&7Current team size: &a" + GameManager.getCurrentMap().teamManager.getTeamSize(), "", "&a&l+ &aLeft click to increase &l+", "&c&l- &cRight click to decrease &l-"), "b(size+)", "b(size-)"));


        inv.setItem(24, new BetterItem((event) -> {
            if (BuildModeManager.isInBuild()) {
                InvOpener.openDelay(event.getPlayer(), BuildInv.getTeamManagerTeamsInv());
            }
            return true;
        }, Material.RED_BED)
                .setName("Edit Teams")
                .setGlint(true)
                .create(
                        new BetterItemDescription("Open the edit teams page", Arrays.asList("Here you can", "> Set the beds of teams", "> Set the spawnpoints of teams"))
                ));
        //inv.setItem(24, InventoryHandeler.createStack(Material.RED_BED, "&5Team spawns", Arrays.asList("&7Inspect team spawnpoint settings", "", "&7This includes:", "&f> &7Setting the spawn of teams", "&f> &7Setting the beds of a team"), "b(teamsett)"));


        inv.setItem(36, new BetterItem((event) -> {
            if (BuildModeManager.isInBuild()) {
                InvOpener.openDelay(event.getPlayer(), BuildInv.getBuildInv());
            }
            return true;
        }, Material.RED_STAINED_GLASS_PANE)
                .setName("&f« &cClose")
                .create(
                        new BetterItemDescription("Click to go back to the main overview", List.of(""))
                ));
        //inv.setItem(36, InventoryHandeler.createStack(Material.RED_STAINED_GLASS_PANE, "&cGo back", "o(buildinv)"));
        return inv;
    }

    public static Inventory getSpawnermanager() {
        GameMap map = GameManager.getMap();
        Inventory inv = InventoryHandeler.createInventory("&6Spawner list");

        ArrayList<String> gold_list = new ArrayList<>();
        gold_list.add("&7Manage gold spawners:"); //gold_list.add("&aLeft click to create a gold spawner"); gold_list.add("&c&l- &cRight click to delete &cALL&c gold spawners &l-"); gold_list.add( "");gold_list.add("&bList of all gold spawners:");
        for (Spawner spawner : map.locs.getSpawners()) {
            if (spawner.getType() == SpawnerType.GOLD) {
                SerializableLocation loc = spawner.getLoc();
                gold_list.add("&f> " + loc.getX() + " | " + loc.getY() + " | " + loc.getZ());
            }
        }
        gold_list.add("");
        gold_list.add("&c&l+ &cLeft click to delete all &6gold&c spawners");

        ArrayList<String> iron_list = new ArrayList<>();
        iron_list.add("&7Manage iron spawners:"); //iron_list.add("&aLeft click to create an iron spawner"); iron_list.add("&c&l- &cRight click to delete &cALL&c iron spawners &l-"); iron_list.add( "");iron_list.add("&bList of all iron spawners:");
        for (Spawner spawner : map.locs.getSpawners()) {
            if (spawner.getType() == SpawnerType.IRON) {
                SerializableLocation loc = spawner.getLoc();
                iron_list.add("&f> " + loc.getX() + " | " + loc.getY() + " | " + loc.getZ());
            }
        }
        iron_list.add("");
        iron_list.add("&c&l+ &cLeft click to delete all &7iron&c spawners");

        ArrayList<String> bronze_list = new ArrayList<>();
        bronze_list.add("&7Manage bronze spawners:"); //bronze_list.add("&aLeft click to create a bronze spawner"); bronze_list.add("&c&l- &cRight click to delete &cALL&c bronze spawners &l-"); bronze_list.add( "");bronze_list.add("&bList of all bronze spawners:");
        for (Spawner spawner : map.locs.getSpawners()) {
            if (spawner.getType() == SpawnerType.BRONZE) {
                SerializableLocation loc = spawner.getLoc();
                bronze_list.add("&f> " + loc.getX() + " | " + loc.getY() + " | " + loc.getZ());
            }
        }
        bronze_list.add("");
        bronze_list.add("&c&l+ &cLeft click to delete all bronze spawners");

        inv.setItem(20, new BetterItem((event) -> {
            LocationSaver locs_iron = GameManager.getMap().locs;
            locs_iron.getSpawners().removeAll(locs_iron.getSpawners(SpawnerType.GOLD));
            MessageCreator.sendTitle(event.getPlayer(), "&aSuccess!", "&cDeleted &lALL &6gold &cspawners", 30, true);
            InvOpener.closeDelay(event.getPlayer());
            return true;
        }, Material.GOLD_BLOCK).addLore(gold_list).setName("&6Gold spawners").create());
        //inv.setItem(20, InventoryHandeler.createStack(Material.GOLD_BLOCK, "&6Gold spawners", gold_list, "b(delgold)"));
        inv.setItem(22, new BetterItem((event) -> {
            LocationSaver locs_iron = GameManager.getMap().locs;
            locs_iron.getSpawners().removeAll(locs_iron.getSpawners(SpawnerType.IRON));
            MessageCreator.sendTitle(event.getPlayer(), "&aSuccess!", "&cDeleted &lALL &firon &cspawners", 30, true);
            InvOpener.closeDelay(event.getPlayer());
            return true;
        }, Material.IRON_BLOCK).addLore(iron_list).setName("&fIron spawners").create());
        //inv.setItem(22, InventoryHandeler.createStack(Material.IRON_BLOCK, "&fIron spawners", iron_list, "b(deliron)"));
        inv.setItem(24, new BetterItem((event) -> {
            LocationSaver locs_iron = GameManager.getMap().locs;
            locs_iron.getSpawners().removeAll(locs_iron.getSpawners(SpawnerType.BRONZE));
            MessageCreator.sendTitle(event.getPlayer(), "&aSuccess!", "&cDeleted &lALL &cbronze &cspawners", 30, true);
            InvOpener.closeDelay(event.getPlayer());
            return true;
        }, Material.BROWN_TERRACOTTA).addLore(bronze_list).setName("&cBronze spawners").create());
        return inv;
    }

    public static Inventory getSpawnPointInv() {
        Inventory inv = InventoryHandeler.createInventory("Manage Spawnpoints");
        SerializableLocation specspawn = GameManager.getMap().locs.getSpectator_spawn();
        String specspawn_loc = "&f> &7" + specspawn.getX() + " | " + specspawn.getY() + " | " + specspawn.getZ();
        SerializableLocation lobbyspawn = GameManager.getMap().locs.getBuild_mode_spawn();
        String lobbyspawn_loc = "&f> &7" + lobbyspawn.getX() + " | " + lobbyspawn.getY() + " | " + specspawn.getZ();

        inv.setItem(20, new BetterItem((event) -> {
            if (event.isRightClick()) {
                GameManager.getMap().locs.getBuild_mode_spawn().teleport(event.getPlayer());
                InvOpener.closeDelay(event.getPlayer());
                BedWars.getWorld().spawnParticle(Particle.PORTAL, GameManager.getMap().locs.getSpectator_spawn().getLocation().add(0, 1, 0), 1000, 0, 0, 0);
                event.getPlayer().sendMessage(MessageCreator.t("&7[&aBuildMode&7] &7Teleported you to the &aBuildMode spawn&7!"));
            } else {
                SerializableLocation toSet = new SerializableLocation(event.getPlayer());
                GameManager.getMap().locs.setBuild_mode_spawn(toSet);
                InvOpener.closeDelay(event.getPlayer());

                event.getPlayer().sendMessage(MessageCreator.t("&7[&aBuildMode&7] &aSuccessfully &7set the &aBuildMode spawn&7 to your location."));
            }
            return true;
        }, Material.WITHER_SKELETON_SKULL)
                .setName("&aBuildMode spawnpoint")
                .create(
                        new BetterItemDescription("Set to &a&lYOUR&a location", "Teleport to the location", Arrays.asList("Here everyone", "> Spectating while choosing a map", "> &aBuilding", "will spawn"))
                ));
        //inv.setItem(20, InventoryHandeler.createStack(Material.WITHER_SKELETON_SKULL, "&fLobby spawn", Arrays.asList("&7If you choose to spectate or", "&7build a map you will be teleported to this location", "&a&l+ &aLeft click&7 to &bset&7 this &blocation", "&a&l+ &aRight click&7 to &bteleport", "", "&bCurrent location:", specspawn_loc), "b(lobbyloc)", "b(lobbyloc)"));

        inv.setItem(22, new BetterItem((event) -> {
            BuildModeManager.setTime(event.getPlayer());
            return true;
        }, Material.CLOCK)
                .setName("&6&k- &aSet the time &6&k-")
                .setGlint(true)
                .create(
                        new BetterItemDescription("Set the map time", Arrays.asList("You will be able to set the time", "of the minecraft world", "while this map is being played / built"))
                ));
        //inv.setItem(22, InventoryHandeler.createStack(Material.CLOCK, "&7Set ingame time", Collections.singletonList("&a&l+ &aLeft click&7 to set the game time"), "b(settime)"));

        inv.setItem(24, new BetterItem((event) -> {
            if (event.isRightClick()) {
                GameManager.getMap().locs.getSpectator_spawn().teleport(event.getPlayer());
                InvOpener.closeDelay(event.getPlayer());
                BedWars.getWorld().spawnParticle(Particle.PORTAL, GameManager.getMap().locs.getSpectator_spawn().getLocation().add(0, 1, 0), 1000, 0, 0, 0);
                event.getPlayer().sendMessage(MessageCreator.t("&7[&aBuildMode&7] &7Teleported you to the spectator spawn&7!"));
            } else {
                SerializableLocation toSet = new SerializableLocation(event.getPlayer());
                GameManager.getMap().locs.setSpectator_spawn(toSet);
                InvOpener.closeDelay(event.getPlayer());
                event.getPlayer().sendMessage(MessageCreator.t("&7[&aBuildMode&7] &aSuccessfully &7set the spectator spawn&7 to your location."));
            }
            return true;
        }, Material.SKELETON_SKULL)
                .setName("&7Spectator spawnpoint")
                .create(
                        new BetterItemDescription("Set to &a&lYOUR&a location", "Teleport to the location", Arrays.asList("Here everyone", "> That &cdies&7 and doesn't have a bed", "> Who joins after the game has &astarted", "will spawn"))
                ));
        //inv.setItem(24, InventoryHandeler.createStack(Material.SKELETON_SKULL, "&fSpectator spawn", Arrays.asList("&7If you die in bedwars", "&7you will be teleported to this location", "&a&l+ &aLeft click&7 to &bset&7 this &blocation", "&a&l+ &aRight click&7 to &bteleport", "", "&bCurrent location:", lobbyspawn_loc), "b(specloc)", "b(lobbyloc)"));

        inv.setItem(36, new BetterItem((event) -> {
            if (!BuildModeManager.isInBuild()) return true;
            InvOpener.openDelay(event.getPlayer(), BuildInv.getBuildInv());
            return true;
        }, Material.RED_STAINED_GLASS_PANE)
                .setName("&f« &cClose")
                .create(
                        new BetterItemDescription("Click to close", List.of(""))
                ));
        //inv.setItem(36, InventoryHandeler.createStack(Material.RED_STAINED_GLASS_PANE, "&cGo back", "o(buildinv)"));
        return inv;
    }

    public static Inventory getTeamManagerTeamsInv() {
        Inventory inv = InventoryHandeler.createInventory("Team list", 36);
        int slot = 9;
        for (int team = 0; team < 18; team++) {
            if (team == GameManager.getMap().teamManager.getNumberOfTeams()) break;
            inv.setItem(slot, makeTeamItem(team));
            slot++;
        }


        inv.setItem(27, new BetterItem((event) -> {
            if (!BuildModeManager.isInBuild()) return true;
            InvOpener.openDelay(event.getPlayer(), BuildInv.getTeamManager());
            return true;
        }, Material.RED_STAINED_GLASS_PANE)
                .setName("&f« &cClose")
                .create(
                        new BetterItemDescription("Click to close", List.of(""))
                ));
        //inv.setItem(27, InventoryHandeler.createStack(Material.RED_STAINED_GLASS_PANE, "&cGo back", "o(teamman)"));
        return inv;
    }

    private static ItemStack makeTeamItem(int team) {
        Team real_team = GameManager.getMap().teamManager.teams.get(team);
        ArrayList<String> lore = new ArrayList<>();
        lore.add("&f> &7Team index: &b" + team);
        lore.add("&f> &7Team size: &b" + GameManager.getMap().teamManager.getTeamSize());
        if (real_team.getRespawnBlock() != null) {
            SerializableLocation loc = real_team.getRespawnBlock().getLoc();
            lore.add("&f> &7Team bed: &3" + loc.getX() + " | " + loc.getY() + " | " + loc.getZ());
        } else {
            lore.add("&f> &7Team bed: &cNot set yet!");
        }
        if (real_team.getSpawn() != null) {
            SerializableLocation loc = real_team.getSpawn();
            lore.add("&f> &7Team spawn: &3" + loc.getX() + " | " + loc.getY() + " | " + loc.getZ());
        } else {
            lore.add("&f> &7Team spawn: &cNot set yet!");
        }
        return new BetterItem((event) -> {
            if (BuildModeManager.isInBuild()) {
                BuildModeManager.equipTeamTools(real_team, event.getPlayer().getInventory());
                InvOpener.closeDelay(event.getPlayer());
                MessageCreator.sendTitle(event.getPlayer(), real_team.col.teamName, "&7Editing team.", 30);
            }
            return true;
        }, real_team.col.gui_material)
                .setName(real_team.col.teamName)
                .create(
                        new BetterItemDescription("Receive team tools", lore)
                );
    }

}
