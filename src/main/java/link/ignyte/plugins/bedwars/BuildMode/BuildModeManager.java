package link.ignyte.plugins.bedwars.BuildMode;

import link.ignyte.plugins.bedwars.BedWars;
import link.ignyte.plugins.bedwars.Inventories.BetterItem.BetterItem;
import link.ignyte.plugins.bedwars.Inventories.BetterItem.BetterItemDescription;
import link.ignyte.plugins.bedwars.Inventories.BetterItem.EventType;
import link.ignyte.plugins.bedwars.Inventories.InvOpener;
import link.ignyte.plugins.bedwars.Inventories.InventoryHandeler;
import link.ignyte.plugins.bedwars.Management.*;
import link.ignyte.plugins.bedwars.Maps.GameMap;
import link.ignyte.plugins.bedwars.Maps.MapManager;
import link.ignyte.plugins.bedwars.Maps.SerializableLocation;
import link.ignyte.plugins.bedwars.Maps.Spawners.SpawnerType;
import link.ignyte.plugins.bedwars.Maps.Teams.RespawnBlock;
import link.ignyte.plugins.bedwars.Maps.Teams.Team;
import link.ignyte.plugins.bedwars.Maps.Teams.TeamManager;
import link.ignyte.plugins.bedwars.Utils.Advancements;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import link.ignyte.plugins.bedwars.Utils.Utils;
import net.wesjd.anvilgui.AnvilGUI;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Arrays;
import java.util.Collections;

public class BuildModeManager {

    public static void startBuild(GameMap map) {
        GameManager.setGameMap(map);
        GameManager.setState(GameState.IN_GAME);
        for (Player player : Bukkit.getOnlinePlayers()) {
            player.closeInventory();
            Advancements.award(player, "bedwars/root/build");
        }
        map.bbox.setToAir(Utils.getMaterialList());
        map.teamManager.redoBeds();
        for (Entity entity : BedWars.getWorld().getEntities())
            if (entity.getType() == EntityType.DROPPED_ITEM) entity.remove();
        WorldBorderManager.forceUpdate();
        for (Player player : Bukkit.getOnlinePlayers()) {
            player.setGameMode(GameMode.CREATIVE);
            MessageCreator.sendTitle(player, "&aBuildMode", "&7Building " + map.name, 100);
            player.setFlying(true);
            player.getInventory().clear();
            player.teleport(map.locs.build_mode_spawn.getLocation().add(0, 1, 0));
            Advancements.award(player, "build");
        }
        StatusMessenger.update();
        ScoreboardManager.now();

    }

    public static boolean isInBuild() {
        return GameManager.getPlayType() == PlayType.BUILDING && GameManager.getState() == GameState.IN_GAME;
    }


    public static void equipSpawners(Player player) {
        Inventory pinv = player.getInventory();
        pinv.setItem(0, new BetterItem((event) -> {
            if (BuildModeManager.isInBuild()) {
                if (event.getType() == EventType.EXTERNAL) {
                    if (event.getClickedBlock() != null && event.getFace() != null) {
                        BuildModeManager.createSpawner(SpawnerType.GOLD, event.getClickedBlock().getRelative(event.getFace()), event.getPlayer());
                    }
                    InvOpener.closeDelay(event.getPlayer());
                }
            }
            return true;
        }, Material.GOLD_BLOCK).addLore("&a&l+ &7Click to create a &6gold&7 spawner").setName("&7Create a &6gold&7 spawner").setDroppable(true).setRemoveOnDrop(true).create());
        pinv.setItem(1, new BetterItem((event) -> {
            if (BuildModeManager.isInBuild()) {
                if (event.getType() == EventType.EXTERNAL) {
                    if (event.getClickedBlock() != null && event.getFace() != null) {
                        BuildModeManager.createSpawner(SpawnerType.IRON, event.getClickedBlock().getRelative(event.getFace()), event.getPlayer());
                    }
                    InvOpener.closeDelay(event.getPlayer());
                }
            }
            return true;
        }, Material.IRON_BLOCK).addLore("&a&l+ &7Click to create a &firon&7 spawner").setName("&7Create a &firon&7 spawner").setDroppable(true).setRemoveOnDrop(true).create());
        pinv.setItem(2, new BetterItem((event) -> {
            if (BuildModeManager.isInBuild()) {
                if (event.getType() == EventType.EXTERNAL) {
                    if (event.getClickedBlock() != null && event.getFace() != null) {
                        BuildModeManager.createSpawner(SpawnerType.BRONZE, event.getClickedBlock().getRelative(event.getFace()), event.getPlayer());
                    }
                    InvOpener.closeDelay(event.getPlayer());
                }
            }
            return true;
        }, Material.BROWN_TERRACOTTA).addLore("&a&l+ &7Click to create a &cbronze&7 spawner").setName("&7Create a &cbronze&7 spawner").setDroppable(true).setRemoveOnDrop(true).create());
        pinv.setItem(3, new BetterItem((event) -> {
            if (BuildModeManager.isInBuild()) {
                if (event.getType() == EventType.EXTERNAL) {
                    if (event.getClickedBlock() != null && event.getFace() != null) {
                        BuildModeManager.deleteSpawner(event.getClickedBlock().getRelative(event.getFace()), event.getPlayer());
                    }
                    InvOpener.closeDelay(event.getPlayer());
                }
            }
            return true;
        }, Material.RED_WOOL).addLore("&a&l+ &7Click to create a &cdelete &7a spawner").setName("&cRemove a spawner").setDroppable(true).setRemoveOnDrop(true).create());
        pinv.setItem(4, new BetterItem((event) -> {
            InvOpener.openDelay(event.getPlayer(), BuildInv.getSpawnermanager());
            return true;
        }, Material.BOOK).addLore("&a&l+ &7Click to &aview &7a list of spawners").setName("&7See a list of all spawner").setDroppable(true).setRemoveOnDrop(true).create());
        player.sendMessage(MessageCreator.t("&7[&aBuildMode&7] Place this blocks &babove/next to/under &7your spawner and &c&lnot&7 as a replacement!"));
    }

    public static void rename(Player player) {
        new BukkitRunnable() {
            @Override
            public void run() {
                new AnvilGUI.Builder().onComplete(((player1, s) -> {
                    if (player == player1) {
                        rename_confirm(player, s);
                    }
                    return AnvilGUI.Response.text("Renaming...");
                })).title("Rename your map").text("New name here").plugin(BedWars.getInstance()).open(player);
            }
        }.runTaskLater(BedWars.getInstance(), 1);
    }

    private static void rename_confirm(Player player, String text) {
        if (!MapManager.exists(text)) {
            GameManager.getMap().name = text;
            MapManager.loadNames();
            InvOpener.closeLater(player);
            player.sendMessage(MessageCreator.t("&7[&aBuildMode&7] &aSuccesfully renamed the map to \"" + text + "&a\"!"));
        } else {
            player.sendMessage(MessageCreator.t("&7[&aBuildMode&7] &cThis name already exists"));
            new BukkitRunnable() {
                @Override
                public void run() {
                    new AnvilGUI.Builder().onComplete(((player1, s) -> {
                        if (player == player1) {
                            rename_confirm(player, s);
                        }
                        return AnvilGUI.Response.text("Renaming...");
                    })).title("Rename your map").text("Name already in use!").plugin(BedWars.getInstance()).open(player);
                }
            }.runTaskLater(BedWars.getInstance(), 1);
        }
    }

    public static void setTime(Player player) {
        new BukkitRunnable() {
            @Override
            public void run() {
                new AnvilGUI.Builder().onComplete(((player1, s) -> {
                    if (player == player1) {
                        setTimeConfirm(s, player1);
                    }
                    return AnvilGUI.Response.text("Setting time...");
                })).title("Set the time of your map").text("New time here").plugin(BedWars.getInstance()).open(player);
            }
        }.runTaskLater(BedWars.getInstance(), 1);
    }

    private static void setTimeConfirm(String newTime, Player player) {
        if (isInt(newTime)) {
            int newTimeToSet = Integer.parseInt(newTime);
            GameManager.getMap().timeHolder.setTime(newTimeToSet);
            player.sendMessage(MessageCreator.t("&7[&aBuildMode&7] Time valid."));
            WorldBorderManager.forceUpdate();
            InvOpener.closeLater(player);
        } else {
            new BukkitRunnable() {
                @Override
                public void run() {
                    new AnvilGUI.Builder().onComplete(((player1, s) -> {
                        if (player == player1) {
                            setTimeConfirm(s, player1);
                        }
                        return AnvilGUI.Response.text("Setting time...");
                    })).title("Set the time of your map").text("Not an integer!").plugin(BedWars.getInstance()).open(player);
                }
            }.runTaskLater(BedWars.getInstance(), 1);
        }
    }

    public static boolean checkReady() {
        GameMap map = GameManager.getMap();
        TeamManager teamManager = map.teamManager;
        boolean spawner_ok = map.locs.getSpawners().size() > 2;
        boolean teams_ok = false;
        for (Team team : teamManager.getTeamList()) {
            teams_ok = team.getRespawnBlock() != null && team.getSpawn() != null;
        }
        return (spawner_ok && teams_ok);
    }

    private static boolean isInt(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException | NullPointerException a) {
            return false;
        }
        return true;
    }

    public static void equipTeamTools(Team team, Inventory inv) {
        inv.setItem(0, new BetterItem((event) -> {
            if (event.getClickedBlock() == null) return true;
            Block clickedBlock = event.getClickedBlock().getRelative(BlockFace.UP);
            if (BuildModeManager.isInBuild()) {
                RespawnBlock oldRespawnBlock = team.getRespawnBlock();
                if (oldRespawnBlock != null) {
                    SerializableLocation oldBedLoc = oldRespawnBlock.getLoc();
                    event.getPlayer().sendMessage(MessageCreator.t("&7[&aBuildMode&7] &cDestroying old bed..."));
                    Block oldBedBlock = oldBedLoc.getBlock();
                    oldBedBlock.setType(Material.AIR);
                    oldBedBlock.getState().update();
                    for (BlockFace face : BlockFace.values()) {
                        if (clickedBlock.getRelative(face, 2).getType().toString().endsWith("BED"))
                            clickedBlock.getRelative(face, 1).setType(Material.AIR);
                        oldBedBlock.getRelative(face, 1).getState().update();
                    }

                }
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        BlockFace facing = null;
                        for (BlockFace face : BlockFace.values()) {
                            if (face == BlockFace.SELF) continue;
                            //Bukkit.broadcastMessage("Found " + clicked.getRelative(face).getType().toString() + " on " + face.toString());
                            if (clickedBlock.getRelative(face, 1).getType().toString().endsWith("BED")) {
                                facing = face;
                                break;
                            }
                        }
                        if (facing == null) {
                            event.getPlayer().sendMessage(MessageCreator.t("&7[&aBuildMode&7] Error: could not find the correct direction, make sure no beds are nearby!"));
                            MessageCreator.sendTitle(event.getPlayer(), "&cFailure!", "&7Please look in the chat", 75);
                        } else {
                            team.updateBed(new RespawnBlock(new SerializableLocation(clickedBlock), facing));
                            event.getPlayer().sendMessage(MessageCreator.t("&7[&aBuildMode&7] Registered bed for team " + team.col.teamName + ". Direction is " + facing));
                            MessageCreator.sendTitle(event.getPlayer(), "&aSuccess!", "&7Set the bed of [" + team.col.teamName + "&7]", 75);
                            clickedBlock.getRelative(facing, 1).setType(Material.AIR);
                            clickedBlock.setType(Material.AIR);
                            new BukkitRunnable() {
                                @Override
                                public void run() {
                                    team.getRespawnBlock().redo();
                                }
                            }.runTaskLater(BedWars.getInstance(), 10);
                        }
                    }
                }.runTaskLater(BedWars.getInstance(), 1);
                return false;
            }
            return true;
        }, Material.RED_BED, Collections.singletonList(EventType.EXTERNAL)).setName("&7Set the &cbed &7of " + team.col.teamName).setDroppable(true).setRemoveOnDrop(true).create(new BetterItemDescription("Place to set bed", Arrays.asList("To use this item", "just place the bed wherever you want.", "&cDon't place if there qre beds nearby!"))));
        //inv.setItem(0, createStack(Material.RED_BED, "&7Set &cbed &7of [" + Utils.getTeamName(realTeam) + "&7]", Arrays.asList("&7How to use:", "&7f> &7Set the bed and wait for it to be registered"), "i(" + team + ")"));

        inv.setItem(1, new BetterItem((event) -> {
            if (BuildModeManager.isInBuild()) {
                SerializableLocation loc = new SerializableLocation(event.getClickedBlock().getRelative(event.getFace()));
                team.setSpawn(loc);
                BedWars.getWorld().spawnParticle(Particle.PORTAL, loc.getTpLocation(), 1000, 0, 0, 0);
                MessageCreator.sendTitle(event.getPlayer(), "&aSuccess!", "&7Set the spawn location!", 30);
            }
            return true;
        }, Material.BLAZE_ROD, Collections.singletonList(EventType.EXTERNAL)).setName("&7Set the spawn of " + team.col.teamName).setDroppable(true).setRemoveOnDrop(true).create(new BetterItemDescription("Players will spawn ON this block", Arrays.asList("Players will spawn rotated", "to the &aSpectator spawn", "of your world!"))));
        //inv.setItem(1, createStack(Material.BLAZE_ROD, "&7Set the &bspawn&7 of [" + Utils.getTeamName(realTeam) + "]", Arrays.asList("&7How to use:", "&f> &7Set the spawn by left clicking on a block."), "l(" + team + ")"));


        inv.setItem(2, new BetterItem((event) -> {
            if (BuildModeManager.isInBuild()) {
                InvOpener.openDelay(event.getPlayer(), BuildInv.getTeamManagerTeamsInv());
            }
            return true;
        }, Material.CHEST).setName("&5Edit another team").setDroppable(true).setRemoveOnDrop(true).create(new BetterItemDescription("Open the edit teams page", Arrays.asList("Here you can", "> Set the beds of teams", "> Set the spawnpoints of teams"))));
    }


    public static void createSpawner(SpawnerType type, Block where, Player player) {
        if (where.getType() == Material.AIR) {
            where.setType(Material.GREEN_WOOL);
            player.sendMessage(MessageCreator.t("&7[&aBuildMode&7] Checking block..."));
            new BukkitRunnable() {
                @Override
                public void run() {
                    where.setType(Material.AIR);
                    if (GameManager.getMap().locs.getSpawnerAt(new SerializableLocation(where.getLocation())) == null) {
                        BedWars.getWorld().spawnParticle(Particle.CLOUD, where.getLocation().add(0.5, 1, 0.5), 250, 0.1, 0.1, 0.1, 0);
                        MessageCreator.sendTitle(player, "&aSuccess!", "&7Spawner created", 50);
                        GameManager.getMap().locs.addSpawner(type, new SerializableLocation(where.getLocation()));
                        player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 100, 2);
                        player.sendMessage(MessageCreator.t("&7[&aBuildMode&7] &aCreated spawner!"));
                    } else {
                        BedWars.getWorld().spawnParticle(Particle.BLOCK_MARKER, where.getLocation().add(-0.5, 1, -0.5), 1, 0, 0, 0, 0);
                        MessageCreator.sendTitle(player, "&cFailure!", "&7There is already a spawner!", 75);
                        player.playEffect(player.getLocation(), Effect.ANVIL_BREAK, null);
                        player.sendMessage(MessageCreator.t("&7[&aBuildMode&7] &cThere is already a spawner in this location!"));
                    }
                }
            }.runTaskLater(BedWars.getInstance(), 20);
        } else {
            player.sendMessage(MessageCreator.t("&7[&aBuildMode&7] &cCan't replace " + where.getType() + "!"));
        }

    }

    public static void deleteSpawner(Block where, Player player) {
        where.setType(Material.WHITE_WOOL);
        player.sendMessage(MessageCreator.t("&7[&aBuildMode&7] Checking block..."));
        new BukkitRunnable() {
            @Override
            public void run() {
                where.setType(Material.AIR);
                if (GameManager.getMap().locs.getSpawnerAt(new SerializableLocation(where.getLocation())) != null) {
                    BedWars.getWorld().spawnParticle(Particle.CLOUD, where.getLocation().add(0.5, 1, 0.5), 250, 0.1, 0.1, 0.1, 0);
                    MessageCreator.sendTitle(player, "&aSuccess!", "&cSpawner deleted", 50);
                    GameManager.getMap().locs.removeSpawner(GameManager.getMap().locs.getSpawnerAt(new SerializableLocation(where.getLocation())));
                    player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 100, 2);
                    player.sendMessage(MessageCreator.t("&7[&aBuildMode&7] &cDeleted this spawner"));
                } else {
                    BedWars.getWorld().spawnParticle(Particle.BLOCK_MARKER, where.getLocation().add(-0.5, 1, -0.5), 1, 0, 0, 0, 0);
                    MessageCreator.sendTitle(player, "&cFailure!", "&7There is no spawner!", 75);
                    player.playEffect(player.getLocation(), Effect.ANVIL_BREAK, null);
                    player.sendMessage(MessageCreator.t("&7[&aBuildMode&7] &cThere is no spawner in this location!"));
                }
            }
        }.runTaskLater(BedWars.getInstance(), 20);

    }

    public static Inventory villagerInv(Entity villager) {
        Inventory inv = InventoryHandeler.createInventory("&7Villager overview");
        inv.setItem(25, new BetterItem(itemClickEvent -> {
            villager.remove();
            return true;
        }, Material.SKELETON_SKULL).setName("&cKill this villager").create(new BetterItemDescription("Remove this villager", "").setOnLeft("&cKill")));
        return inv;
    }
}
