package link.ignyte.plugins.bedwars.Listeners;

import link.ignyte.plugins.bedwars.Management.GameManager;
import link.ignyte.plugins.bedwars.Management.GameState;
import link.ignyte.plugins.bedwars.Management.PlayType;
import link.ignyte.plugins.bedwars.Utils.Features.EnderChestManager;
import link.ignyte.plugins.bedwars.Utils.Features.GunpowderManager;
import link.ignyte.plugins.bedwars.Utils.Features.RescuePlattformManager;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import link.ignyte.plugins.bedwars.Utils.Utils;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class BlockListeners implements Listener {
    @EventHandler
    public void onPlace(BlockPlaceEvent event) {
        PlayType playType = GameManager.getPlayType();
        if (GameManager.getState() != GameState.IN_GAME) {
            event.setCancelled(true);
        } else {
            switch (playType) {
                case BUILDING:
                    // Don't need to check if outside - world border
                    if (Utils.getMaterialList().contains(event.getBlockPlaced().getType())) {
                        if (!event.getBlockPlaced().getType().toString().endsWith("BED")) {
                            event.getPlayer().sendMessage(MessageCreator.t("&7[&aBuildMode&7] &cI'm sorry, but this block can't be used!"));
                            event.setCancelled(true);
                        }
                    }
                    break;
                case PLAYING:
                    if (!Utils.getMaterialList().contains(event.getBlockPlaced().getType())) {
                        event.setCancelled(true);
                        event.getPlayer().sendMessage(MessageCreator.withPrefix("&cYou can't place this block!"));
                    } else if (event.getBlockPlaced().getType().toString().endsWith("BED")) {
                        event.setCancelled(true);
                    }
                    break;
            }
        }
    }

    @EventHandler
    public void onBreak(BlockBreakEvent event) {
        if (GameManager.getState() == GameState.IN_GAME) {
            if (GameManager.getPlayType() == PlayType.PLAYING) {
                if (event.getBlock().getType() == Material.IRON_BLOCK) {
                    if (event.getPlayer().getInventory().getItemInMainHand().getType() == Material.WOODEN_PICKAXE) {
                        event.setCancelled(true);
                        event.getPlayer().sendMessage(MessageCreator.t("&cYou can't break an iron block with just a wooden pickaxe!"));
                    }
                }
                if (Utils.isBed(event.getBlock().getType())) {
                    event.setCancelled(GameManager.getMap().destroyRespawnBlockThere(event.getBlock(), event.getPlayer()));
                }
                if (!Utils.getMaterialList().contains(event.getBlock().getType())) {
                    event.setCancelled(true);
                    event.getPlayer().sendMessage(MessageCreator.withPrefix("&cYou can't break this block!"));
                }
            }
        } else {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if (GameManager.getState() == GameState.IN_GAME) {
            if (GameManager.getPlayType() == PlayType.PLAYING) {
                if (event.hasBlock()) {
                    if (!event.getClickedBlock().getType().toString().endsWith("CHEST")) {
                        //event.setCancelled(true);
                    } else if (event.getClickedBlock().getType() == Material.ENDER_CHEST) {
                        EnderChestManager.open(event.getPlayer());
                    }
                }
                if (event.hasItem()) {
                    switch (event.getItem().getType()) {
                        case GUNPOWDER -> GunpowderManager.onClick(event.getPlayer(), event.getItem());
                        case BLAZE_ROD -> {
                            if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
                                RescuePlattformManager.execute(event.getPlayer(), event.getItem());
                            }
                        }
                        //default -> event.setCancelled(true);
                    }
                }
            }
        } else {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onEntityExplode(EntityExplodeEvent event) {
        event.blockList().removeIf(this::cancel);
    }

    private boolean cancel(Block block) {
        Material type = block.getType();
        if (!Utils.getMaterialList().contains(type)) return true;
        return type == Material.RED_BED;
    }
}
