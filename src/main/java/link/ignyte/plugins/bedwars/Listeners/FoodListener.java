package link.ignyte.plugins.bedwars.Listeners;

import link.ignyte.plugins.bedwars.Management.GameManager;
import link.ignyte.plugins.bedwars.Management.GameState;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;

public class FoodListener implements Listener {
    @EventHandler
    public void onHungerChange(FoodLevelChangeEvent event) {
        if (GameManager.getState() != GameState.IN_GAME) {
            event.setCancelled(true);
        }
    }
}
