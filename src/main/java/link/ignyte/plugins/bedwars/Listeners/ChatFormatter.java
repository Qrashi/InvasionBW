package link.ignyte.plugins.bedwars.Listeners;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatFormatter implements Listener {

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        event.setFormat(ChatColor.BLACK + "| " + ChatColor.GRAY + "%1$s" + ChatColor.GREEN + " »" + ChatColor.RESET + " %2$s");
    }
}
