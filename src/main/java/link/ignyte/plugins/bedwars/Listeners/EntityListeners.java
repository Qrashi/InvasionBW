package link.ignyte.plugins.bedwars.Listeners;

import link.ignyte.plugins.bedwars.BuildMode.BuildModeManager;
import link.ignyte.plugins.bedwars.Inventories.InvOpener;
import link.ignyte.plugins.bedwars.Inventories.Shop.ShopManager;
import link.ignyte.plugins.bedwars.Management.GameManager;
import link.ignyte.plugins.bedwars.Management.GameRunner;
import link.ignyte.plugins.bedwars.Management.GameState;
import link.ignyte.plugins.bedwars.Management.PlayType;
import link.ignyte.plugins.bedwars.Players.PeresistentPlayerData;
import link.ignyte.plugins.bedwars.Players.PlayerData;
import link.ignyte.plugins.bedwars.Players.PlayerDataManager;
import link.ignyte.plugins.bedwars.Players.PlayerManager;
import link.ignyte.plugins.bedwars.Utils.Advancements;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import link.ignyte.plugins.bedwars.Utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;

public class EntityListeners implements Listener {

    @EventHandler
    public void onDamage(EntityDamageEvent event) {
        if (event.getEntityType() == EntityType.PLAYER) {
            if (GameManager.getState() == GameState.COUNTDOWN) {
                event.setDamage(0);
            } else {
                if (GameManager.getState() != GameState.IN_GAME) {
                    event.setCancelled(true);
                } else if (GameManager.getPlayType() == PlayType.PLAYING) {
                    // InGame - look if damage is too big
                    if (event.getEntity() instanceof Player player) {
                        if (player.getHealth() <= event.getFinalDamage()) {
                            Bukkit.broadcastMessage(MessageCreator.t(player.getDisplayName() + "&c died."));
                            event.setDamage(0);
                            GameRunner.die(player);
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void onSpawn(EntitySpawnEvent event) {
        if (event.getEntityType() == EntityType.VILLAGER) {
            if (event.getEntity() instanceof Villager villager) {
                villager.setAI(false);
                villager.setProfession(Villager.Profession.ARMORER);
                villager.setVillagerLevel(3);
                villager.setVillagerType(Villager.Type.SNOW);
            }
        }
    }

    @EventHandler
    public void onDamageExplode(BlockExplodeEvent event) {
        Bukkit.broadcastMessage("hey explode!");
        for (Block block : event.blockList()) {
            if (!Utils.getMaterialList().contains(block.getType())) {
                event.setCancelled(true);
            } else if (block.getType().toString().endsWith("BED") || block.getType() == Material.IRON_BLOCK) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onEntityInteractEvent(PlayerInteractEntityEvent event) {
        if (event.getRightClicked().getType() == EntityType.VILLAGER) {
            if (GameManager.getState() == GameState.IN_GAME) {
                if (GameManager.getPlayType() == PlayType.BUILDING) {
                    InvOpener.openDelay(event.getPlayer(), BuildModeManager.villagerInv(event.getRightClicked()));
                } else if (GameManager.getPlayType() == PlayType.PLAYING) {
                    InvOpener.openDelay(event.getPlayer(), ShopManager.inv());
                }
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onPlayerAttack(EntityDamageByEntityEvent event) {
        if (GameManager.getState() == GameState.IN_GAME) {
            if (GameManager.getPlayType() == PlayType.BUILDING && event.getEntityType() == EntityType.VILLAGER) {
                if (event.getDamager() instanceof Player damager) {
                    damager.playSound(damager.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 2);
                    event.getEntity().remove();
                }
                event.setCancelled(true);
                return;
            }
            Entity damaging_entity = event.getDamager();
            Entity damaged_entity = event.getEntity();
            if (damaging_entity instanceof Player damaging && damaged_entity instanceof Player damaged) {
                event.setCancelled(handleTwoPlayers(damaging, damaged, event.getFinalDamage()));
            }
        }


    }

    @EventHandler
    public void onPlayerAttack(ProjectileHitEvent event) {
        Entity damaged_entity = event.getHitEntity();
        Entity damaging_entity = (Entity) event.getEntity().getShooter();
        if (damaging_entity instanceof Player damaging && damaged_entity instanceof Player damaged) {
            event.setCancelled(handleTwoPlayers(damaging, damaged, 0));
        }
    }

    private boolean handleTwoPlayers(Player damager, Player damaged, double damage) {
        if (damager == damaged) {
            return true;
        }
        if (GameManager.getPlayType() != PlayType.PLAYING) {
            return true;
        } else {
            if (GameManager.getState() == GameState.IN_GAME) {
                PlayerData data_damaged = PlayerData.get(damaged);
                if (data_damaged.getTeam() == null) {
                    PlayerManager.fixPlayer(damaged, false);
                    return true;
                }
                PlayerData data_damaging = PlayerDataManager.getData(damager);
                if (data_damaging.getTeam() == null) {
                    PlayerManager.fixPlayer(damager, false);
                    return true;
                }
                if (data_damaging.getTeam().col.equals(data_damaged.getTeam().col)) {
                    // Same team
                    return true;
                }
                // Valid event, process damage
                if (damaged.getHealth() <= damage) {
                    Bukkit.broadcastMessage(MessageCreator.t(damaged.getDisplayName() + "&c was killed by " + damager.getDisplayName()));

                    PeresistentPlayerData.PeresistentData peresistentPlayerData = PeresistentPlayerData.get(damager);
                    if (peresistentPlayerData.kills == 0) {
                        Advancements.award(damager, "bedwars/root/play/kill");
                    } else if (peresistentPlayerData.kills == 99) {
                        Advancements.award(damager, "bedwars/root/play/kill/100");
                    }
                    peresistentPlayerData.kills++;

                    GameRunner.die(damaged);
                    return true;
                }
                return false;
            } else {
                return GameManager.getState() != GameState.COUNTDOWN;
            }
        }
    }
}
