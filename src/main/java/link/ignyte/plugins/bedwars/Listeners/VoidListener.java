package link.ignyte.plugins.bedwars.Listeners;

import link.ignyte.plugins.bedwars.Management.GameManager;
import link.ignyte.plugins.bedwars.Management.GameRunner;
import link.ignyte.plugins.bedwars.Management.GameState;
import link.ignyte.plugins.bedwars.Management.PlayType;
import link.ignyte.plugins.bedwars.Players.PlayerData;
import link.ignyte.plugins.bedwars.Players.PlayerManager;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class VoidListener implements Listener {
    @EventHandler
    public void onDamage(EntityDamageEvent event) {
        Entity entity = event.getEntity();
        if (entity instanceof Player player) {
            if (event.getCause() == EntityDamageEvent.DamageCause.VOID) {
                if (GameManager.getState() == GameState.IN_GAME && GameManager.getPlayType() == PlayType.PLAYING) {
                    // Playing a game - die
                    Bukkit.broadcastMessage(MessageCreator.withPrefix(player.getDisplayName() + "&c died."));
                    if (PlayerData.get(player).getTeam() == null) {
                        PlayerManager.fixPlayer(player, false);
                        event.setDamage(0);
                    } else {
                        GameRunner.die(player);
                        event.setDamage(0);
                    }
                } else {
                    PlayerManager.fixPlayer(player, false);
                    event.setDamage(0);
                }
            }
        }
    }
}
