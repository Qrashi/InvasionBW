package link.ignyte.plugins.bedwars;

import link.ignyte.plugins.bedwars.Commands.*;
import link.ignyte.plugins.bedwars.Feedback.FeedbackInv;
import link.ignyte.plugins.bedwars.Inventories.BetterItem.BetterItemManager;
import link.ignyte.plugins.bedwars.Inventories.BetterItem.GlowEnchant;
import link.ignyte.plugins.bedwars.Inventories.EndInventory;
import link.ignyte.plugins.bedwars.Inventories.ItemStacks;
import link.ignyte.plugins.bedwars.Inventories.Setup.MapChooser;
import link.ignyte.plugins.bedwars.Inventories.Shop.ShopManager;
import link.ignyte.plugins.bedwars.Listeners.*;
import link.ignyte.plugins.bedwars.Management.Countdown.CountdownManager;
import link.ignyte.plugins.bedwars.Management.Countdown.TeamGUI;
import link.ignyte.plugins.bedwars.Management.*;
import link.ignyte.plugins.bedwars.Maps.MapManager;
import link.ignyte.plugins.bedwars.Players.PlayerDataManager;
import link.ignyte.plugins.bedwars.Players.PlayerManager;
import link.ignyte.plugins.bedwars.Utils.*;
import link.ignyte.plugins.bedwars.Utils.error.*;
import link.ignyte.utils.JsonSingleton.GsonMode;
import link.ignyte.utils.JsonSingleton.JsonSingletons;
import org.bukkit.Bukkit;
import org.bukkit.GameRule;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;

import java.lang.reflect.Field;
import java.time.Instant;
import java.util.logging.Logger;

import static org.bukkit.Bukkit.getOnlinePlayers;

public final class BedWars extends JavaPlugin {

    public static final String version = "v1.3";
    private static final int initialisation_steps = 10;
    public static World main;
    public static long startup;
    private static BedWars instance;
    private static Logger logger;

    public static void log(String message) {
        logger.info(message);
    }

    public static Enchantment getGlow() {
        return new GlowEnchant(KeyAssistant.getKey("glow"));
    }

    public static Logger getLogging() {
        return logger;
    }

    public static World getWorld() {
        return main;
    }

    public static void setWorld(World world) {
        main = world;
        world.setGameRule(GameRule.DO_FIRE_TICK, false);
    }

    public static String getFolder() {
        return getInstance().getDataFolder().getPath();
    }

    public static BedWars getInstance() {
        return instance;
    }

    @Override
    public void onEnable() {
        logger = getLogger();
        instance = this;
        startup = Instant.now().getEpochSecond() - 1;
        long start = System.nanoTime();
        log("Starting BedWars at T+" + (startup + 1) + " (timestamp) [1/" + initialisation_steps + "]");

        log("Loading configuration files [2/" + initialisation_steps + "]");
        JsonSingletons.initialize(GsonMode.ONLY_EXPOSE, getDataFolder().getPath());
        FileUpdater.update();
        MapManager.loadNames();

        log("Initializing item API & default items [3/" + initialisation_steps + "]");
        getServer().getPluginManager().registerEvents(new BetterItemManager(), this);
        MapStatistics.load();
        ItemStacks.regenerate();
        TeamGUI.load();
        MapChooser.load();
        FeedbackInv.refresh();

        log("Adding custom enchantments [4/" + initialisation_steps + "]");
        registerGlow();

        log("Generating shop inventories [5/" + initialisation_steps + "]");
        ShopManager.load();

        log("Registering listeners [7/" + initialisation_steps + "]");
        register();

        log("Running Additional tasks [8/" + initialisation_steps + "]");
        StatusMessenger.start();
        Options.get(Options.class).auto_mode.start();
        GameManager.reset();


        logger.info("Fixing players [9/" + initialisation_steps + "]");
        for (Player player : getOnlinePlayers()) {
            player.setDisplayName(player.getName());
            Objective objective = player.getScoreboard().getObjective(DisplaySlot.SIDEBAR);
            if (objective != null) objective.unregister();
            if (player.getLocation().getWorld() != null) setWorld(player.getLocation().getWorld());
            MessageCreator.sendTitle(player, "", "&aReloaded!", 20);
        }
        new BukkitRunnable() {
            @Override
            public void run() {
                ScoreboardManager.addAll();
                Bukkit.getOnlinePlayers().forEach(PlayerManager::fixPlayer);
                if (PlayerManager.fix && Bukkit.getOnlinePlayers().size() > 0) {
                    WorldBorderManager.forceUpdate();
                    PlayerManager.fix = false;
                }
            }
        }.runTaskLater(this, 1);
        logger.info("Initializing advancements [10/" + initialisation_steps + "]");
        Advancements.initialize();
        long duration = (System.nanoTime() - start) / 1000000;
        logger.info("Plugin startup sequence completed within " + duration + "ms.");

    }

    private void registerGlow() {
        try {
            Field f = Enchantment.class.getDeclaredField("acceptingNew");
            f.setAccessible(true);
            f.set(null, true);
        } catch (Exception e) {
            ErrorHandeler.error(new ErrorCode(ErrorEffect.CORE, ErrorSeverity.MIDDLE, ErrorRelation.MISC_EVENT, "Glint:reg:accField"));
            e.printStackTrace();
        }
        try {
            if (Enchantment.getByKey(KeyAssistant.getKey("glow")) == null) {
                GlowEnchant glow = new GlowEnchant(KeyAssistant.getKey("glow"));
                Enchantment.registerEnchantment(glow);
            }
        } catch (IllegalArgumentException ex) {
            ErrorHandeler.error(new ErrorCode(ErrorEffect.CORE, ErrorSeverity.MIDDLE, ErrorRelation.MISC_EVENT, "Glint:reg:illegArgExecep"));
            ex.printStackTrace();
        } catch (Exception e) {
            ErrorHandeler.error(new ErrorCode(ErrorEffect.CORE, ErrorSeverity.MIDDLE, ErrorRelation.MISC_EVENT, "Glint:reg:add"));
            e.printStackTrace();
        }

    }

    @Override
    public void onDisable() {
        logger.info("Saving configurations [1/3]");
        JsonSingletons.saveAll();
        logger.info("Fixing player states [2/3]");
        for (Player player : getOnlinePlayers()) {
            player.setDisplayName(player.getName());
            Objective objective = player.getScoreboard().getObjective(DisplaySlot.SIDEBAR);
            if (objective != null) objective.unregister();
        }
        logger.info("Removing dropped items [3/3]");
        if (main != null) {
            for (Entity entity : BedWars.getWorld().getEntities()) {
                if (entity.getType() == EntityType.DROPPED_ITEM) {
                    entity.remove();
                }
            }
        }
        ScoreboardManager.unregister();
    }

    public void reload() {
        long start = System.nanoTime();
        // New ones / checked ones
        log("Reloading plugin...");
        startup = Instant.now().getEpochSecond() - 1;
        log("Clearing and regenerating caches [1/1]");
        BetterItemManager.reload(); // Invalidates ALL items!
        MapChooser.load();  // Load static items
        TeamGUI.load();
        GameRunner.stop();  // Stop 0 player reload task
        if (GameManager.getState() == GameState.COUNTDOWN) CountdownManager.stop();  // stop countdown task
        if (EndInventory.active) {
            EndInventory.task.cancel();
        }  // Stop endinventory task
        EndInventory.confirmed = false;
        GameManager.reset();
        SpawnManager.stop();
        PlayerDataManager.reset();
        ItemStacks.regenerate();
        ShopManager.generate();
        FeedbackInv.refresh();
        MapChooser.refresh();

        log("Reloading files [2/2]");
        JsonSingletons.initialize(GsonMode.ONLY_EXPOSE, getDataFolder().getPath());
        MapManager.loadNames();

        log("Running initialisation tasks [3/3]");
        PlayerManager.fix = true;
        ScoreboardManager.reload();
        for (Player player : getOnlinePlayers()) {
            player.setDisplayName(player.getName());
            PlayerManager.fixPlayer(player, false);
            player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 200, 1);
            MessageCreator.sendTitle(player, "&aWelcome back", MessageCreator.gameName() + "&7 has been reconfigured!", 50);
        }
        if (PlayerManager.fix && Bukkit.getOnlinePlayers().size() > 0) {
            WorldBorderManager.forceUpdate();
            PlayerManager.fix = false;
        }
        for (Entity entity : BedWars.getWorld().getEntities()) {
            if (entity.getType() == EntityType.DROPPED_ITEM) {
                entity.remove();
            }
        }
        long finish = System.nanoTime();
        long duration = (finish - start) / 1000000;
        logger.info("Reload complete in T+" + duration + "ms.");
    }

    private void register() {
        PluginManager pluginManager = Bukkit.getPluginManager();
        pluginManager.registerEvents(new BlockListeners(), this);
        pluginManager.registerEvents(new VoidListener(), this);
        pluginManager.registerEvents(new FoodListener(), this);
        pluginManager.registerEvents(new PlayerDataManager(), this);
        pluginManager.registerEvents(new PlayerManager(), this);
        pluginManager.registerEvents(new ScoreboardManager(), this);
        pluginManager.registerEvents(new EntityListeners(), this);
        pluginManager.registerEvents(new ChatFormatter(), this);

        getCommand("error").setExecutor(new ErrorCommand());

        getCommand("endgame").setExecutor(new EndCommand());
        getCommand("start").setExecutor(new SkipCommand());
        getCommand("build").setExecutor(new BuildCommand());

        getCommand("shop").setExecutor(new ShopCommand());
        getCommand("auto").setExecutor(new AutoCommand());
        getCommand("auto").setTabCompleter(new AutoCommand());
        getCommand("load").setExecutor(new LoadCommand());
        getCommand("save").setExecutor(new SaveCommand());

        getCommand("r").setExecutor(new RCommand());
        getCommand("fix").setExecutor(new FixCommand());
        getCommand("quickstart").setExecutor(new QStartCommand());
        getCommand("quickstart").setTabCompleter(new ShopCommand());
        getCommand("dev").setExecutor(new DevCommand());
        getCommand("dev").setTabCompleter(new DevCommand());
    }
}
