package link.ignyte.plugins.bedwars.Feedback;

import link.ignyte.plugins.bedwars.Inventories.BetterItem.BetterItem;
import link.ignyte.plugins.bedwars.Inventories.BetterItem.BetterItemDescription;
import link.ignyte.plugins.bedwars.Inventories.InvOpener;
import link.ignyte.plugins.bedwars.Inventories.InventoryHandeler;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;

import java.util.Arrays;

public class FeedbackInv {

    public static Inventory feedback_inv;

    public static void refresh() {
        feedback_inv = InventoryHandeler.createInventory("&aHow did you enjoy " + MessageCreator.gameName() + "&a?");

        feedback_inv.setItem(19, new BetterItem((event) -> {
            Feedback.addFeedback(1);
            event.getPlayer().sendMessage(MessageCreator.t("&7[&aFeedback&7] Thank you for your feedback!"));
            InvOpener.closeDelay(event.getPlayer());
            return true;
        }, Material.LIME_WOOL).setName("&aIt was very good").create(new BetterItemDescription("Vote", Arrays.asList("Click if you think that", "the overall " + MessageCreator.gameName() + " &7experience was", "&avery good.", "", "Ratings like this: &b" + Feedback.get(Feedback.class).getAwesome()))));

        feedback_inv.setItem(21, new BetterItem((event) -> {
            Feedback.addFeedback(2);
            event.getPlayer().sendMessage(MessageCreator.t("&7[&aFeedback&7] Thank you for your feedback!"));
            InvOpener.closeDelay(event.getPlayer());
            return true;
        }, Material.GREEN_WOOL).setName("&aIt good, &eit could be better").create(new BetterItemDescription("Vote", Arrays.asList("Click if you think that", "the overall " + MessageCreator.gameName() + " &7experience was", "&agood &cbut&e there are some improvements to be made", "", "Ratings like this: &b" + Feedback.get(Feedback.class).getGreat()))));

        feedback_inv.setItem(23, new BetterItem((event) -> {
            Feedback.addFeedback(3);
            event.getPlayer().sendMessage(MessageCreator.t("&7[&aFeedback&7] Thank you for your feedback!"));
            InvOpener.closeDelay(event.getPlayer());
            return true;
        }, Material.YELLOW_WOOL).setName("&eIt was OK").create(new BetterItemDescription("Vote", Arrays.asList("Click if you think that", "the overall " + MessageCreator.gameName() + " &7experience was", "&eOK.", "", "Ratings like this: &b" + Feedback.get(Feedback.class).getOkay()))));
        feedback_inv.setItem(25, new BetterItem((event) -> {
            Feedback.addFeedback(4);
            event.getPlayer().sendMessage(MessageCreator.t("&7[&aFeedback&7] Thank you for your feedback!"));
            InvOpener.closeDelay(event.getPlayer());
            return true;
        }, Material.RED_WOOL).setName("&cIt was horrible").create(new BetterItemDescription("Vote", Arrays.asList("Click if you think that", "the overall " + MessageCreator.gameName() + " &7experience was", "&chorrible.", "", "Ratings like this: &b" + Feedback.get(Feedback.class).getVery_bad()))));
    }

    public static Inventory getFeedbackInv() {
        return feedback_inv;
    }


    public static void update(int enchant) {
        switch (enchant) {
            case 1 -> feedback_inv.setItem(19, new BetterItem((event) -> {
                Feedback.addFeedback(1);
                event.getPlayer().sendMessage(MessageCreator.t("&7[&aFeedback&7] Thank you for your feedback!"));
                InvOpener.closeDelay(event.getPlayer());
                return true;
            }, Material.LIME_WOOL).setName("&aIt was very good").setGlint(true).create(new BetterItemDescription("Vote", Arrays.asList("Click if you think that", "the overall " + MessageCreator.gameName() + " &7experience was", "&avery good.", "", "Ratings like this: &b" + Feedback.get(Feedback.class).getAwesome()))));
            case 2 -> feedback_inv.setItem(21, new BetterItem((event) -> {
                Feedback.addFeedback(2);
                event.getPlayer().sendMessage(MessageCreator.t("&7[&aFeedback&7] Thank you for your feedback!"));
                InvOpener.closeDelay(event.getPlayer());
                return true;
            }, Material.GREEN_WOOL).setName("&aIt good, &eit could be better").setGlint(true).create(new BetterItemDescription("Vote", Arrays.asList("Click if you think that", "the overall " + MessageCreator.gameName() + " &7experience was", "&agood &cbut&e there are some improvements to be made", "", "Ratings like this: &b" + Feedback.get(Feedback.class).getGreat()))));
            case 3 -> feedback_inv.setItem(23, new BetterItem((event) -> {
                Feedback.addFeedback(3);
                event.getPlayer().sendMessage(MessageCreator.t("&7[&aFeedback&7] Thank you for your feedback!"));
                InvOpener.closeDelay(event.getPlayer());
                return true;
            }, Material.YELLOW_WOOL).setName("&eIt was OK").setGlint(true).create(new BetterItemDescription("Vote", Arrays.asList("Click if you think that", "the overall " + MessageCreator.gameName() + " &7experience was", "&eOK.", "", "Ratings like this: &b" + Feedback.get(Feedback.class).getOkay()))));
            case 4 -> feedback_inv.setItem(25, new BetterItem((event) -> {
                Feedback.addFeedback(4);
                event.getPlayer().sendMessage(MessageCreator.t("&7[&aFeedback&7] Thank you for your feedback!"));
                InvOpener.closeDelay(event.getPlayer());

                return true;
            }, Material.RED_WOOL).setName("&cIt was horrible").setGlint(true).create(new BetterItemDescription("Vote", Arrays.asList("Click if you think that", "the overall " + MessageCreator.gameName() + " &7experience was", "&chorrible.", "", "Ratings like this: &b" + Feedback.get(Feedback.class).getVery_bad()))));
        }

    }
}
