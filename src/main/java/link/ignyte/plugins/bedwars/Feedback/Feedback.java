package link.ignyte.plugins.bedwars.Feedback;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import link.ignyte.plugins.bedwars.Inventories.InvOpener;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import link.ignyte.utils.JsonSingleton.JsonPath;
import link.ignyte.utils.JsonSingleton.JsonSerializable;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

@JsonPath("feedback.json")
public class Feedback extends JsonSerializable {
    @Expose
    @SerializedName(value = "awesome", alternate = {"lime"})
    private int awesome;
    @Expose
    @SerializedName(value = "great", alternate = {"green"})
    private int great;
    @Expose
    @SerializedName(value = "okay", alternate = {"yellow"})
    private int okay;
    @Expose
    @SerializedName(value = "very_bad", alternate = {"red"})
    private int very_bad;


    public Feedback(boolean generateDefault) {
        if (generateDefault) {
            awesome = 0;
            great = 0;
            okay = 0;
            very_bad = 0;
        }
    }

    public static void addFeedback(int feedback) {
        switch (feedback) {
            case 1 -> get(Feedback.class).awesome++;
            case 2 -> get(Feedback.class).great++;
            case 3 -> get(Feedback.class).okay++;
            case 4 -> get(Feedback.class).very_bad++;
        }
        FeedbackInv.update(feedback);
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (player.getOpenInventory().getTitle().equals(MessageCreator.t("&aHow did you enjoy " + MessageCreator.gameName() + "&a?"))) {
                InvOpener.openDelay(player, FeedbackInv.getFeedbackInv());
            }
        }
    }

    public int getAwesome() {
        return awesome;
    }

    public int getGreat() {
        return great;
    }

    public int getOkay() {
        return okay;
    }

    public int getVery_bad() {
        return very_bad;
    }
}

