package link.ignyte.plugins.bedwars.Management;

import link.ignyte.plugins.bedwars.Inventories.InvOpener;
import link.ignyte.plugins.bedwars.Inventories.Setup.SetupInventories;
import link.ignyte.plugins.bedwars.Maps.ClearState;
import link.ignyte.plugins.bedwars.Maps.GameMap;
import link.ignyte.plugins.bedwars.Maps.MapManager;
import link.ignyte.plugins.bedwars.Maps.Teams.Team;
import link.ignyte.plugins.bedwars.Players.PlayerData;
import link.ignyte.plugins.bedwars.Players.PlayerDataManager;
import link.ignyte.plugins.bedwars.Utils.Advancements;
import link.ignyte.plugins.bedwars.Utils.Features.EnderChestManager;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import link.ignyte.plugins.bedwars.Utils.Options;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class GameManager {

    public static GameMap map = null;
    private static GameState state = GameState.SETUP;
    private static PlayType playType = null;

    public static GameMap getMap() {
        return map;
    }

    public static void setGameMap(GameMap toSet) {
        map = toSet;
        ScoreboardManager.now();
        StatusMessenger.update();
    }

    public static GameState getState() {
        return state;
    }

    public static void setState(GameState state) {
        GameManager.state = state;
        ScoreboardManager.now();
        StatusMessenger.update();
    }

    public static PlayType getPlayType() {
        return playType;
    }

    public static void setPlayType(PlayType playType) {
        GameManager.playType = playType;
        ScoreboardManager.now();
        StatusMessenger.update();
    }

    public static void modeSelectComplete() {
        StatusMessenger.update();
        if (playType == PlayType.EDIT_LOBBY) {
            state = GameState.IN_GAME;
            for (Player player : Bukkit.getOnlinePlayers()) {
                Options.get(Options.class).spawn.teleport(player);
                player.setGameMode(GameMode.CREATIVE);
                player.sendTitle(MessageCreator.t("&6Editing the lobby"), MessageCreator.t("&aHave fun!"), 10, 150, 20);
                player.getInventory().clear();
            }
        } else {
            state = GameState.MAP_CHOOSE;
            for (Player player : Bukkit.getOnlinePlayers()) {
                if (player.getOpenInventory().getTitle().endsWith("setup")) {
                    InvOpener.openDelay(player, SetupInventories.setupInv(player));
                }
            }
        }
    }

    public static void reset() {
        map = null;
        state = GameState.SETUP;
        playType = null;
    }

    public static void startGame() {

        // Check teams
        if (!Options.get(Options.class).dev_mode) {
            int alive_teams = 0;
            Team lastTeam = null;
            for (Team team : map.teamManager.teams) {
                if (team.memberCount() == 0) {
                    team.respawnBlock.destroyed = true;
                } else {
                    alive_teams++;
                    lastTeam = team;
                }
            }
            if (alive_teams == 1) {
                // End game
                GameRunner.end(lastTeam);
                return;
            }
        }

        state = GameState.IN_GAME;
        map.bbox.state = ClearState.NOT_CLEARED;
        MapManager.save(MapManager.class);
        for (Player player : Bukkit.getOnlinePlayers()) {
            PlayerData data = PlayerDataManager.getData(player);
            if (data.getTeam() == null) {
                player.setGameMode(GameMode.SPECTATOR);
                player.getInventory().clear();
                map.locs.spectator_spawn.teleport(player);
                if (Options.get(Options.class).put_spectators_into_teams)
                    MessageCreator.sendTitle(player, "&7Spectator", "&aOnce somebody has left, you will fill the space", 60);
                else MessageCreator.sendTitle(player, "&7Spectator", "", 60);
                continue;
            }
            data.played = true;
            Vector dir = GameManager.getMap().bbox.getMiddle().getLocation().add(0, data.getTeam().getSpawn().getY(), 0).toVector().subtract(data.getTeam().getSpawn().getLocationNoYawPitch().toVector());
            player.teleport(data.getTeam().getSpawn().getCenter().setDirection(dir));
            MessageCreator.sendTitle(player, "&aStart", MessageCreator.gameName() + " &7by IgynteLabs", 30);
            player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 0.1f);
            player.getInventory().clear();
            player.setGameMode(GameMode.SURVIVAL);
            player.setDisplayName(MessageCreator.t(data.getTeam().col.teamName + "&7 | " + player.getName() + "&r"));
            player.setFallDistance(0);
            player.setHealth(20);
            player.setFoodLevel(20);
            Advancements.award(player, "bedwars/root/play");
        }
        EnderChestManager.start();
        WorldBorderManager.forceUpdate();
        SpawnManager.startGame();
        GameRunner.startManager();
        StatusMessenger.update();
        ScoreboardManager.now();
    }
}
