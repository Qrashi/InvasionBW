package link.ignyte.plugins.bedwars.Management;

import link.ignyte.plugins.bedwars.BedWars;
import link.ignyte.plugins.bedwars.Maps.GameMap;
import link.ignyte.plugins.bedwars.Maps.Teams.Team;
import link.ignyte.plugins.bedwars.Players.PeresistentPlayerData;
import link.ignyte.plugins.bedwars.Players.PlayerData;
import link.ignyte.plugins.bedwars.Players.PlayerManager;
import link.ignyte.plugins.bedwars.Utils.Advancements;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;

public class GameRunner {

    public static boolean running = false;
    private static GameManager gameManager;
    private static BukkitRunnable task;
    private static GameMap map;

    public static void startManager() {
        running = true;
        map = GameManager.getMap();
        task = new BukkitRunnable() {
            @Override
            public void run() {
                if (Bukkit.getOnlinePlayers().size() == 0) {
                    BedWars.getInstance().reload();
                    cancel();
                }
            }
        };
        task.runTaskTimer(BedWars.getInstance(), 0, 5);
    }

    public static void stop() {
        if (!running) return;
        task.cancel();
        running = false;
    }

    public static void die(Player player) {
        // Should ONLY handle rewarding opponent player and checking if game should end
        // The rest should be done by PlayerManager

        PlayerData data = PlayerData.get(player);
        data.current_deaths++;
        PeresistentPlayerData.PeresistentData peresistentData = PeresistentPlayerData.get(player);
        if (peresistentData.deaths == 99) {
            Advancements.award(player, "bedwars/root/play/die/100");
        } else if (peresistentData.deaths == 0) {
            Advancements.award(player, "bedwars/root/play/die");
        }
        PeresistentPlayerData.get(player).deaths++;
        if (data.getTeam().respawnBlock.destroyed) {
            // Player shouldn't respawn
            data.getTeam().leave(player);
            data.setTeam(null);
            int teamsWithMoreThanOnePlayer = 0;
            Team lastTeam = null;
            for (Team team : GameManager.getMap().teamManager.teams) {
                if (!team.respawnBlock.destroyed) {
                    teamsWithMoreThanOnePlayer++;
                    lastTeam = team;
                    continue;
                } // If respawn block is present, at least one ONLINE player is in the team
                if (team.memberCount() > 0) {  // if no respawn block is present NO OFFLINE players are present.
                    // No member in team
                    teamsWithMoreThanOnePlayer++;
                    lastTeam = team;
                }
            }
            if (teamsWithMoreThanOnePlayer == 0) {
                MessageCreator.sendWithPrefix("Uhm, it seems like there is nobody in a team and no winner team. Weird (lol)");
                BedWars.getInstance().reload();
                return;
            }
            if (teamsWithMoreThanOnePlayer == 1) {
                // End game
                end(lastTeam);
            } else { // Will get fixed by GameRunner.end
                PlayerManager.fixPlayer(player, true);
            }
        } else {
            // Just respawn
            PlayerManager.fixPlayer(player, false);
        }
    }

    public static void end(Team winnerTeam) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            Advancements.award(player, "bedwars/root/play/end");
            if (PlayerData.get(player).current_deaths == 0) {
                Advancements.award(player, "bedwars/root/play/end/nodeath");
            }
            if (PlayerData.get(player).getTeam() == null) {
                // Looser ;)
                MessageCreator.sendTitle(player, winnerTeam.col.teamName + "&a won!", "&7Well played!", 100, true);
            }
        }
        ArrayList<Player> winners = new ArrayList<>();
        winnerTeam.members().forEach(player -> {
            PeresistentPlayerData.PeresistentData peresistentData = PeresistentPlayerData.get(player);
            if (peresistentData.wins == 0) {
                Advancements.award(player, "bedwars/root/play/win");
            } else if (peresistentData.wins == 99) {
                Advancements.award(player, "bedwars/root/play/win/100");
            }
            peresistentData.wins++;

            winners.add(player);
            PlayerData.get(player).setTeam(null);
            if (player.isOnline()) {
                MessageCreator.sendTitle(player, "&6&k!&r&6 You won! &k!", "&7Well played!", 100, true);
                player.setGameMode(GameMode.SPECTATOR);
            }
            Advancements.award(player, "win");
        });
        GameManager.setState(GameState.END_PHASE);
        ScoreboardManager.now();
        EndCredits.end(winners);
    }
}
