package link.ignyte.plugins.bedwars.Management;

import link.ignyte.plugins.bedwars.BedWars;
import link.ignyte.plugins.bedwars.Inventories.Setup.MapSpectateManager;
import link.ignyte.plugins.bedwars.Maps.GameMap;
import link.ignyte.plugins.bedwars.Utils.Options;
import org.bukkit.WorldBorder;

public class WorldBorderManager {

    public static void forceUpdate() {
        WorldBorder wb = BedWars.getWorld().getWorldBorder();
        GameMap map = MapSpectateManager.getSpectating();
        if (map != null) {
            // Spectating map
            setToMap(wb, map);
            return;
        }
        if (GameManager.getState() == GameState.IN_GAME || GameManager.getState() == GameState.END_PHASE) {
            setToMap(wb, GameManager.getMap());
        } else {
            wb.setCenter(Options.get(Options.class).spawn.getLocationNoYawPitch());
            wb.setSize(Options.get(Options.class).spawn_worldborder_size);
            BedWars.getWorld().setTime(Options.get(Options.class).lobby_time);
        }
    }

    private static void setToMap(WorldBorder wb, GameMap map) {
        map.timeHolder.set();
        wb.setCenter(map.bbox.getMiddle().getLocation());
        wb.setSize(map.bbox.getMaxRadiusFromMiddle());
    }
}