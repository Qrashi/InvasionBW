package link.ignyte.plugins.bedwars.Management.Countdown;

import link.ignyte.plugins.bedwars.Inventories.BetterItem.BetterItem;
import link.ignyte.plugins.bedwars.Inventories.BetterItem.BetterItemDescription;
import link.ignyte.plugins.bedwars.Inventories.InvOpener;
import link.ignyte.plugins.bedwars.Inventories.InventoryHandeler;
import link.ignyte.plugins.bedwars.Management.GameManager;
import link.ignyte.plugins.bedwars.Management.StatusMessenger;
import link.ignyte.plugins.bedwars.Maps.Teams.Team;
import link.ignyte.plugins.bedwars.Players.PlayerData;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.List;

public class TeamGUI {

    private static final HashMap<Team, Inventory> inventories = new HashMap<>();
    private static int inventorySize = 0;
    private static ItemStack info;
    private static ItemStack close;

    public static void load() {
        refresh();
        inventorySize = 0;
        info = new BetterItem(new BetterItemDescription(
                "Click on a team (terracotta block) to join it!",
                List.of("&7This inventory will be &eupdated &7once somebody switches teams.")
        ), true)
                .setMaterial(Material.BOOK).setName("&3Information").create();
        close = new BetterItem(itemClickEvent -> {
            InvOpener.closeDelay(itemClickEvent.getPlayer());
            return true;
        }, Material.RED_STAINED_GLASS_PANE)
                .setName("&cx Close")
                .create();
    }

    private static void refresh() {
        StatusMessenger.update();
        inventories.clear();
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (player.getOpenInventory().getTitle().startsWith(MessageCreator.t("&aSelect a team"))) {
                InvOpener.openDelay(player, getTeamGUI(player));
            }
        }
    }


    public static Inventory getTeamGUI(Player player) {
        Team team = PlayerData.get(player).getTeam();
        if (inventories.containsKey(team)) {
            return inventories.get(team);
        }
        return createInventory(team);
    }

    public static Inventory createInventory(Team viewerTeam) {
        if (inventorySize == 0) {
            //Recalculate inventory size
            inventorySize = (GameManager.getMap().teamManager.getNumberOfTeams() / 9) * 9;
            if (GameManager.getMap().teamManager.getNumberOfTeams() % 9 != 0) inventorySize += 9;
            inventorySize += 18; // Top and lower bar
        }
        Inventory inv = InventoryHandeler.createInventory("&aSelect a team", inventorySize);
        inv.setItem(0, makeTeamItem(viewerTeam, true));
        inv.setItem(8, info);
        inv.setItem(inventorySize - 1, close);
        int slot = 9;
        for (Team team : GameManager.getMap().teamManager.teams) {
            inv.setItem(slot, makeTeamItem(team, team == viewerTeam));
            slot++;
        }
        return inv;
    }

    private static ItemStack makeTeamItem(Team team, boolean isViewerTeam) {
        BetterItem item = new BetterItem(itemClickEvent -> true, Material.RED_STAINED_GLASS_PANE);
        if (team == null) {
            // Selecor, no team selected
            return item.create(new BetterItemDescription("&cYou haven't joined any team yet!", "&aLeft click any team to join it"));
        } else {
            item.setGlint(isViewerTeam).setMaterial(team.col.gui_material).setName(team.col.teamName);
            if (team.freeSpace() == 0) {
                // Team is full
                BetterItemDescription desc = new BetterItemDescription("&b&n&lMembers:");
                if (isViewerTeam) {
                    desc.setOnLeft("&cLeave " + team.col.teamName);
                    item.setOnComplete(itemClickEvent -> {
                        GameManager.getMap().teamManager.leave(itemClickEvent.getPlayer());
                        refresh();
                        InvOpener.openDelay(itemClickEvent.getPlayer(), getTeamGUI(itemClickEvent.getPlayer()));
                        return true;
                    });
                }
                return item.create(desc.add(team.memberList()).add("&cThis team is &lfull!"));
            } else {
                BetterItemDescription desc = new BetterItemDescription("&b&n&lMembers:").add(team.memberList());
                if (isViewerTeam) {
                    item.setOnComplete(itemClickEvent -> {
                        GameManager.getMap().teamManager.leave(itemClickEvent.getPlayer());
                        refresh();
                        return true;
                    });
                    desc.setOnLeft("&cLeave " + team.col.teamName);
                } else {
                    item.setOnComplete(itemClickEvent -> {
                        GameManager.getMap().teamManager.leave(itemClickEvent.getPlayer());
                        GameManager.getMap().teamManager.join(team, itemClickEvent.getPlayer());
                        refresh();
                        return true;
                    });
                    desc.setOnLeft("&aJoin " + team.col.teamName);
                }
                return item.create(desc);
            }
        }
    }
}
