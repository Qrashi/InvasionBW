package link.ignyte.plugins.bedwars.Management;

public enum GameState {
    SETUP,
    MAP_CHOOSE,
    COUNTDOWN,
    IN_GAME,
    END_PHASE,
}
