package link.ignyte.plugins.bedwars.Management;

import link.ignyte.plugins.bedwars.BedWars;
import link.ignyte.plugins.bedwars.Inventories.Setup.MapSpectateManager;
import link.ignyte.plugins.bedwars.Management.Countdown.CountdownManager;
import link.ignyte.plugins.bedwars.Maps.GameMap;
import link.ignyte.plugins.bedwars.Maps.Teams.Team;
import link.ignyte.plugins.bedwars.Players.PlayerData;
import link.ignyte.plugins.bedwars.Players.PlayerDataManager;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import link.ignyte.plugins.bedwars.Utils.Options;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class StatusMessenger {

    public static void start() {
        new BukkitRunnable() {
            @Override
            public void run() {
                update();
            }
        }.runTaskTimer(BedWars.getPlugin(BedWars.class), 0, 20);
    }

    public static void update() {
        GameState gameState = GameManager.getState();
        PlayType playType = GameManager.getPlayType();
        switch (gameState) {
            case SETUP -> {
                if (playType == null)
                    sendPerms(MessageCreator.gameName() + "&7 is waiting for setup to complete.", "&aPlease start&7 the game using the &cbed &7item in your inventory!", "BedWars.gamehost");
                else
                    sendPerms(MessageCreator.gameName() + "&7 setup. &Current mode: " + playType.verb, "&aSelected mode: " + playType.verb + "&7 | Continue with the &bbed &7item in your inventory!", "BedWars.gamehost");
                sendHeader(MessageCreator.gameName() + "&7 is &astarting.");
            }
            case MAP_CHOOSE -> {
                sendHeader(MessageCreator.gameName() + "&7 is &astarting.");
                GameMap spectating = MapSpectateManager.getSpectating();
                if (spectating != null) {
                    sendPerms("&7Spectating " + spectating.name, "&7Spectating " + spectating.name + ". &cUse item to return", "BedWars.gamehost");
                    return;
                }
                sendPerms("&7The admins are choosing a map to &7" + playType.verb, "&7Please &aselect a map&7 to &a" + playType.verb + "!", "BedWars.gamehost");
            }
            case COUNTDOWN -> {
                sendHeader(MessageCreator.gameName() + "&7 is &astarting.");
                if (CountdownManager.getCountdown() < 10)
                    sendAll(MessageCreator.gameName() + "&a will start in " + CountdownManager.getCountdown() + " seconds.");
                else
                    sendPerms(MessageCreator.gameName() + "&a will start in " + CountdownManager.getCountdown() + " seconds.", "&aStarting in " + CountdownManager.getCountdown() + " seconds. &7Use &a&l/skip &r&7to skip.", "bedwars.gamehost");
            }
            case IN_GAME -> {
                sendHeader(MessageCreator.gameName());
                switch (playType) {
                    case BUILDING -> sendPerms("&7You are &eediting&7 " + GameManager.getMap().name, "&eEditing &7" + GameManager.getMap().name + ". Use &b/build&7 to access build menu", "BedWars.gamehost");
                    case EDIT_LOBBY -> sendPerms("&eEditing the lobby", "&eEditing the lobby. &7Use &c/endgame &7to reload", "bedwars.gamehost");
                    case PLAYING -> sendTeams();
                }
            }
            case END_PHASE -> {
                sendHeader(MessageCreator.gameName() + "&c will end soon.");
                sendPerms(MessageCreator.gameName() + "&c will reload in " + EndCredits.getTimeRemaining() + " seconds.", "&cReloading in " + EndCredits.getTimeRemaining() + " seconds. &7Use &a&l/skip&r&7 to skip", "bedwars.gamehost");
            }
        }
    }

    private static void sendTeams() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            PlayerData data = PlayerDataManager.getData(player);
            Team team = data.getTeam();
            if (team == null) {
                // Spectator
                if (Options.get(Options.class).put_spectators_into_teams) {
                    if (data.played) {
                        if (Options.get(Options.class).put_dead_players_into_teams) {
                            send("&7Spectator. &aYou will be put in a team with free slots.", player);
                            continue;
                        }
                    } else {
                        send("&7Spectator. &aYou will be put in a team with free slots.", player);
                        continue;
                    }
                }
                send("&7Spectator", player);
            } else {
                // Print team string
                String bed = "&a⬛ ";
                if (team.respawnBlock.destroyed) bed = "&c⬛ ";
                send(bed + team.col.teamName + " " + team.memberCount() + "/" + GameManager.getMap().teamManager.getTeamSize(), player);
            }
        }
    }

    public static void sendPerms(String normalPlayer, String opPlayer, String permission) {
        TextComponent normal = MessageCreator.generateComponent(normalPlayer);
        TextComponent perms = MessageCreator.generateComponent(opPlayer);
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (player.hasPermission(permission)) {
                player.spigot().sendMessage(ChatMessageType.ACTION_BAR, perms);
            } else {
                player.spigot().sendMessage(ChatMessageType.ACTION_BAR, normal);
            }

        }
    }

    public static void sendAll(String message) {
        TextComponent text = MessageCreator.generateComponent(message);
        Bukkit.getOnlinePlayers().forEach(player -> player.spigot().sendMessage(ChatMessageType.ACTION_BAR, text));
    }

    public static void send(String bar, Player toSendTo) {
        TextComponent toSend = MessageCreator.generateComponent(bar);
        toSendTo.spigot().sendMessage(ChatMessageType.ACTION_BAR, toSend);
    }

    public static void sendHeader(String header) {
        Bukkit.getOnlinePlayers().forEach(player -> player.setPlayerListHeaderFooter(MessageCreator.t("\n" + header + "\n&7Hosted on " + Options.get(Options.class).network_name + "\n"), MessageCreator.t("\n&calpha &7" + BedWars.version + "&8 report bugs using &e/bug")));
    }


}
