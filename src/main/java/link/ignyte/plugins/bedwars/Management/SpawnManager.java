package link.ignyte.plugins.bedwars.Management;

import link.ignyte.plugins.bedwars.BedWars;
import link.ignyte.plugins.bedwars.Maps.GameMap;
import link.ignyte.plugins.bedwars.Maps.Spawners.Spawner;
import link.ignyte.plugins.bedwars.Maps.Spawners.SpawnerType;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;

public class SpawnManager {

    private static final int bronzeRythm = 10;
    private static final int ironRythm = 200;
    private static final int goldRythm = 600;
    private static BukkitRunnable bronze;
    private static BukkitRunnable iron;
    private static BukkitRunnable gold;
    private static boolean running = false;

    public static void startGame() {
        running = true;
        GameMap map = GameManager.getMap();
        List<Spawner> bronzeList = map.locs.getSpawners(SpawnerType.BRONZE);
        List<Spawner> ironList = map.locs.getSpawners(SpawnerType.IRON);
        List<Spawner> goldList = map.locs.getSpawners(SpawnerType.GOLD);
        //bronze
        bronze = new BukkitRunnable() {
            @Override
            public void run() {
                for (Spawner spawner : bronzeList) {
                    spawner.spawn();
                }
            }
        };
        iron = new BukkitRunnable() {
            @Override
            public void run() {
                for (Spawner spawner : ironList) {
                    spawner.spawn();
                }
            }
        };
        gold = new BukkitRunnable() {
            @Override
            public void run() {
                for (Spawner spawner : goldList) {
                    spawner.spawn();
                }
            }
        };
        bronze.runTaskTimer(BedWars.getInstance(), 0, bronzeRythm);
        iron.runTaskTimer(BedWars.getInstance(), 0, ironRythm);
        gold.runTaskTimer(BedWars.getInstance(), 0, goldRythm);
    }

    public static void stop() {
        if (!running) return;
        bronze.cancel();
        iron.cancel();
        gold.cancel();
        running = false;
    }
}
