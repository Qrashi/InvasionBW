package link.ignyte.plugins.bedwars.Management;

import link.ignyte.plugins.bedwars.BedWars;
import link.ignyte.plugins.bedwars.Feedback.FeedbackInv;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import link.ignyte.plugins.bedwars.Utils.Options;
import link.ignyte.plugins.bedwars.Utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;

public class EndCredits {

    public static int remaining;
    private static BukkitRunnable end_task;

    public static void forceEnd() {
        end_task.cancel();
        BedWars.getInstance().reload();
    }

    public static void end(ArrayList<Player> spawnRockets) {
        remaining = 20;
        Bukkit.broadcastMessage(MessageCreator.t("&aThanks for playing " + MessageCreator.gameName() + "!\n&aWe really hope you enjoyed it."));
        for (Player player : Bukkit.getOnlinePlayers()) {
            //player.performCommand("credits");
            player.setGameMode(GameMode.SPECTATOR);
        }
        final int[] playerIndex = {0};
        end_task = new BukkitRunnable() {
            @Override
            public void run() {
                if (remaining == 0) {
                    BedWars.getInstance().reload();
                    cancel();
                    return;
                }
                if (remaining == 10) for (Player player : Bukkit.getOnlinePlayers())
                    MessageCreator.sendTitle(player, MessageCreator.gameName(), "&ahosted on " + Options.get(Options.class).network_name, 1000, false);
                if (remaining == 7) for (Player player : Bukkit.getOnlinePlayers())
                    MessageCreator.sendTitle(player, MessageCreator.gameName(), "&emade by &bIgnyte&5Labs", 1000, false);
                if (remaining == 5) for (Player player : Bukkit.getOnlinePlayers())
                    MessageCreator.sendTitle(player, "&aSpecial &7Thanks to", "&7github/WesJD/AnvilGUI", 60, false);
                if (remaining == 4) for (Player player : Bukkit.getOnlinePlayers())
                    MessageCreator.sendTitle(player, "&aSpecial &7Thanks to", "&7github/trigary/AdvancementCreator", 60, false);
                if (remaining == 3) for (Player player : Bukkit.getOnlinePlayers())
                    MessageCreator.sendTitle(player, "&aSpecial &7Thanks to", "&7github/JordanOsterberg/JScoreboards", 60, false);
                if (remaining == 2) for (Player player : Bukkit.getOnlinePlayers())
                    MessageCreator.sendTitle(player, "&aSpecial &7Thanks to", "&7github/ronmamo/reflections", 60, false);
                if (remaining == 1) for (Player player : Bukkit.getOnlinePlayers())
                    MessageCreator.sendTitle(player, "&creloading", "&7please wait", 60, false);
                if (remaining == 15) {
                    for (Player player : Bukkit.getOnlinePlayers()) {
                        player.openInventory(FeedbackInv.getFeedbackInv());
                    }
                }
                Utils.spawnFirework(spawnRockets.get(playerIndex[0]));
                playerIndex[0] = (playerIndex[0] + 1) % spawnRockets.size();
                remaining--;
                ScoreboardManager.now();
                StatusMessenger.update();
            }
        };
        end_task.runTaskTimer(BedWars.getInstance(), 0, 20);
    }

    public static int getTimeRemaining() {
        return remaining;
    }
}
