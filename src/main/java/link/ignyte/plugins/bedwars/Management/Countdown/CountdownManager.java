package link.ignyte.plugins.bedwars.Management.Countdown;

import link.ignyte.plugins.bedwars.BedWars;
import link.ignyte.plugins.bedwars.Management.GameManager;
import link.ignyte.plugins.bedwars.Management.ScoreboardManager;
import link.ignyte.plugins.bedwars.Management.StatusMessenger;
import link.ignyte.plugins.bedwars.Maps.Teams.Team;
import link.ignyte.plugins.bedwars.Maps.Teams.TeamManager;
import link.ignyte.plugins.bedwars.Players.PlayerData;
import link.ignyte.plugins.bedwars.Players.PlayerDataManager;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import link.ignyte.plugins.bedwars.Utils.error.*;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Comparator;
import java.util.List;

public class CountdownManager {

    private static int countdown = 0;
    private static int playersNeeded = 0;
    private static int maxPlayers = 0;
    private static boolean forced = false;
    private static int notEnoughPlayersMessage = 0;
    private static BukkitRunnable task;

    public static void startCountdown() {
        StatusMessenger.update();
        countdown = 180;
        TeamManager teamManager = GameManager.getMap().teamManager;
        playersNeeded = teamManager.getTeamSize() + 1;
        maxPlayers = teamManager.getNumberOfTeams() * teamManager.getTeamSize();
        forced = false;
        notEnoughPlayersMessage = 10;
        task = new BukkitRunnable() {
            @Override
            public void run() {
                if (countdown > 60 && Bukkit.getOnlinePlayers().size() >= playersNeeded) {
                    MessageCreator.sendWithPrefix("" + MessageCreator.gameName() + "&a will start in &l60&a seconds.");
                    countdown = 60;
                }
                if (countdown > 10 && Bukkit.getOnlinePlayers().size() >= maxPlayers) {
                    countdown = 10;
                }
                if (teamManager.getNumberOfTeams() > 2) {
                    if (countdown > 20 && Bukkit.getOnlinePlayers().size() >= maxPlayers - teamManager.getTeamSize()) {
                        MessageCreator.sendWithPrefix("" + MessageCreator.gameName() + "&a will start in &l20&a seconds.");
                        countdown = 20;
                    }
                    if (countdown > 10 && Bukkit.getOnlinePlayers().size() >= maxPlayers - (teamManager.getTeamSize() / 2)) {
                        MessageCreator.sendWithPrefix("" + MessageCreator.gameName() + "&a will start in &l10&a seconds.");
                        countdown = 10;
                    }
                }
                if (Bukkit.getOnlinePlayers().size() < playersNeeded && !forced) {
                    countdown = 181;
                    if (notEnoughPlayersMessage == 10) {
                        MessageCreator.sendWithPrefix("&cThere are not enough players to start...");
                        notEnoughPlayersMessage = 0;
                    }
                    notEnoughPlayersMessage++;
                } else {
                    notEnoughPlayersMessage = 10;
                }
                if (countdown < 10) {
                    playSound();
                }
                countdown--;
                ScoreboardManager.now();
                StatusMessenger.update();
                for (Player player : Bukkit.getOnlinePlayers()) {
                    player.setLevel(countdown);
                    player.setExp(0);
                }
                if (countdown == 9) {
                    for (Player player : Bukkit.getOnlinePlayers()) {
                        player.getInventory().clear();
                        player.closeInventory();
                    }
                    joinAll();
                }
                if (countdown == 5) {
                    GameManager.getMap().teamManager.redoBeds();
                    for (Entity entity : BedWars.getWorld().getEntities()) {
                        if (entity.getType() == EntityType.DROPPED_ITEM) {
                            entity.remove();
                        }
                    }
                } else if (countdown == 1) {
                    for (Player player : Bukkit.getOnlinePlayers()) {
                        MessageCreator.sendTitle(player, "&a1", "", 20);
                    }
                } else if (countdown == 0) {
                    countdown = 1;
                    GameManager.startGame();
                    MessageCreator.sendWithPrefix("&cPlease note that the plugin is currently in alpha phase!");
                    cancel();
                    return;
                } else if (countdown == 3) {
                    for (Player player : Bukkit.getOnlinePlayers()) {
                        MessageCreator.sendTitle(player, "&c3", "", 30);
                    }
                } else if (countdown == 2) {
                    for (Player player : Bukkit.getOnlinePlayers()) {
                        MessageCreator.sendTitle(player, "&e2", "", 30);
                    }
                }
            }
        };
        task.runTaskTimer(BedWars.getInstance(), 0, 20);
    }

    public static boolean skip() {
        if (!forced && countdown > 10) {
            forced = true;
            countdown = 10;
            return true;
        }
        return false;
    }

    public static int getCountdown() {
        return countdown;
    }

    public static void joinAll() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            PlayerData data = PlayerDataManager.getData(player);
            if (data.getTeam() == null) {
                putIntoBestTeam(player);
            }
        }
    }

    private static void putIntoBestTeam(Player player) {
        List<Team> teams = GameManager.getMap().teamManager.getTeamList();
        teams.sort(Comparator.comparingInt(Team::memberCount));
        Team team = teams.get(0); //First team has the least amount of players
        if (team.hasSpace()) {
            PlayerDataManager.getData(player).setTeam(team);
            team.join(player);
            player.sendMessage(MessageCreator.withPrefix("&aYou joined " + team.col.teamName));
        } else {
            ErrorHandeler.error(new ErrorCode(ErrorEffect.CORE, ErrorSeverity.HIGH, ErrorRelation.MISC_EVENT, "CountMan:startGame:bestTeam=full"));
            player.kickPlayer("&cA critical error has occcured. No space left for you. We are sorry for the inconvenience.");
        }
    }

    private static void playSound() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1000, 1);
        }
    }

    public static void stop() {
        task.cancel();
    }
}
