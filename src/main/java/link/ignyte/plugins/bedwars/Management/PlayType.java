package link.ignyte.plugins.bedwars.Management;

public enum PlayType {
    BUILDING("build", "&aBuildMode"),
    EDIT_LOBBY("edit lobby", "&aLobbyEdit"),
    PLAYING("play", "&aPlay BedWars");

    public final String verb;
    public final String action;

    PlayType(final String verb, String action) {
        this.verb = verb;
        this.action = action;
    }
}
