package link.ignyte.plugins.bedwars.Management;

import dev.jcsoftware.jscoreboards.JPerPlayerScoreboard;
import link.ignyte.plugins.bedwars.Inventories.Setup.MapSpectateManager;
import link.ignyte.plugins.bedwars.Management.Countdown.CountdownManager;
import link.ignyte.plugins.bedwars.Maps.Teams.Team;
import link.ignyte.plugins.bedwars.Players.PlayerData;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ScoreboardManager implements Listener {

    private static JPerPlayerScoreboard scoreboard = new JPerPlayerScoreboard(player -> getTitle(), ScoreboardManager::getContent);

    public static void unregister() {
        Bukkit.getOnlinePlayers().forEach(scoreboard::removePlayer);
    }

    public static void reload() {
        Bukkit.getOnlinePlayers().forEach(scoreboard::removePlayer);
        scoreboard = new JPerPlayerScoreboard(player -> getTitle(), ScoreboardManager::getContent);
        Bukkit.getOnlinePlayers().forEach(scoreboard::addPlayer);
    }

    public static void addAll() {
        Bukkit.getOnlinePlayers().forEach(scoreboard::addPlayer);
    }

    private static String getTitle() {
        switch (GameManager.getState()) {
            case MAP_CHOOSE:
            case SETUP:
                return MessageCreator.gameName() + "&7 | Setup";
            case COUNTDOWN:
                return MessageCreator.gameName() + " &7| Coundown";
            case END_PHASE:
            case IN_GAME: {
                return switch (GameManager.getPlayType()) {
                    case PLAYING -> MessageCreator.gameName();
                    case BUILDING -> "&aBuildMode";
                    case EDIT_LOBBY -> "&eLobbyEdit";
                };
            }
        }
        return MessageCreator.gameName() + "&7 | loading"; // Should NOT happen!
    }

    private static List<String> getContent(Player player) {
        switch (GameManager.getState()) {
            case SETUP: {
                List<String> content = new ArrayList<>(Arrays.asList("", "&7Setting up...", ""));
                if (player.hasPermission("bedwars.gamehost"))
                    content.addAll(Arrays.asList("&aPlease start setup", "&ausing the &cbed &aitem."));
                else content.add("&7Please wait...");
                content.add("");
                content.add("&6Current mode:");
                if (GameManager.getPlayType() == null) content.add("&cNo mode selected");
                else content.add(GameManager.getPlayType().action);
                content.add("");
                content.add("&7Use &c/bug &7to report &bbugs");
                return new ArrayList<>(content);
            }
            case MAP_CHOOSE: {
                if (MapSpectateManager.getSpectating() == null) {
                    List<String> content = new ArrayList<>(Arrays.asList("", "&7Setting up...", ""));
                    if (player.hasPermission("bedwars.gamehost"))
                        content.addAll(Arrays.asList("&aPlease start setup", "&ausing the &cbed &aitem."));
                    else content.add("&7Please wait...");
                    content.addAll(Arrays.asList("", "&6Current mode:", GameManager.getPlayType().action, "", "&aSelected map:"));
                    if (GameManager.getMap() == null) content.add("&cNone");
                    else {
                        content.add(GameManager.getMap().name);
                        content.add("&b" + GameManager.getMap().teamManager.getNumberOfTeams() + "x" + GameManager.getMap().teamManager.getTeamSize() + " players");
                    }
                    content.add("");
                    content.add("&7Use &c/bug &7to report &bbugs");
                    return new ArrayList<>(content);
                } else {
                    List<String> content = new ArrayList<>(Arrays.asList("", "&7Spectating map", MapSpectateManager.getSpectating().name, "&b" + MapSpectateManager.getSpectating().teamManager.getNumberOfTeams() + "x" + MapSpectateManager.getSpectating().teamManager.getTeamSize()));
                    if (player.hasPermission("bedwars.gamehost"))
                        content.addAll(Arrays.asList("&cUse the glass", "&cto returm to lobby", ""));
                    else content.add("");
                    content.add("&7Use &c/bug &7to report &bbugs");
                    return new ArrayList<>(content);
                }
            }
            case COUNTDOWN: {
                // PlayType HAS to be PLAY
                List<String> content = new ArrayList<>(Arrays.asList("", "&6Playing on", GameManager.getMap().name + "&7 (" + GameManager.getMap().teamManager.getNumberOfTeams() + "x" + GameManager.getMap().teamManager.getTeamSize() + ")", "", "&7Starting in &a" + CountdownManager.getCountdown() + "s", ""));
                if (PlayerData.get(player).getTeam() == null) {
                    content.add("&cYou haven't joined a team yet!");
                } else {
                    content.addAll(Arrays.asList("&dYour team:", PlayerData.get(player).getTeam().col.teamName));
                }
                content.add("");
                for (Team team : GameManager.getMap().teamManager.teams) {
                    content.add("&f- " + team.col.teamName + " &7(&a" + team.memberCount() + "&7/&c" + GameManager.getMap().teamManager.getTeamSize() + "&7)");
                }
                content.add("");
                content.add("&7Use &c/bug &7to report &bbugs");
                return new ArrayList<>(content);
            }
            case IN_GAME: {
                switch (GameManager.getPlayType()) {
                    case EDIT_LOBBY -> {
                        if (player.hasPermission("bedwars.gamehost"))
                            return new ArrayList<>(Arrays.asList("", "&eEditing the lobby", "", "&cUse &c/endgame", "&cend the game.", "", "&7Use &c/bug &7to report &bbugs"));
                        else
                            return new ArrayList<>(Arrays.asList("", "&eEditing the lobby", "", "&aHave fun!", "", "&7Use &c/bug &7to report &bbugs"));
                    }
                    case BUILDING -> {
                        List<String> content = new ArrayList<>(Arrays.asList("", "&eEditing map", GameManager.getMap().name, ""));
                        if (player.hasPermission("bedwars.gamehost"))
                            content.addAll(Arrays.asList("&7Use &e/build&7 for options", "&a&lFinish&r&a &7building using &e/build", "", "&cUse /endgame to end"));
                        else
                            content.addAll(Arrays.asList("&bMap details:", "&f- &7Time: " + GameManager.getMap().timeHolder.getTime(), "&f- &a" + GameManager.getMap().teamManager.getNumberOfTeams() + " teams"));
                        content.add("");
                        content.add("&7Use &c/bug &7to report &bbugs");
                        return new ArrayList<>(content);
                    }
                    case PLAYING -> {
                        List<String> content = new ArrayList<>(Arrays.asList("", "&aPlaying on", "&7" + GameManager.getMap().name, ""));
                        for (Team team : GameManager.getMap().teamManager.teams) {
                            content.add(team.respawnBlock.getStatus() + " " + team.col.teamName + " &7[&a" + team.memberCount() + "&7/&c" + GameManager.getMap().teamManager.getTeamSize() + "&7] ");
                        }
                        content.add("");
                        content.add("&7Use &c/bug &7to report &bbugs");
                        return new ArrayList<>(content);
                    }
                }
            }
            case END_PHASE: {
                return new ArrayList<>(Arrays.asList("", "&aThanks for playing", MessageCreator.gameName() + "!", "", "&7Reloading in &a" + EndCredits.getTimeRemaining() + "&7s", "", "&7Use &c/bug &7to report bugs."));
            }
        }
        return new ArrayList<>(Arrays.asList("", "&cAn error occured.", "&7Use &c/bug &7to report.", "CODE: scb_no_content", ""));
    }

    public static void now() {
        scoreboard.updateScoreboard();
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        scoreboard.addPlayer(event.getPlayer());
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        scoreboard.removePlayer(event.getPlayer());
    }
}
