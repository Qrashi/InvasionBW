package link.ignyte.plugins.bedwars.Players;

import link.ignyte.plugins.bedwars.Inventories.Setup.MapChooser;
import link.ignyte.plugins.bedwars.Management.GameManager;
import link.ignyte.plugins.bedwars.Management.PlayType;
import link.ignyte.plugins.bedwars.Maps.Teams.Team;
import org.bukkit.entity.Player;

public class PlayerData {

    public MapChooser.Query query;
    public boolean played;
    private int page;
    private Team team;
    private boolean exclude_unfinished;
    public int current_deaths;

    public PlayerData() {
        query = new MapChooser.Query();
        page = 0;
        team = null;
        exclude_unfinished = true;
        current_deaths = 0;
    }

    public static PlayerData get(Player player) {
        return PlayerDataManager.getData(player);
    }

    public boolean isExclude_unfinished() {
        if (GameManager.getPlayType() == PlayType.PLAYING) {
            return exclude_unfinished;
        }
        return false;
    }

    public void toggle_exclude_unfinished() {
        exclude_unfinished = !exclude_unfinished;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }
}
