package link.ignyte.plugins.bedwars.Players;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.HashMap;
import java.util.UUID;

public class PlayerDataManager implements Listener {

    private static final HashMap<UUID, PlayerData> playerData = new HashMap<>();

    public static PlayerData getData(Player player) {
        if (!playerData.containsKey(player.getUniqueId())) playerData.put(player.getUniqueId(), new PlayerData());
        return playerData.get(player.getUniqueId());
    }

    public static void reset() {
        playerData.clear();
        for (Player player : Bukkit.getOnlinePlayers()) {
            playerData.put(player.getUniqueId(), new PlayerData());
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onJoin(PlayerJoinEvent event) {
        if (!playerData.containsKey(event.getPlayer().getUniqueId())) {
            playerData.put(event.getPlayer().getUniqueId(), new PlayerData());
        }
        if (!PeresistentPlayerData.get(PeresistentPlayerData.class).player_data.containsKey(event.getPlayer().getUniqueId())) {
            PeresistentPlayerData.get(PeresistentPlayerData.class).player_data.put(event.getPlayer().getUniqueId(), new PeresistentPlayerData.PeresistentData());
        }
    }

}
