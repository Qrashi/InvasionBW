package link.ignyte.plugins.bedwars.Players;

import link.ignyte.plugins.bedwars.BedWars;
import link.ignyte.plugins.bedwars.Inventories.ItemStacks;
import link.ignyte.plugins.bedwars.Inventories.Setup.MapSpectateManager;
import link.ignyte.plugins.bedwars.Management.Countdown.CountdownManager;
import link.ignyte.plugins.bedwars.Management.EndCredits;
import link.ignyte.plugins.bedwars.Management.GameManager;
import link.ignyte.plugins.bedwars.Management.GameState;
import link.ignyte.plugins.bedwars.Management.WorldBorderManager;
import link.ignyte.plugins.bedwars.Maps.Teams.Team;
import link.ignyte.plugins.bedwars.Maps.Teams.TeamManager;
import link.ignyte.plugins.bedwars.Utils.Advancements;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import link.ignyte.plugins.bedwars.Utils.Options;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.hover.content.Text;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

public class PlayerManager implements Listener {

    public static boolean fix = true;

    private static void sendBugReportMessage(Player player) {
        TextComponent click_me = new TextComponent(ChatColor.translateAlternateColorCodes('&', MessageCreator.withPrefix("&7Please report any issues on GitLab &a[Click me]")));
        click_me.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://gitlab.com/Qrashi/invasionBW/issues/new"));
        click_me.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                new Text(new ComponentBuilder("Click to open a new issue on GitLab\nThank you for reporting!").color(net.md_5.bungee.api.ChatColor.GREEN).create())));
        player.spigot().sendMessage(click_me);
    }

    private static void playerDefault(Player player) {
        player.setLevel(0);
        player.setExp(0);
        player.getInventory().clear();
        player.setHealth(20);
        player.setFoodLevel(20);
        player.setGameMode(GameMode.ADVENTURE);
        player.setVelocity(new Vector());
    }

    public static void fixPlayer(Player player) {
        fixPlayer(player, false);
    }

    public static void fixPlayer(Player player, boolean sendTitle) {
        playerDefault(player);

        switch (GameManager.getState()) {
            case COUNTDOWN, SETUP, MAP_CHOOSE -> {
                if (MapSpectateManager.getSpectating() != null) {
                    MapSpectateManager.teleportPlayer(player);
                    return;
                }
                player.setGameMode(GameMode.ADVENTURE);
                Options.get(Options.class).spawn.teleport(player);
                if (sendTitle)
                    MessageCreator.sendTitle(player, MessageCreator.gameName(), "&aThe game wills start soon.", 60);
                if (player.hasPermission("bedwars.gamehost")) {
                    player.getInventory().setItem(4, ItemStacks.getJoinItem());
                }
            }
            case IN_GAME -> {
                switch (GameManager.getPlayType()) {
                    case PLAYING -> {
                        Team team = PlayerData.get(player).getTeam();
                        if (team == null) {
                            // Spectator
                            if (!GameManager.getMap().teamManager.putInJoinQueue(player)) {
                                // Still no space for this guy
                                GameManager.getMap().locs.spectator_spawn.teleport(player);
                                player.setGameMode(GameMode.SPECTATOR);
                                if (sendTitle) MessageCreator.sendTitle(player, "&7Spectating", "", 60);
                            }
                        } else {
                            if (sendTitle) {
                                MessageCreator.sendTitle(player, "&aRejoined!", "&aRejoined team " + team.col.teamName, 60);
                            }
                            Vector dir = GameManager.getMap().bbox.getMiddle().getLocation().add(0, team.getSpawn().getY(), 0).toVector().subtract(team.getSpawn().getLocationNoYawPitch().toVector());
                            player.teleport(team.getSpawn().getCenter().setDirection(dir));
                            player.setFallDistance(0);
                            player.setGameMode(GameMode.SURVIVAL);
                        }
                    }
                    case BUILDING -> {
                        if (sendTitle) {
                            MessageCreator.sendTitle(player, "&aBuildMode", "&eEditing " + GameManager.getMap().name, 60);
                        }
                        GameManager.getMap().locs.build_mode_spawn.teleport(player);
                        player.setGameMode(GameMode.CREATIVE);
                    }
                    case EDIT_LOBBY -> {
                        if (sendTitle)
                            MessageCreator.sendTitle(player, "&eEditing the lobby", "&cPlease be careful", 60);
                        Options.get(Options.class).spawn.teleport(player);
                        player.setGameMode(GameMode.CREATIVE);
                    }
                }
            }
            case END_PHASE -> {
                if (sendTitle)
                    player.sendMessage(MessageCreator.withPrefix(MessageCreator.gameName() + "&c will reload soon. You will then be able to play."));
                GameManager.getMap().locs.build_mode_spawn.teleport(player);
                player.setGameMode(GameMode.SPECTATOR);
            }
        }
    }

    private static void checkEnd() {
        int teams_left = 0;
        Team lastTeam = null;
        for (Team team : GameManager.getMap().teamManager.teams) {
            if (!team.respawnBlock.destroyed) {
                teams_left++;
                lastTeam = team;
            }
        }
        if (teams_left == 1) {
            EndCredits.end(lastTeam.members());
        }
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        event.setJoinMessage(MessageCreator.t("&7[&2+&7] " + player.getName()));
        World world = player.getLocation().getWorld();
        if (world != null) {
            BedWars.setWorld(world);
        }
        fixPlayer(player, true);
        if (fix) {
            WorldBorderManager.forceUpdate();
            fix = true;
        }
        Advancements.award(player, "join");
        new BukkitRunnable() {
            @Override
            public void run() {
                sendBugReportMessage(player);
            }
        }.runTaskLater(BedWars.getInstance(), 1);
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        event.setQuitMessage(MessageCreator.t("&7[&c-&7] " + event.getPlayer().getName()));
        if (GameManager.getState() == GameState.COUNTDOWN) {
            if (CountdownManager.getCountdown() > 10) {
                // Make space for other players!
                GameManager.getMap().teamManager.leave(event.getPlayer());
            }
        } else if (GameManager.getState() == GameState.IN_GAME) {
            PlayerData data = PlayerDataManager.getData(event.getPlayer());
            Team team = data.getTeam();
            if (team != null) {
                // No spectator
                TeamManager manager = GameManager.getMap().teamManager;
                if (team.respawnBlock.destroyed) {
                    manager.leave(event.getPlayer());
                } else if (team.memberCount() == 1) {
                    // Last member left
                    team.respawnBlock.destroy(team.col, "all players left");
                    manager.leave(event.getPlayer());
                    checkEnd();
                } else {
                    boolean atLeastOneOnline = false;
                    for (Player player : team.members()) {
                        if (player.isOnline()) {
                            atLeastOneOnline = true;
                            break;
                        }
                    }
                    if (!atLeastOneOnline) {
                        team.respawnBlock.destroy(team.col, "no team member online");
                        checkEnd();
                        manager.endTeam(team);
                        // Remove all the other players
                    } else {
                        // At least one online player left, others can join
                        manager.spaceFree(team);
                    }
                }
            }
        }
    }
}
