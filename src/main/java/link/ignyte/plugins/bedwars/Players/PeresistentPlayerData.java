package link.ignyte.plugins.bedwars.Players;

import com.google.gson.annotations.Expose;
import link.ignyte.utils.JsonSingleton.JsonPath;
import link.ignyte.utils.JsonSingleton.JsonSerializable;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

@JsonPath("player_data.json")
public class PeresistentPlayerData extends JsonSerializable {

    @Expose
    public HashMap<UUID, PeresistentData> player_data;

    public PeresistentPlayerData(boolean generateDefault) {
        if (generateDefault) player_data = new HashMap<>();
    }

    public static PeresistentData get(Player player) {
        return PeresistentPlayerData.get(PeresistentPlayerData.class).player_data.get(player.getUniqueId());
    }

    public static class PeresistentData {

        public int beds_destroyed;
        public int kills;
        public int deaths;
        public int wins;

        public PeresistentData() {
            beds_destroyed = 0;
            kills = 0;
            deaths = 0;
            wins = 0;
        }
    }

}
