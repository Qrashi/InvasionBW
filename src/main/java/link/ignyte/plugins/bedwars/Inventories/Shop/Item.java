package link.ignyte.plugins.bedwars.Inventories.Shop;

import com.google.gson.annotations.Expose;
import link.ignyte.plugins.bedwars.BedWars;
import link.ignyte.plugins.bedwars.Inventories.BetterItem.BetterItem;
import link.ignyte.plugins.bedwars.Inventories.BetterItem.BetterItemDescription;
import link.ignyte.plugins.bedwars.Maps.Spawners.SpawnerType;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import link.ignyte.plugins.bedwars.Utils.Utils;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;

public class Item {

    @Expose
    private final int slot;
    @Expose
    private final int reward_count;
    @Expose
    private final SpawnerType price_type;
    @Expose
    private final int price;
    @Expose
    private final int bought_on_shift_click;
    @Expose
    private final EnchantmentContainer enchantmentContainer;
    @Expose
    private Material reward_type;
    @Expose
    private boolean isAvailable;
    @Expose
    private String releaseVersion;
    @Expose
    private String name;

    public Item() {
        this.slot = 4;
        this.reward_type = Material.BARRIER;
        this.reward_count = 1;
        this.price_type = SpawnerType.BRONZE;
        this.price = 1;
        this.isAvailable = false;
        this.releaseVersion = "When configured";
        this.name = "Configure shops or take the example shop from the examples file!";
        this.bought_on_shift_click = 64;
        this.enchantmentContainer = new EnchantmentContainer();
    }

    public int getSlot() {
        return slot;
    }

    public ItemStack getPriceStack() {
        if (reward_type == null) {
            reward_type = Material.AIR;
            BedWars.getLogging().warning("Could not find material!");
        }
        if (!isAvailable) {
            return new BetterItem(
                    new BetterItemDescription(Arrays.asList("This item is &cNOT&7 avialible at the moment!", "&> This item &bwill be released &bwith " + releaseVersion)), false
            ).setMaterial(Utils.spawnerTypeToMaterial(price_type)).setAmount(price).setGlint(true).setName("&cNot released &cYET").create();
        }
        return new BetterItem((event) -> {
            PlayerInventory inv = event.getPlayer().getInventory();
            Material toCheck = Utils.spawnerTypeToMaterial(price_type);
            if (inv.contains(toCheck)) {
                int amount = 0;
                ArrayList<ItemStack> price_stacks = new ArrayList<>();
                for (ItemStack stack : inv.getContents()) {
                    if (stack == null) continue;
                    if (stack.getType() == Utils.spawnerTypeToMaterial(price_type)) {
                        amount = amount + stack.getAmount();
                        price_stacks.add(stack);
                    }
                }
                int amount_after = 0;
                if (amount >= price) {
                    ItemStack returner = new ItemStack(reward_type, reward_count);
                    if (!reward_type.isBlock()) {
                        ItemMeta meta = returner.getItemMeta();
                        meta.setDisplayName(MessageCreator.t(name));
                        returner.setItemMeta(enchantmentContainer.modifyMeta(meta));
                        enchantmentContainer.modifyMeta(returner.getItemMeta());
                    }
                    if (Utils.getArmorMaterials().contains(reward_type)) {
                        event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 1);
                        Utils.setArmor(reward_type, returner, inv);

                        amount_after = amount - price;
                    } else {
                        if (event.isRightClick()) {
                            //half stack
                            //calculate price max in inv
                            int in_inv = (amount / price) / 2;
                            if (in_inv > bought_on_shift_click) {
                                in_inv = bought_on_shift_click;
                            }
                            amount_after = amount - (in_inv * price);
                            returner.setAmount(in_inv * reward_count);
                            addItem(inv, event.getPlayer(), returner, amount, amount_after, price_stacks);
                            event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 2);
                            return true;
                        } else if (event.isShiftClick()) {
                            //full stack
                            int in_inv = (amount / price);
                            if (in_inv > bought_on_shift_click) {
                                in_inv = bought_on_shift_click;
                            }
                            amount_after = amount - (in_inv * price);
                            returner.setAmount(in_inv * reward_count);
                            addItem(inv, event.getPlayer(), returner, amount, amount_after, price_stacks);
                            event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 1);
                            return true;
                        } else {
                            amount_after = amount - price;
                            addItem(inv, event.getPlayer(), returner, amount, amount_after, price_stacks);
                            event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 1);
                            return true;
                        }
                    }

                } else {
                    event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.ITEM_SHIELD_BREAK, 1, 1);
                    event.getPlayer().sendMessage(MessageCreator.t("&7[&aShop&7]&c You dont have enough " + Utils.getSpawnMaterialName(price_type) + " &c(" + price + " needed)"));
                    return true;
                }
                //Bukkit.broadcastMessage("You have " + amount + " price and after the transaction you will have " + amount_after);
                int to_subtract = amount - amount_after;
                for (ItemStack stack : price_stacks) {
                    if (to_subtract == 0) {
                        break;
                    }
                    if (stack.getAmount() < to_subtract) {
                        to_subtract -= stack.getAmount();
                        stack.setAmount(0);
                    } else {
                        stack.setAmount(stack.getAmount() - to_subtract);
                        break;
                    }
                }
            }
            return true;
        }, Utils.spawnerTypeToMaterial(price_type))
                .setName(Utils.getSpawnMaterialName(price_type))
                .setAmount(price)
                .setMoveable(false)
                .create(
                        new BetterItemDescription("&aBuy &7this item", Arrays.asList("&aClick to buy", "&7Features:", "> &bShift&7-click to buy a full stack", "> &6Right&7-click to buy &fhalf&7 a stack", "> &fArmor &7will automatically be &aequipped&7."))
                );
    }

    public ItemStack getRewardStack() {
        if (reward_type == null) {
            reward_type = Material.AIR;
            BedWars.getLogging().warning("Could not find material!");
        }
        if (!isAvailable) {
            return new BetterItem(new BetterItemDescription(Arrays.asList("This item is &cNOT&7 avialible at the moment!", "&> This item &bwill be released &bwith " + releaseVersion))
                    , false).setMaterial(reward_type).setAmount(reward_count).setGlint(true).setName("&cNot released &cYET").create();
        }
        return new BetterItem((event) -> {
            PlayerInventory inv = event.getPlayer().getInventory();
            Material toCheck = Utils.spawnerTypeToMaterial(price_type);
            if (inv.contains(toCheck)) {
                int amount = 0;
                ArrayList<ItemStack> price_stacks = new ArrayList<>();
                for (ItemStack stack : inv.getContents()) {
                    if (stack == null) continue;
                    if (stack.getType() == Utils.spawnerTypeToMaterial(price_type)) {
                        amount = amount + stack.getAmount();
                        price_stacks.add(stack);
                    }
                }
                int amount_after = 0;
                if (amount >= price) {
                    ItemStack returner = new ItemStack(reward_type, reward_count);
                    if (!reward_type.isBlock()) {
                        ItemMeta meta = returner.getItemMeta();
                        meta.setDisplayName(MessageCreator.t(name));
                        returner.setItemMeta(enchantmentContainer.modifyMeta(meta));
                        enchantmentContainer.modifyMeta(returner.getItemMeta());
                    }
                    if (Utils.getArmorMaterials().contains(reward_type)) {
                        event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 1);
                        Utils.setArmor(reward_type, returner, inv);

                        amount_after = amount - price;
                    } else {
                        if (event.isRightClick()) {
                            //half stack
                            //calculate price max in inv
                            int in_inv = (amount / price) / 2;
                            if (in_inv > bought_on_shift_click) {
                                in_inv = bought_on_shift_click;
                            }
                            amount_after = amount - (in_inv * price);
                            returner.setAmount(in_inv * reward_count);
                            addItem(inv, event.getPlayer(), returner, amount, amount_after, price_stacks);
                            event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 2);
                            return true;
                        } else if (event.isShiftClick()) {
                            //full stack
                            int in_inv = (amount / price);
                            if (in_inv > bought_on_shift_click) {
                                in_inv = bought_on_shift_click;
                            }
                            amount_after = amount - (in_inv * price);
                            returner.setAmount(in_inv * reward_count);
                            addItem(inv, event.getPlayer(), returner, amount, amount_after, price_stacks);
                            event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 1);
                            return true;
                        } else {
                            amount_after = amount - price;
                            addItem(inv, event.getPlayer(), returner, amount, amount_after, price_stacks);
                            event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 1);
                            return true;
                        }
                    }
                } else {
                    event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.ITEM_SHIELD_BREAK, 1, 1);
                    event.getPlayer().sendMessage(MessageCreator.t("&7[&aShop&7]&c You dont have enough " + Utils.getSpawnMaterialName(price_type) + " &c(" + price + " needed)"));
                }
            }
            return true;
        }, reward_type)
                .setName(name)
                .modifyMeta((itemMeta -> {
                    enchantmentContainer.modifyMeta(itemMeta);
                    return itemMeta;
                }))
                .setAmount(reward_count)
                .create(
                        new BetterItemDescription("&aBuy &7this item", Arrays.asList("&aClick to buy", "&7Features:", "> &bShift&7-click to buy a full stack", "> &6Right&7-click to buy &fhalf&7 a stack", "> &fArmor &7will automatically be &aequipped&7."))
                );
    }

    private void addItem(PlayerInventory inv, Player player, ItemStack toAdd, int amount, int amount_after, ArrayList<ItemStack> price_stacks) {
        int to_subtract = amount - amount_after;
        for (ItemStack stack : price_stacks) {
            if (to_subtract == 0) {
                break;
            }
            if (stack.getAmount() < to_subtract) {
                to_subtract -= stack.getAmount();
                stack.setAmount(0);
            } else {
                stack.setAmount(stack.getAmount() - to_subtract);
                break;
            }
        }
        if (inv.firstEmpty() == -1) {
            BedWars.getWorld().dropItem(player.getLocation().add(0, 1, 0), toAdd);
            player.playSound(player.getLocation(), Sound.ENTITY_ITEM_PICKUP, 1, 0.1f);
        } else {
            inv.addItem(toAdd);
        }
    }

}
