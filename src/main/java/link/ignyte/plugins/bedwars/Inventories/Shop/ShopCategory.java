package link.ignyte.plugins.bedwars.Inventories.Shop;

import com.google.gson.annotations.Expose;
import link.ignyte.plugins.bedwars.BedWars;
import link.ignyte.plugins.bedwars.Inventories.BetterItem.BetterItem;
import link.ignyte.plugins.bedwars.Inventories.BetterItem.BetterItemDescription;
import link.ignyte.plugins.bedwars.Inventories.InventoryHandeler;
import link.ignyte.plugins.bedwars.Inventories.ItemStacks;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

public class ShopCategory {

    @Expose
    private final ArrayList<Item> itemList;
    @Expose
    private final Material thumbItem;
    @Expose
    private final String categoryName;

    public ShopCategory() {
        itemList = new ArrayList<>();
        itemList.add(new Item());
        this.thumbItem = Material.BARRIER;
        categoryName = "Unconfigured category";
    }

    public Inventory generate() {
        Inventory inv = getBase();
        for (Item item : itemList) {
            inv.setItem(item.getSlot(), item.getRewardStack());
            inv.setItem(item.getSlot() + 9, item.getPriceStack());
        }
        return inv;
    }

    private Inventory getBase() {
        Inventory inv = InventoryHandeler.createInventory(MessageCreator.gameName() + "&f » &7" + categoryName);
        int category = 0;
        for (int i = 28; i < 44; i++) {
            if (i == 35) {
                i = 36;
                continue;
            }
            if (ShopManager.selectedShop.getCategory(category) == null) {
                inv.setItem(i, ItemStacks.getNothing());
                category++;
                continue;
            }
            inv.setItem(i, ShopManager.selectedShop.getCategory(category).getThumb(category));
            category++;
        }
        return inv;
    }

    public ItemStack getThumb(int category) {
        return new BetterItem((event) -> {
            new BukkitRunnable() {
                @Override
                public void run() {
                    ShopManager.inv(category, event.getPlayer());
                }
            }.runTaskLater(BedWars.getInstance(), 1);
            return true;
        }, thumbItem).setName("&7" + categoryName).create(new BetterItemDescription("Open " + categoryName, List.of("&7Click to open this category")));
    }

}
