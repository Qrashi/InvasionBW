package link.ignyte.plugins.bedwars.Inventories.Setup;

import link.ignyte.plugins.bedwars.BedWars;
import link.ignyte.plugins.bedwars.BuildMode.BuildModeManager;
import link.ignyte.plugins.bedwars.Inventories.BetterItem.BetterItem;
import link.ignyte.plugins.bedwars.Inventories.BetterItem.BetterItemDescription;
import link.ignyte.plugins.bedwars.Inventories.InvOpener;
import link.ignyte.plugins.bedwars.Inventories.InventoryHandeler;
import link.ignyte.plugins.bedwars.Management.Countdown.CountdownManager;
import link.ignyte.plugins.bedwars.Management.Countdown.TeamGUI;
import link.ignyte.plugins.bedwars.Management.GameManager;
import link.ignyte.plugins.bedwars.Management.GameState;
import link.ignyte.plugins.bedwars.Management.PlayType;
import link.ignyte.plugins.bedwars.Maps.MapManager;
import link.ignyte.plugins.bedwars.Players.PlayerDataManager;
import link.ignyte.plugins.bedwars.Utils.Advancements;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import link.ignyte.plugins.bedwars.Utils.Utils;
import net.wesjd.anvilgui.AnvilGUI;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Arrays;
import java.util.List;

public class SetupInventories {

    public static Inventory setupInv(Player player) {
        Inventory inv = InventoryHandeler.createInventory(MessageCreator.gameName() + "&a setup");
        PlayType playType = GameManager.getPlayType();
        if (GameManager.getState() == GameState.MAP_CHOOSE) {
            // We already know what to play
            if (playType == PlayType.BUILDING) {
                inv.setItem(21, new BetterItem((event) -> {
                    createMapGUI(event.getPlayer());
                    return true;
                }, Material.BLAZE_POWDER)
                        .setGlint(true)
                        .setName("&aCreate a new map")
                        .create(
                                new BetterItemDescription("&bCreate &7a &a&lnew map", Arrays.asList("Creating a map is &beasy!", "Click here and enter the name", "of your map.", "&7A &b9k * 9k space &7will then get reserved for your map.", "", "&aWill grant you the advancement ", "&6Master Builder&7!", "", "&4Please be aware of griefers, there is NO BACKUP!", "", "&6Will grant \"Master builder\" achievement."))
                        ));
                inv.setItem(23, new BetterItem((event) -> {
                    InvOpener.openDelay(event.getPlayer(), MapChooser.getMapChooseInv(event.getPlayer()));
                    return true;
                }, Material.WOODEN_AXE)
                        .setName("&eEdit a map")
                        .create(
                                new BetterItemDescription("&eEdit&7 an&a existing &7map", Arrays.asList("&eEditing &7an existing map is easy!", "", "Choose the map you would like to &eedit", "&7and &astart building!", "", "&4Please be aware of griefs, there is NO BACKUP!"))
                        ));
            } else if (playType == PlayType.PLAYING) {
                return MapChooser.getMapChooseInv(player);
            }
        } else if (GameManager.getState() == GameState.SETUP) {
            // No play state has been set
            if (MapManager.get(MapManager.class).maps.size() == 0) {
                inv.setItem(19, new BetterItem(new BetterItemDescription(Arrays.asList("&cCannot play", "&cNo maps playable!", "", "&aFinish a map and then try again.")), false, Material.BARRIER)
                        .setName("&c&lCannot play")
                        .create());
            } else {
                inv.setItem(19, new BetterItem((event) -> {
                    GameManager.setPlayType(PlayType.PLAYING);
                    InvOpener.openDelay(player, SetupInventories.setupInv(event.getPlayer()));
                    return true;
                }, Material.IRON_SWORD)
                        .setName("&6&lPlay a game")
                        .setGlint(playType == PlayType.PLAYING)
                        .create(
                                new BetterItemDescription("Choose a map and play", Arrays.asList("Once you have chosen a game mode,", "click on &acontinue &7(&agreen&7 glass pane)", "", "&cPlease note:", "Once a mode is &aselected&7,", "You will need to &c&lreset " + MessageCreator.gameName(), "in order to &achange it."))
                        ));
            }
            inv.setItem(22, new BetterItem((event) -> {
                GameManager.setPlayType(PlayType.BUILDING);
                InvOpener.openDelay(player, SetupInventories.setupInv(event.getPlayer()));
                return true;
            }, Material.SMOOTH_SANDSTONE)
                    .setName("&a&lBuildMode")
                    .setGlint(playType == PlayType.BUILDING)
                    .create(
                            new BetterItemDescription("Choose / create a map and &bbuild it", Arrays.asList("Once you have chosen a game mode,", "click on &acontinue &7(&agreen&7 glass pane)", "", "&cPlease note:", "Once a mode is &aselected&7,", "You will need to &c&lreset " + MessageCreator.gameName(), "in order to &achange it."))
                    ));
            if (playType == PlayType.BUILDING || playType == PlayType.EDIT_LOBBY) {
                inv.setItem(31, new BetterItem((event) -> {
                    GameManager.setPlayType(PlayType.EDIT_LOBBY);
                    InvOpener.openDelay(player, SetupInventories.setupInv(event.getPlayer()));
                    return true;
                }, Material.GRASS_BLOCK)
                        .setName("&6&lEdit the lobby")
                        .setGlint(playType == PlayType.EDIT_LOBBY)
                        .create(
                                new BetterItemDescription("Edit lobby", Arrays.asList("Once you have chosen a game mode,", "click on &acontinue &7(&agreen&7 glass pane)", "", "&cPlease note:", "Once a mode is &aselected&7,", "You will need to &c&lreset " + MessageCreator.gameName(), "in order to &achange it.", "", "&cATTENTION:", "The lobby is &cNOT &7protected in &cany way!"))
                        ));
            }
            if (playType == null) {
                inv.setItem(25, new BetterItem((event) -> {
                    event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.ITEM_SHIELD_BREAK, 2, 1);
                    return true;
                }, Material.RED_STAINED_GLASS_PANE)
                        .setName("&c&lSelect a mode to continues!")
                        .setGlint(false)
                        .create(
                                new BetterItemDescription(Arrays.asList("&cYou can not continue!", "", "&c&lNo mode has been selected!", "Select a mode by &aclicking &7on it.", "", "&bPRO TIP:", "&aRead all descriptions of the items!"))
                        ));
            } else {
                inv.setItem(25, new BetterItem((event) -> {
                    event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 2, 1);
                    GameManager.modeSelectComplete();
                    return true;
                }, Material.LIME_STAINED_GLASS_PANE)
                        .setName("&a&lContinue")
                        .setGlint(true)
                        .create(
                                new BetterItemDescription("Continue", List.of("&aContinue " + MessageCreator.gameName() + " setup"))
                        ));
            }
        }


        return inv;
    }

    public static void mapInvFinished() {
        PlayType playType = GameManager.getPlayType();
        if (playType == PlayType.BUILDING) {
            BuildModeManager.startBuild(GameManager.getMap());
        } else if (playType == PlayType.PLAYING) {
            GameManager.map.teamManager.init();
            for (Player player : Bukkit.getOnlinePlayers()) {
                player.closeInventory();
                player.getInventory().clear();
            }
            GameManager.setState(GameState.COUNTDOWN);
            GameManager.map.bbox.setToAir(Utils.getMaterialList());
            for (Player player : Bukkit.getOnlinePlayers()) {
                MessageCreator.sendTitle(player, "&aStarting countdown!", "&7" + GameManager.map.name, 20);
                player.getInventory().setItem(4, new BetterItem((event) -> {
                    InvOpener.openDelay(event.getPlayer(), TeamGUI.getTeamGUI(event.getPlayer()));
                    return true;
                }, Material.RED_BED)
                        .setName("&aSelect a team")
                        .setGlint(PlayerDataManager.getData(player).getTeam() == null)
                        .create(
                                new BetterItemDescription("Join a team", Arrays.asList("Click to open the &bTeamGUI", "Here you can", "> &ajoin", "> &asee", "&aall &7teams!"))
                        ));
            }
            MessageCreator.sendWithPrefix("&cPlease note that the plugin is currently in alpha phase!");
            CountdownManager.startCountdown();
        }
    }

    private static void createMap(String name, Player player) {
        if (MapManager.exists(name)) {
            player.sendMessage(MessageCreator.withPrefix("&cMap name already in use!"));
            new BukkitRunnable() {
                @Override
                public void run() {
                    new AnvilGUI.Builder()
                            .onComplete((player, text) -> {
                                new BukkitRunnable() {
                                    @Override
                                    public void run() {
                                        createMap(text, player);
                                        InvOpener.closeLater(player);
                                    }
                                }.runTaskLater(BedWars.getInstance(), 10);
                                return AnvilGUI.Response.text("Checking name");
                            })
                            .title("Enter your maps name")
                            .text(MessageCreator.t("&cName already in use"))
                            .plugin(BedWars.getInstance())
                            .open(player);
                }
            }.runTaskLater(BedWars.getInstance(), 1);
        } else {
            player.sendMessage(MessageCreator.withPrefix("&aCreating map " + name + "..."));
            for (Player players : Bukkit.getOnlinePlayers()) {
                InvOpener.closeLater(players);
                MessageCreator.sendTitle(players, "&acreating map", "&7please wait...", 40);
            }
            MapManager.newMap(name);
            Advancements.award(player, "bedwars/root/build/create");
            BuildModeManager.startBuild(GameManager.getMap());
        }

    }

    public static void createMapGUI(Player clickedPlayer) {
        new BukkitRunnable() {
            @Override
            public void run() {
                new AnvilGUI.Builder()
                        .onComplete((player, text) -> {
                            new BukkitRunnable() {
                                @Override
                                public void run() {
                                    createMap(text, player);
                                    InvOpener.closeLater(player);
                                }
                            }.runTaskLater(BedWars.getInstance(), 20);
                            return AnvilGUI.Response.text("Checking name");
                        })
                        .title("Name your map")
                        .text("Enter name here")
                        .plugin(BedWars.getInstance())
                        .open(clickedPlayer);
            }
        }.runTaskLater(BedWars.getInstance(), 1);

    }
}
