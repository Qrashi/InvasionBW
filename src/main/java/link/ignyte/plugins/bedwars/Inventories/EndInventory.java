package link.ignyte.plugins.bedwars.Inventories;

import link.ignyte.plugins.bedwars.BedWars;
import link.ignyte.plugins.bedwars.Inventories.BetterItem.BetterItem;
import link.ignyte.plugins.bedwars.Inventories.BetterItem.BetterItemDescription;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Arrays;

public class EndInventory {

    public static boolean confirmed = false;
    public static BukkitRunnable task;
    public static boolean active;

    // I hate this, this is so unnecessary please kill me

    public static Inventory getInv() {
        Inventory inv = InventoryHandeler.createInventory("&cEnd the game");
        if (confirmed) {
            inv.setItem(21, new BetterItem(new BetterItemDescription(
                            Arrays.asList("&aAlready confirmed!", "&aConfirmation &7will &cexpire &7in &c10 &7seconds.")
                    ), false)
                            .setMaterial(Material.LIME_STAINED_GLASS_PANE).setName("&aAlready confirmed!").create()
            );
            inv.setItem(23, new BetterItem((event) -> {
                if (event.isRightClick()) {
                    EndInventory.setConfirmed(false);
                    InvOpener.closeDelay(event.getPlayer());
                    MessageCreator.sendPlayersWithPermission("&aThe game end token was invalidated.", "bedwars.gamehost");
                } else {
                    if (EndInventory.isConfirmed()) {
                        EndInventory.endGame();
                    }
                }
                return true;
            }, Material.BARRIER)
                    .setName("&4End this game")
                    .create(
                            new BetterItemDescription("Reset " + MessageCreator.gameName(), "Invalidate token", Arrays.asList("All unsaved progress will be &4lost", "All running &agames &7will be &ccanceled &7without players recieving &arewards&7."))
                    ));
        } else {
            inv.setItem(21, new BetterItem((event) -> {
                EndInventory.confirm(event.getPlayer());
                InvOpener.openDelay(event.getPlayer(), EndInventory.getInv());
                return true;
            }, Material.LEVER)
                    .setName("&4Confirm to end")
                    .setGlint(true)
                    .create(
                            new BetterItemDescription("Confirm to end this game", Arrays.asList("The confirmation will", "&cexpire&7 in &c10&7 seconds."))
                    ));
            inv.setItem(23, new BetterItem(new BetterItemDescription(
                            Arrays.asList("Please &aconfirm &7using the &blever &7(&f&l>&7)", "&aAfterwards&7, you will be able to &aend the game. &7(&f&l>&7)")
                    ), false)
                            .setMaterial(Material.RED_STAINED_GLASS_PANE).setName("&cNot confirmed!").create()
            );
        }
        return inv;
    }

    public static boolean isConfirmed() {
        return confirmed;
    }

    public static void setConfirmed(boolean newConfirmed) {
        confirmed = newConfirmed;
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (player.getOpenInventory().getTitle().equals(MessageCreator.t("&cEnd the game"))) {
                InvOpener.openDelay(player, EndInventory.getInv());
            }
        }
    }

    static void confirm(Player player) {
        if (confirmed) {
            player.sendMessage(MessageCreator.t("&cAlready confirmed!"));
            return;
        }
        MessageCreator.sendPlayersWithPermission("&cA game end token was generated", "bedwars.gamehost");
        setConfirmed(true);
        active = true;
        task = new BukkitRunnable() {
            @Override
            public void run() {
                if (EndInventory.isConfirmed()) {
                    EndInventory.setConfirmed(false);
                }
                EndInventory.active = false;
            }
        };
        task.runTaskLater(BedWars.getInstance(), 200);
    }

    public static void endGame() {
        confirmed = false;
        task.cancel();
        active = false;
        Bukkit.broadcastMessage(MessageCreator.t("&cResetting " + MessageCreator.gameName()));
        BedWars.getInstance().reload();
    }
}
