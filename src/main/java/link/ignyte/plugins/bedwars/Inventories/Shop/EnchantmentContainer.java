package link.ignyte.plugins.bedwars.Inventories.Shop;

import com.google.gson.annotations.Expose;
import link.ignyte.plugins.bedwars.BedWars;
import link.ignyte.plugins.bedwars.Inventories.BetterItem.BetterEnchant;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class EnchantmentContainer {

    @Expose
    private final ArrayList<BetterEnchant> enchantList;

    public EnchantmentContainer() {
        enchantList = new ArrayList<>();
        enchantList.add(new BetterEnchant());
    }

    void addEnchant(BetterEnchant enchant) {
        enchantList.add(enchant);
    }

    public ItemMeta modifyMeta(ItemMeta modified) {
        for (BetterEnchant enchant : enchantList) {
            if (enchant.getEnchant() == null) {
                BedWars.getLogging().warning("Cannot find enchantment " + enchant.getRawEnchant());
            }
            modified.addEnchant(enchant.getEnchant(), enchant.getLevel(), true);
        }
        return modified;
    }
}
