package link.ignyte.plugins.bedwars.Inventories.BetterItem;

import com.google.gson.annotations.Expose;
import link.ignyte.utils.JsonSingleton.JsonPath;
import link.ignyte.utils.JsonSingleton.JsonSerializable;

@JsonPath("items.json")
public class ItemStats extends JsonSerializable {

    @Expose
    private int parsed_items;
    @Expose
    private int executed_clicks;
    @Expose
    private int drops;

    public ItemStats(boolean generateDefaults) {
        parsed_items = 0;
        executed_clicks = 0;
        drops = 0;
    }

    public void add_parse() {
        parsed_items++;
    }

    public void add_execeution() {
        executed_clicks++;
    }

    public void add_drop() {
        drops++;
    }

}
