package link.ignyte.plugins.bedwars.Inventories.Setup;

import link.ignyte.plugins.bedwars.BedWars;
import link.ignyte.plugins.bedwars.Inventories.BetterItem.BetterItem;
import link.ignyte.plugins.bedwars.Inventories.BetterItem.BetterItemDescription;
import link.ignyte.plugins.bedwars.Inventories.InvOpener;
import link.ignyte.plugins.bedwars.Inventories.InventoryHandeler;
import link.ignyte.plugins.bedwars.Management.GameManager;
import link.ignyte.plugins.bedwars.Management.PlayType;
import link.ignyte.plugins.bedwars.Maps.ClearState;
import link.ignyte.plugins.bedwars.Maps.GameMap;
import link.ignyte.plugins.bedwars.Maps.MapManager;
import link.ignyte.plugins.bedwars.Players.PlayerData;
import link.ignyte.plugins.bedwars.Players.PlayerDataManager;
import link.ignyte.plugins.bedwars.Utils.MapStatistics;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.hover.content.Text;
import net.wesjd.anvilgui.AnvilGUI;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

public class MapChooser {

    private static final HashMap<String, HashMap<Integer, HashMap<Boolean, Inventory>>> inventories = new HashMap<>();
    private static final HashMap<GameMap, ItemStack> mapItems = new HashMap<>();
    // Set of maps to page to exclude unfinished to inventories
    private static ItemStack continueItem;
    private static ItemStack statistics;
    private static ItemStack search;
    private static ItemStack random;
    private static ItemStack close;
    private static ItemStack discord;
    private static ItemStack previous;
    private static ItemStack next;
    private static ItemStack show;
    private static ItemStack hide;

    public static void refresh() {
        inventories.clear();
        mapItems.clear();
        createNewInventory(new Query(), 1, true);
        continueItem = makeContinue();
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (player.getOpenInventory().getTitle().endsWith("p" + PlayerData.get(player).getPage())) {
                InvOpener.openDelay(player, getMapChooseInv(player));
            }
        }
    }

    public static void load() {

        search = new BetterItem((event) -> {
            MapChooser.startSearch(event.getPlayer(), "ESC> cancel search");
            return true;
        }, Material.GLASS_PANE)
                .setName("&fSearch &7maps")
                .create(
                        new BetterItemDescription("Search maps", List.of("Click and enter a search term"))
                );
        random = new BetterItem((event) -> {
            MapChooser.selectRandomMap();
            event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.ENTITY_FIREWORK_ROCKET_LAUNCH, SoundCategory.MASTER, 1f, 1.2f);
            MapChooser.refresh();
            return true;
        }, Material.FIREWORK_ROCKET)
                .setName("&aChoose a &lrandom&a map")
                .setGlint(true)
                .create(
                        new BetterItemDescription("Click to select a random map", List.of("&7Selects a &arandom&7, &aplayable&7 map from the map pool"))
                );
        statistics = new BetterItem(new BetterItemDescription(
                Arrays.asList("> &6Total&7 maps: " + MapStatistics.totalMaps, "> &aFinished &7maps: " + MapStatistics.finishedMaps, "> &5Full Pages&7: " + MapStatistics.pages, "> &6Total &cteams&7: " + MapStatistics.teams, "> &6Total &aplayer &7space: " + MapStatistics.players, "> &aTotal spawners: " + MapStatistics.spawners, "&7Plugin made with &c<3 &7by &8Ignyte&bLabs", "&b&n&lStatistics:")
        ), false)
                .setMaterial(Material.BREWING_STAND).setName("&5&lStatistics").create();
        if (GameManager.getPlayType() == PlayType.BUILDING) {
            close = new BetterItem((event) -> {
                InvOpener.openDelay(event.getPlayer(), SetupInventories.setupInv(event.getPlayer()));
                return true;
            }, Material.RED_STAINED_GLASS_PANE)
                    .setName("&f« &cGo back")
                    .create(
                            new BetterItemDescription("Go back", List.of(""))
                    );
        } else {
            close = new BetterItem((event) -> {
                InvOpener.closeDelay(event.getPlayer());
                return true;
            }, Material.RED_STAINED_GLASS_PANE)
                    .setName("&fx &cClose")
                    .create(
                            new BetterItemDescription("Close", List.of(""))
                    );
        }
        discord = new BetterItem((event) -> {
            TextComponent clickme = new TextComponent(ChatColor.translateAlternateColorCodes('&', "&7[&3Network&7] &9Link to our discord server &a[Click me]"));
            clickme.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://discord.gg/t7sT9Ka"));
            clickme.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                    new Text(new ComponentBuilder("Click to open the discord invitation").color(net.md_5.bungee.api.ChatColor.BLUE).create())));
            event.getPlayer().spigot().sendMessage(clickme);
            InvOpener.closeDelay(event.getPlayer());
            return true;
        }, Material.BOOK)
                .setName("&3&lInformation")
                .create(
                        new BetterItemDescription("Get a discord invite link", Arrays.asList("You may choose maps by &aclicking &7on them.", "", "&7General &3information:", "", "&fQ: &7Why are there so many &cbarriers?", "&fA: &7This is because you can't play a game on an &4unfinished &7map.", "&fA: &7The authors of the map have marked it as \"&4incomplete&7\"", "", "&fQ: &7Why are there so many gun powders?", "&fA: &7This map is &cEXPERIMENTAL &7and under &econstruction&7!", "&fA: &7You may &eedit &7and &efinish &7it.", "", "&7Click to get a &9Discord &ainvite link"))
                );
        previous = new BetterItem((event) -> {
            PlayerData playerDataN = PlayerDataManager.getData(event.getPlayer());
            if (playerDataN.getPage() != 0) {
                playerDataN.setPage(playerDataN.getPage() - 1);
                InvOpener.openDelay(event.getPlayer(), MapChooser.getMapChooseInv(event.getPlayer()));
            }
            return true;
        }, Material.REPEATER)
                .setName("&7« &aPrevious page&7 «")
                .create(
                        new BetterItemDescription("Get to the &aprevious&7 page", List.of(""))
                );
        next = new BetterItem((event) -> {
            PlayerData playerDataN = PlayerDataManager.getData(event.getPlayer());
            playerDataN.setPage(playerDataN.getPage() + 1);
            InvOpener.openDelay(event.getPlayer(), MapChooser.getMapChooseInv(event.getPlayer()));
            return true;
        }, Material.COMPARATOR)
                .setName("&7» &aNext page&7 »")
                .create(
                        new BetterItemDescription("Get to the &anext&7 page", List.of(""))
                );
        show = new BetterItem((eventa) -> {
            PlayerDataManager.getData(eventa.getPlayer()).toggle_exclude_unfinished();
            InvOpener.openDelay(eventa.getPlayer(), MapChooser.getMapChooseInv(eventa.getPlayer()));
            return true;
        }, Material.ENDER_PEARL)
                .setName("&bView &cunfinished &bmaps")
                .setGlint(false)
                .create(
                        new BetterItemDescription("View unfinished maps", List.of("&7Click to view  &cunfinished&7 maps."))
                );
        hide = new BetterItem((eventa) -> {
            PlayerDataManager.getData(eventa.getPlayer()).toggle_exclude_unfinished();
            InvOpener.openDelay(eventa.getPlayer(), MapChooser.getMapChooseInv(eventa.getPlayer()));
            return true;
        }, Material.ENDER_EYE)
                .setName("&cHide unfinished maps")
                .setGlint(true)
                .create(
                        new BetterItemDescription("Hide unfinished maps", List.of("&7Click to hide &cunfinished&7 maps."))
                );
        refresh();
    }


    public static Inventory getMapChooseInv(Player player) {
        PlayerData data = PlayerData.get(player);
        return get(data.query, data.getPage(), data.isExclude_unfinished());
    }

    private static Inventory get(Query query, int page, boolean excludeUnfinished) {
        if (inventories.containsKey(query.query)) {
            if (inventories.get(query.query).containsKey(page)) {
                if (inventories.get(query.query).get(page).containsKey(excludeUnfinished)) {
                    return inventories.get(query.query).get(page).get(excludeUnfinished);
                }
            }
        }
        return createNewInventory(query, page, excludeUnfinished);
    }

    private static Inventory createNewInventory(Query query, int page, boolean excludeUnfinished) {
        String name;
        if (query.query.equalsIgnoreCase("")) {
            name = "&aSelect a map";
            if (!excludeUnfinished && GameManager.getPlayType() != PlayType.BUILDING) {
                name = name + " | &cviewing unfinished";
            }
        } else {
            if (query.searchResults.isEmpty()) {
                name = "&cNo results found for \"&7" + query.query + "&c\"!";
            } else {
                if (query.searchResults.size() == 1) {
                    name = "&a" + query.searchResults.size() + " map&7 found for \"" + query.query + "&7\"";
                } else {
                    name = "&a" + query.searchResults.size() + " maps&7 found for \"" + query.query + "&7\"";
                }
            }
        }
        name = name + " | &bp" + page;
        Inventory inv = InventoryHandeler.createInventory(name);
        fillWithMaps(inv, query.searchResults, page, excludeUnfinished);
        if (inventories.containsKey(query.query)) {
            if (inventories.get(query.query).containsKey(page)) {
                inventories.get(query.query).get(page).put(excludeUnfinished, inv);
            } else {
                HashMap<Boolean, Inventory> exclude_per_page = new HashMap<>();
                exclude_per_page.put(excludeUnfinished, inv);
                inventories.get(query.query).put(page, exclude_per_page);
            }
        } else {
            HashMap<Integer, HashMap<Boolean, Inventory>> pages_per_query = new HashMap<>();
            HashMap<Boolean, Inventory> exclude_per_page = new HashMap<>();
            exclude_per_page.put(excludeUnfinished, inv);
            pages_per_query.put(page, exclude_per_page);
            inventories.put(query.query, pages_per_query);
        }
        return inv;
    }

    private static ItemStack makeContinue() {
        if (GameManager.getMap() != null) {
            if (GameManager.getPlayType() == PlayType.PLAYING && !GameManager.getMap().available) {
                return new BetterItem(new BetterItemDescription(
                        Arrays.asList("&4&lInvalid map!", "&cHow did we even &lget here?!", "&aReport this bug using &l/bug!")
                ), false)
                        .setMaterial(Material.BARRIER).setName("&4&lInvalid map!").create();
            } else {
                BetterItemDescription desc;
                if (GameManager.getMap().bbox.state == ClearState.NOT_CLEARED) {
                    desc = new BetterItemDescription("&cClear &7map and&a continue", Arrays.asList("Map &aready!", "You will be &b" + GameManager.getPlayType().toString().toLowerCase() + "&7 this map", "&b" + GameManager.getMap().name + " &cwill be cleared before you play."));
                } else {
                    desc = new BetterItemDescription("&aContinue", Arrays.asList("Map &aready", "You will be &b" + GameManager.getPlayType().toString().toLowerCase() + "&7 this map", "&b" + GameManager.getMap().name));
                }
                return new BetterItem((event) -> {
                    SetupInventories.mapInvFinished();
                    return true;
                }, Material.LIME_STAINED_GLASS_PANE)
                        .setName("&aEverything set!")
                        .create(desc);
            }
        } else {
            return new BetterItem(new BetterItemDescription(
                    Arrays.asList("&cNo map choosen!", "", "&a&l+ &7Please choose a map by &a&lclicking &7on it.")
            ), false)
                    .setMaterial(Material.BARRIER).setName("&cNo map choosen").create();

        }
    }

    private static ItemStack makeMapItem(GameMap map) {
        if (mapItems.containsKey(map)) {
            return mapItems.get(map);
        } else {
            BetterItem item = new BetterItem(itemClickEvent -> true, Material.BARRIER);
            if (map == null) {
                // "Selector", GameManager selected map is null
                ItemStack result = item.setName("&c&lNo map chosen").create(new BetterItemDescription(Arrays.asList("&cNo map selected!", "", "&7Select a map to &acontinue.")));
                mapItems.put(null, result);
                return result;
            } else {
                // Map has details, name etc
                if (map == GameManager.getMap()) {
                    // Selected map
                    item.setName("&aSelected: &7" + map.name);
                } else {
                    item.setName(map.name);
                }
                String status = "&aLeft click to select";
                if (GameManager.getPlayType() == PlayType.PLAYING && !map.available) {
                    status = "&cNot playable; has not been finished";
                }
                List<String> desc = Arrays.asList(status, "", "> &a" + map.teamManager.getNumberOfTeams() + "&7x&a" + map.teamManager.getTeamSize() + " &7teams", "> &cmin &7players: " + (map.teamManager.getTeamSize() + 1));
                if (GameManager.getPlayType() == PlayType.PLAYING && !map.available) {
                    // Unavialible maps should not be selectable
                    item.setDescription(new BetterItemDescription("&aRight click to&f spectate", BetterItemDescription.ClickType.RIGHT, desc).setHeader("&b&n&lDetails:"));
                    item.setOnComplete(itemClickEvent -> {
                        if (itemClickEvent.isRightClick()) {
                            MapSpectateManager.spectate(map, false);
                        }
                        return true;
                    }).setMaterial(Material.BARRIER);
                } else {
                    item.setDescription(new BetterItemDescription("&aSelect map", "&fSpectate map", desc).setHeader("&b&n&lDetails:"));
                    item.setOnComplete(itemClickEvent -> {
                        if (itemClickEvent.isRightClick()) {
                            MapSpectateManager.spectate(map, false);
                        } else {
                            GameManager.setGameMap(map);
                            MapChooser.refresh();
                        }
                        return true;
                    });
                    if (map.available) {
                        item.setMaterial(Material.SANDSTONE);
                    } else {
                        item.setMaterial(Material.GUNPOWDER);
                    }
                }
                item.setGlint(map == GameManager.getMap());
                ItemStack result = item.create();
                mapItems.put(map, result);
                return result;
            }

        }
    }

    public static void startSearch(Player player, String searchText) {
        PlayerData.get(player).query.query = searchText;
        new BukkitRunnable() {
            @Override
            public void run() {
                new AnvilGUI.Builder()
                        .onComplete(((completedPlayer, text) -> {
                            if (text.equals(searchText)) {
                                PlayerData.get(player).query.query = "";
                                PlayerData.get(player).query.searchResults = MapManager.get(MapManager.class).maps;
                            } else {
                                PlayerDataManager.getData(completedPlayer).query.query = text;
                                PlayerDataManager.getData(completedPlayer).query.searchResults = collectMaps(text);
                                PlayerDataManager.getData(completedPlayer).setPage(0);
                            }
                            InvOpener.openDelay(completedPlayer, MapChooser.getMapChooseInv(completedPlayer));
                            return AnvilGUI.Response.text(MessageCreator.withPrefix("Searching for \"" + text + "&7\""));
                        }))
                        .onClose((player1 -> {
                            if (PlayerData.get(player).query.query.equals(searchText)) {
                                PlayerData.get(player).query.query = "";
                                PlayerData.get(player).query.searchResults = MapManager.get(MapManager.class).maps;
                                InvOpener.openDelay(player1, MapChooser.getMapChooseInv(player1));
                            }
                        }))
                        .text(searchText)
                        .title("Search maps")
                        .plugin(BedWars.getInstance())
                        .open(player);
            }
        }.runTaskLater(BedWars.getInstance(), 1);
    }

    public static ArrayList<GameMap> collectMaps(String query) {
        ArrayList<GameMap> matchList = new ArrayList<>();
        for (GameMap map : MapManager.get(MapManager.class).maps) {
            if (map.name.toLowerCase().contains(query.toLowerCase())) {
                matchList.add(map);
            }
        } // update mapchooser
        return matchList;
    }

    private static void fillWithMaps(Inventory inv, ArrayList<GameMap> maps, int page, boolean excludeUnfinished) {
        inv.setItem(0, makeMapItem(GameManager.map));
        inv.setItem(8, continueItem);
        if (GameManager.getPlayType() == PlayType.PLAYING) {
            if (excludeUnfinished) {
                inv.setItem(5, show);
            } else {
                inv.setItem(5, hide);
            }
            inv.setItem(3, random);
        }
        inv.setItem(4, search);
        int mapID = 0;
        for (int slot = 9; slot < 36; slot++) {
            mapID = mapID + (page * 27);
            if (mapID >= maps.size()) {
                inv.setItem(slot, new ItemStack(Material.AIR));
            } else { // No more maps coming
                GameMap map = maps.get(mapID);
                if (excludeUnfinished && !map.available) {
                    slot--;  // Stay at the same spot but take new map
                } else {
                    inv.setItem(slot, makeMapItem(map));
                }
            }
            mapID++;
        }

        inv.setItem(44, discord);
        inv.setItem(38, previous);
        inv.setItem(40, statistics);
        inv.setItem(42, next);
        inv.setItem(36, close);
    }

    public static void selectRandomMap() {
        ArrayList<GameMap> playable = new ArrayList<>();
        for (GameMap map : MapManager.get(MapManager.class).maps) {
            if (map.available) {
                playable.add(map);
            }
        }
        if (playable.size() != 0) {  // Should always be the case.
            int index = new Random().nextInt(playable.size());
            GameManager.setGameMap(playable.get(index));
        }
        refresh();
    }

    public static class Query {
        public String query;
        public ArrayList<GameMap> searchResults;

        public Query() {
            this.query = "";
            this.searchResults = MapManager.get(MapManager.class).maps;
        }
    }
}
