package link.ignyte.plugins.bedwars.Inventories.BetterItem;

import link.ignyte.plugins.bedwars.Utils.MessageCreator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BetterItemDescription {

    private final List<ClickType> react_to;
    private final List<String> description;
    private String on_left;
    private String on_right;
    private String header = "&b&n&lInfo:";

    public BetterItemDescription(String reaction_left, String reaction_right, List<String> desc) {
        on_left = reaction_left;
        on_right = reaction_right;
        description = desc;
        react_to = Arrays.asList(ClickType.LEFT, ClickType.RIGHT);
    }

    public BetterItemDescription(String reaction, List<String> desc) {
        on_left = reaction;
        description = desc;
        react_to = List.of(ClickType.LEFT);
    }

    public BetterItemDescription(String reaction, ClickType reaction_type, List<String> desc) {
        if (reaction_type == ClickType.LEFT) {
            on_left = reaction;
        } else {
            on_right = reaction;
        }
        description = desc;
        react_to = List.of(reaction_type);
    }

    public BetterItemDescription(List<String> desc) {
        description = desc;
        react_to = List.of();
    }

    public BetterItemDescription(String... lore) {
        description = Arrays.asList(lore);
        react_to = List.of();
    }

    public BetterItemDescription(String header) {
        description = new ArrayList<>();
        this.header = header;
        react_to = List.of();
    }

    public BetterItemDescription(List<String> desc, String header) {
        this.header = header;
        description = desc;
        react_to = List.of();
    }

    public BetterItemDescription setHeader(String header) {
        this.header = header;
        return this;
    }

    public BetterItemDescription add(String toAdd) {
        description.add(toAdd);
        return this;
    }

    public BetterItemDescription add(List<String> toAdd) {
        description.addAll(toAdd);
        return this;
    }

    public BetterItemDescription setOnRight(String right) {
        on_right = right;
        return this;
    }

    public BetterItemDescription setOnLeft(String left) {
        on_left = left;
        return this;
    }

    public List<String> toLore() {
        ArrayList<String> lore = new ArrayList<>();
        lore.add("");
        if (description.size() != 0) {
            if (!description.get(0).equals("")) {
                lore.add("");
                lore.add("&f   " + header);
                for (String description_line : description) {
                    lore.add("&f   &7" + description_line);
                }
                lore.add("&f ");
                lore.add("");
            }
        }
        if (react_to.size() != 0) {
            if (react_to.contains(ClickType.LEFT)) {
                lore.add("&a&l+ &7" + on_left);
            }
            if (react_to.contains(ClickType.RIGHT)) {
                lore.add("&c&l- &7" + on_right);
            }
        }
        lore.add("");
        ArrayList<String> colorful_lore = new ArrayList<>();
        for (String item : lore) {
            colorful_lore.add(MessageCreator.t("&7" + item));
        }
        return colorful_lore;
    }

    public enum ClickType {
        LEFT,
        RIGHT
    }
}
