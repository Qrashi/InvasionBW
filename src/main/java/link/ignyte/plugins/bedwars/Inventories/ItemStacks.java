package link.ignyte.plugins.bedwars.Inventories;

import link.ignyte.plugins.bedwars.Inventories.BetterItem.BetterItem;
import link.ignyte.plugins.bedwars.Inventories.BetterItem.BetterItemDescription;
import link.ignyte.plugins.bedwars.Inventories.Setup.SetupInventories;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.Collections;

public class ItemStacks {

    private static ItemStack nthing;
    public static ItemStack bronze;
    public static ItemStack iron;
    public static ItemStack gold;
    private static ItemStack joinitem;

    public static ItemStack getJoinItem() {
        return joinitem;
    }

    public static ItemStack getNothing() {
        return nthing;
    }

    public static void regenerate() {
        iron = new BetterItem(new BetterItemDescription(
                Arrays.asList("&eSecond most &6valuable &7resource", "Used to buy", "> &fArmor", "> &cBasic Swords", "> &aTraps", "> &5Potions", "Can sometimes be found in the base.")
        ), true)
                .setMaterial(Material.IRON_INGOT).setDroppable(true).setName("&fIron").setPlaySound(false).setStatic(true).create();
        gold = new BetterItem(new BetterItemDescription(
                Arrays.asList("&6&lMost &6valuable &7resource", "Used to buy", "> &6Bows", "> &cSwords", "> &2Enderpearls", "> &cE&fx&cp&fl&co&fs&ci&fv&ce&fs", "Can be found in the &6middle &7of the map.")
        ), true)
                .setMaterial(Material.GOLD_INGOT).setDroppable(true).setName("&6Gold").setPlaySound(false).setStatic(true).create();
        bronze = new BetterItem(new BetterItemDescription(
                Arrays.asList("Least &6valuable &7resource", "Used to buy", "> &eBuilding blocks", "> &cSticks", "> &aFood", "> Ladders and &fCobwebs", "Can be found in your &6base &7of the map.")
        ), true)
                .setMaterial(Material.BRICK).setDroppable(true).setName("&cBronze").setPlaySound(false).setStatic(true).create();
        iron.setAmount(1);
        gold.setAmount(1);
        bronze.setAmount(1);
        nthing = new BetterItem(new BetterItemDescription(Collections.emptyList()), false)
                .setMaterial(Material.GRAY_STAINED_GLASS_PANE)
                .setName(" ")
                .setPlaySound(false)
                .setStatic(true)
                .create();
        joinitem = new BetterItem((event) -> {
            InvOpener.openDelay(event.getPlayer(), SetupInventories.setupInv(event.getPlayer()));
            return true;
        }, Material.RED_BED).setGlint(true).setName(MessageCreator.gameName() + " &asetup").addLore("&a&l+ &7Left click to open").setGlint(true).create();

    }
}
