package link.ignyte.plugins.bedwars.Inventories.Shop;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;

public class Shop {
    @Expose
    private final ArrayList<ShopCategory> shopCategories;
    @Expose
    private ShopCategory quickShop;


    public Shop() {
        shopCategories = new ArrayList<>();
        shopCategories.add(new ShopCategory());
        quickShop = new ShopCategory();
    }

    public ShopCategory getQuickShop() {
        return quickShop;
    }

    void addShopCategory(ShopCategory toAdd) {
        shopCategories.add(toAdd);
    }

    public ShopCategory getCategory(int i) {
        if (shopCategories.size() <= i) return null;
        return shopCategories.get(i);
    }

    public ArrayList<ShopCategory> getShopCategories() {
        return shopCategories;
    }

}
