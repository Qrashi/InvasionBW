package link.ignyte.plugins.bedwars.Inventories.BetterItem;

import link.ignyte.plugins.bedwars.BedWars;
import link.ignyte.plugins.bedwars.Utils.KeyAssistant;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.function.Function;

public class BetterItem {
    private final ArrayList<Enchantment> enchantments = new ArrayList<>();
    private final ArrayList<String> lore = new ArrayList<>();
    private final boolean execute;
    //private ItemID id;
    private final UUID uuid;
    private Material material;
    private String name = null;
    private int amount = 1;
    private Function<ItemClickEvent, Boolean> onComplete;
    private List<EventType> listenTo;
    private boolean glint = false;
    private boolean droppable = false;
    private boolean removeOnDrop = false;
    private BetterItemDescription description;
    private boolean playSound = true;
    private Function<ItemMeta, ItemMeta> applyOnMeta;
    private boolean useDesc = false;
    private boolean moveable;
    private boolean defaultTimestamp = false;

    public BetterItem(BetterItemDescription description, boolean isMoveable) {
        //better item description removes the possibility to execute any "onClick" events!
        moveable = isMoveable;
        useDesc = true;
        defaultTimestamp = false;
        execute = false;

        if (!moveable) {
            uuid = BetterItemManager.getStaticUUID();
        } else {
            uuid = BetterItemManager.registerItem(this);
        }
        this.description = description;
        applyOnMeta = (meta) -> meta;
    }

    //data stored in peresistent container:
    //uuid
    //timestamp
    //droppable
    //moveable

    public BetterItem(BetterItemDescription description, boolean isMoveable, Material material) {
        //better item description removes the possibility to execute any "onClick" events!
        moveable = isMoveable;
        useDesc = true;
        this.material = material;
        defaultTimestamp = false;
        execute = false;

        if (!moveable) {
            uuid = BetterItemManager.getStaticUUID();
        } else {
            uuid = BetterItemManager.registerItem(this);
        }
        this.description = description;
        applyOnMeta = (meta) -> meta;
    }

    public BetterItem(Function<ItemClickEvent, Boolean> onComplete, Material material, List<EventType> listenTo) {
        this.onComplete = onComplete;
        this.material = material;
        moveable = true;
        this.listenTo = listenTo;
        execute = true;
        applyOnMeta = (meta) -> meta;
        uuid = BetterItemManager.registerItem(this);

    }

    public BetterItem(Function<ItemClickEvent, Boolean> onComplete, Material material) {
        this.onComplete = onComplete;
        execute = true;
        moveable = true;
        this.material = material;
        applyOnMeta = (meta) -> meta;
        this.listenTo = Arrays.asList(EventType.DROP, EventType.EXTERNAL, EventType.INVENTORY);
        uuid = BetterItemManager.registerItem(this);

    }

    public boolean listenTo(EventType type) {
        return listenTo.contains(type);
    }

    public BetterItem setStatic(boolean statc) {
        this.defaultTimestamp = statc;
        return this;
    }

    public BetterItem setMoveable(boolean mvble) {
        this.moveable = mvble;
        return this;
    }

    //bei static uuid wird eventSetcancelled assumed ohne das item nachzusehen - staticuuid entfernen
    //besseres system für moveable etc einführen - altes löschen!!

    public BetterItem setDescription(BetterItemDescription description) {
        useDesc = true;
        this.description = description;
        return this;
    }

    public BetterItem setMaterial(Material material) {
        this.material = material;
        return this;
    }

    public BetterItem setName(String name) {
        this.name = name;
        return this;
    }

    public BetterItem modifyMeta(Function<ItemMeta, ItemMeta> func) {
        applyOnMeta = func;
        return this;
    }

    public boolean getRemoveOnDrop() {
        return removeOnDrop;
    }

    public BetterItem setRemoveOnDrop(boolean bool) {
        removeOnDrop = bool;
        return this;
    }

    public BetterItem setAmount(int amount) {
        this.amount = amount;
        return this;
    }

    public BetterItem addEnchant(Enchantment enchantment) {
        enchantments.add(enchantment);
        return this;
    }

    public BetterItem addEnchant(List<Enchantment> enchantment) {
        enchantments.addAll(enchantment);
        return this;
    }

    public BetterItem setGlint(boolean glint) {
        this.glint = glint;
        return this;
    }

    public BetterItem addLore(String toAdd) {
        lore.add(toAdd);
        return this;
    }

    public BetterItem addLore(List<String> toAdd) {
        lore.addAll(toAdd);
        return this;
    }

    public BetterItem setDroppable(boolean droppable) {
        this.droppable = droppable;
        return this;
    }

    public boolean getPlaySound() {
        return playSound;
    }

    public BetterItem setPlaySound(boolean playSound) {
        this.playSound = playSound;
        return this;
    }

    public Function<ItemClickEvent, Boolean> getOnComplete() {
        return onComplete;
    }

    public BetterItem setOnComplete(Function<ItemClickEvent, Boolean> newComplete) {
        onComplete = newComplete;
        return this;
    }

    public ItemStack create() {
        ItemStack stack = new ItemStack(material, amount);
        ArrayList<String> colorful_lore = new ArrayList<>();
        for (String item : lore) {
            colorful_lore.add(MessageCreator.t("&7" + item));
        }
        if (useDesc && description != null) {
            stack.setItemMeta(createMetadata(description.toLore(), stack.getItemMeta()));
            return stack;
        }
        stack.setItemMeta(createMetadata(colorful_lore, stack.getItemMeta()));
        return stack;
    }

    public ItemStack create(BetterItemDescription description) {
        ItemStack stack = new ItemStack(material, amount);
        stack.setItemMeta(createMetadata(description.toLore(), stack.getItemMeta()));
        return stack;
    }

    private ItemMeta createMetadata(List<String> lore, ItemMeta meta) {
        if (meta == null) return meta;
        if (name != null) {
            meta.setDisplayName(MessageCreator.t(name));
        }
        meta.getPersistentDataContainer().set(KeyAssistant.getKey("undroppable"), new PeresistentBoolean(), !droppable);
        meta.getPersistentDataContainer().set(KeyAssistant.getKey("moveable"), new PeresistentBoolean(), moveable);
        meta.getPersistentDataContainer().set(KeyAssistant.getKey("execute"), new PeresistentBoolean(), execute);
        if (defaultTimestamp) {
            meta.getPersistentDataContainer().set(KeyAssistant.getKey("timestamp"), PersistentDataType.LONG, BedWars.startup + 1);
        } else {
            meta.getPersistentDataContainer().set(KeyAssistant.getKey("timestamp"), PersistentDataType.LONG, Instant.now().getEpochSecond());
        }
        if (!defaultTimestamp) {
            meta.getPersistentDataContainer().set(KeyAssistant.getKey("uuid"), new UUIDTagType(), uuid);
        } else {
            meta.getPersistentDataContainer().set(KeyAssistant.getKey("uuid"), new UUIDTagType(), BetterItemManager.getStaticUUID());
        }
        if (moveable)
            meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_DYE, ItemFlag.HIDE_ENCHANTS);
        if (glint) {
            meta.addEnchant(BedWars.getGlow(), 1, true);
        }
        meta.setLore(lore);
        meta = applyOnMeta.apply(meta);
        return meta;
    }
}
