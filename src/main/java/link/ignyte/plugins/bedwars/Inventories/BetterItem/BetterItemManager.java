package link.ignyte.plugins.bedwars.Inventories.BetterItem;

import link.ignyte.plugins.bedwars.BedWars;
import link.ignyte.plugins.bedwars.Utils.KeyAssistant;
import link.ignyte.plugins.bedwars.Utils.error.*;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

import java.util.HashMap;
import java.util.UUID;

public class BetterItemManager implements Listener {

    private static final ItemStats stats = ItemStats.get(ItemStats.class);
    private static HashMap<UUID, BetterItem> data_storage;
    private static UUID staticUUID;

    public BetterItemManager() {
        data_storage = new HashMap<>();
        staticUUID = UUID.randomUUID();
        data_storage.put(staticUUID, new BetterItem((event) -> false, Material.AIR));
        BedWars.getLogging().info("Items initialisation phase complete.");
    }

    public static UUID getStaticUUID() {
        return staticUUID;
    }

    private static boolean checkBoolKey(PersistentDataContainer container, NamespacedKey key) {
        if (container.has(key, new PeresistentBoolean())) {
            return container.get(key, new PeresistentBoolean());
        }
        return false;
    }

    public static void reload() {
        data_storage.clear();
        //if unmoveable already cancelled
        data_storage.put(staticUUID, new BetterItem((event) -> false, Material.AIR).setPlaySound(false));
        BedWars.getLogging().info("Item stats saved");
    }

    public static BetterItem getItem(UUID uuid) {
        if (data_storage.containsKey(uuid)) return data_storage.get(uuid);
        return null;
    }

    private static UUID getUUID() {
        UUID uuid = UUID.randomUUID();
        if (data_storage.containsKey(uuid)) {
            Bukkit.broadcastMessage("The odds are insane: The computer just generated an already in use UUID!");
            return getUUID();
        }
        return uuid;
    }

    public static UUID registerItem(BetterItem toRegister) {
        /*if(data_storage.containsValue(toRegister)) {
            //if(Game.getDev()) Bukkit.broadcastMessage("&7[" + Game.getGameName() + "&7| &bDEV &7] Item same");
            for (BetterItem item : data_storage.values()) {
                if(item.equals(toRegister)) {
                    //if(Game.getDev()) Bukkit.broadcastMessage("&7[" + Game.getGameName() + "&7| &bDEV &7] Reused UUID");
                    return item.getUuid();
                }
            }
        }
        *
        * This could one day be useful if the performance of the plugin would be HORRIBLE but I don't think that it's
        * going to get that way (hopefully).
        *
        */
        UUID uuid = getUUID();
        data_storage.put(uuid, toRegister);
        return uuid;
    }

    public static boolean isValid(UUID uuid) {
        return data_storage.containsKey(uuid);
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        EventType eventType = EventType.INVENTORY;
        ItemStack item = event.getCurrentItem();
        if (item != null) {
            if (item.hasItemMeta()) {
                ItemMeta meta = item.getItemMeta();
                if (meta != null) {
                    //Bukkit.broadcastMessage("Checking time");
                    PersistentDataContainer container = meta.getPersistentDataContainer();
                    if (container.has(KeyAssistant.getKey("timestamp"), PersistentDataType.LONG)) {
                        if (container.get(KeyAssistant.getKey("timestamp"), PersistentDataType.LONG) > BedWars.startup) {
                            //parse
                            stats.add_parse();
                            if (!checkBoolKey(container, KeyAssistant.getKey("moveable"))) {
                                event.setCancelled(true);
                            }
                            if (!checkBoolKey(container, KeyAssistant.getKey("execute"))) {
                                return;
                            }

                            if (!container.has(KeyAssistant.getKey("uuid"), new UUIDTagType())) {
                                ErrorHandeler.error(new ErrorCode(ErrorEffect.CORE, ErrorSeverity.LOWEST, ErrorRelation.ITEM, "BeIt:Click:noUUID"));
                            }
                            UUID uuid = container.get(KeyAssistant.getKey("uuid"), new UUIDTagType());
                            if (BetterItemManager.isValid(uuid)) {
                                BetterItem betterItem = BetterItemManager.getItem(uuid);
                                //Bukkit.broadcastMessage("Checking listenTo");
                                if (betterItem.listenTo(eventType)) {
                                    stats.add_execeution();
                                    event.setCancelled(betterItem.getOnComplete().apply(new ItemClickEvent((Player) event.getWhoClicked(), event.isShiftClick(), null, eventType, null, event.getAction() == InventoryAction.PICKUP_HALF, betterItem.getPlaySound())));
                                }
                            }
                        } else {
                            //if(Game.getDev()) Bukkit.broadcastMessage("ALARM!");
                            item.setType(Material.AIR);
                        }
                    }
                }
            }
        }

    }

    @EventHandler
    public void onDrag(InventoryDragEvent event) {
        EventType eventType = EventType.INVENTORY;
        ItemStack item = event.getCursor();
        if (item != null) {
            if (item.hasItemMeta()) {
                ItemMeta meta = item.getItemMeta();
                if (meta != null) {
                    PersistentDataContainer container = meta.getPersistentDataContainer();
                    if (container.has(KeyAssistant.getKey("timestamp"), PersistentDataType.LONG)) {
                        //if(Game.getDev()) Bukkit.broadcastMessage("timestamp diff: " + (Game.BedWars.startup - container.get(KeyAssistant.getKey("timestamp"), PersistentDataType.LONG).longValue()));
                        if (container.get(KeyAssistant.getKey("timestamp"), PersistentDataType.LONG).longValue() > BedWars.startup) {
                            //parse
                            stats.add_parse();
                            if (!checkBoolKey(container, KeyAssistant.getKey("moveable"))) {
                                event.setCancelled(true);
                            }
                            if (!checkBoolKey(container, KeyAssistant.getKey("execute"))) {
                                return;
                            }
                            UUID uuid = container.get(KeyAssistant.getKey("uuid"), new UUIDTagType());
                            if (BetterItemManager.isValid(uuid)) {
                                BetterItem betterItem = BetterItemManager.getItem(uuid);
                                if (betterItem.listenTo(eventType)) {
                                    stats.add_execeution();
                                    event.setCancelled(betterItem.getOnComplete().apply(new ItemClickEvent((Player) event.getWhoClicked(), false, null, eventType, null, false, betterItem.getPlaySound())));
                                }
                            }
                        } else {
                            //if(Game.getDev()) Bukkit.broadcastMessage("ALARM!");
                            item.setType(Material.AIR);
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        EventType eventType = EventType.EXTERNAL;
        ItemStack item = event.getItem();
        if (item != null) {
            if (item.hasItemMeta()) {
                ItemMeta meta = item.getItemMeta();
                if (meta != null) {
                    PersistentDataContainer container = meta.getPersistentDataContainer();
                    if (container.has(KeyAssistant.getKey("timestamp"), PersistentDataType.LONG)) {
                        //if(Game.getDev()) Bukkit.broadcastMessage("time created after reload: " + (container.get(KeyAssistant.getKey("timestamp"), PersistentDataType.LONG).longValue() - Game.BedWars.startup));
                        if (container.get(KeyAssistant.getKey("timestamp"), PersistentDataType.LONG).longValue() > BedWars.startup) {
                            //parse
                            stats.add_parse();
                            if (!checkBoolKey(container, KeyAssistant.getKey("moveable"))) {
                                event.setCancelled(true);
                            }
                            if (!checkBoolKey(container, KeyAssistant.getKey("execute"))) {
                                return;
                            }
                            UUID uuid = container.get(KeyAssistant.getKey("uuid"), new UUIDTagType());
                            //Bukkit.broadcastMessage(uuid.toString());
                            if (BetterItemManager.isValid(uuid)) {
                                BetterItem betterItem = BetterItemManager.getItem(uuid);
                                if (betterItem.listenTo(eventType)) {
                                    stats.add_execeution();
                                    event.setCancelled(betterItem.getOnComplete().apply(new ItemClickEvent(event.getPlayer(), event.getPlayer().isSneaking(), event.getClickedBlock(), eventType, event.getBlockFace(), false, betterItem.getPlaySound())));
                                }
                            }
                        } else {
                            //if(Game.getDev()) Bukkit.broadcastMessage("ALARM!");
                            item.setType(Material.AIR);
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void onDrop(PlayerDropItemEvent event) {
        EventType eventType = EventType.INVENTORY;
        ItemStack item = event.getItemDrop().getItemStack();
        if (item.hasItemMeta()) {
            ItemMeta meta = item.getItemMeta();
            if (meta != null) {
                PersistentDataContainer container = meta.getPersistentDataContainer();
                if (container.has(KeyAssistant.getKey("timestamp"), PersistentDataType.LONG)) {
                    ////if(Game.getDev()) Bukkit.broadcastMessage("timestamp diff: " + (Game.BedWars.startup - container.get(KeyAssistant.getKey("timestamp"), PersistentDataType.LONG).longValue()));
                    if (container.get(KeyAssistant.getKey("timestamp"), PersistentDataType.LONG).longValue() > BedWars.startup) {
                        //parse
                        stats.add_execeution();
                        //if(Game.getDev()) Bukkit.broadcastMessage("checking unmoveable!");
                        if (container.has(KeyAssistant.getKey("undroppable"), new PeresistentBoolean()) && container.has(KeyAssistant.getKey("uuid"), new UUIDTagType())) {
                            event.setCancelled(container.get(KeyAssistant.getKey("undroppable"), new PeresistentBoolean()));
                        }
                        if (!checkBoolKey(container, KeyAssistant.getKey("moveable"))) {
                            event.setCancelled(true);
                        }
                        if (!checkBoolKey(container, KeyAssistant.getKey("execute"))) {
                            return;
                        }

                        UUID uuid = container.get(KeyAssistant.getKey("uuid"), new UUIDTagType());
                        if (BetterItemManager.isValid(uuid)) {
                            BetterItem betterItem = BetterItemManager.getItem(uuid);
                            if (betterItem.listenTo(eventType)) {
                                if (betterItem.getRemoveOnDrop()) {
                                    event.getItemDrop().remove();
                                    return;
                                }
                                boolean shift = event.getItemDrop().getItemStack().getAmount() > 1 || event.getPlayer().isSneaking();
                                stats.add_drop();
                                stats.add_execeution();
                                event.setCancelled(betterItem.getOnComplete().apply(new ItemClickEvent(event.getPlayer(), shift, null, eventType, null, false, betterItem.getPlaySound())));
                            }
                        }
                    } else {
                        //if(Game.getDev()) Bukkit.broadcastMessage("ALARM!");
                        item.setType(Material.AIR);
                    }
                }
            }
        }
    }
}
