package link.ignyte.plugins.bedwars.Inventories.Setup;

import link.ignyte.plugins.bedwars.Inventories.BetterItem.BetterItem;
import link.ignyte.plugins.bedwars.Inventories.BetterItem.BetterItemDescription;
import link.ignyte.plugins.bedwars.Inventories.InvOpener;
import link.ignyte.plugins.bedwars.Inventories.ItemStacks;
import link.ignyte.plugins.bedwars.Management.ScoreboardManager;
import link.ignyte.plugins.bedwars.Management.WorldBorderManager;
import link.ignyte.plugins.bedwars.Maps.GameMap;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import link.ignyte.plugins.bedwars.Utils.Options;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class MapSpectateManager {

    private static GameMap spectating = null;
    private static boolean creative;
    private static Location teleport;

    public static void spectate(GameMap map, boolean creative) {
        spectating = map;
        MapSpectateManager.creative = creative;
        teleport = spectating.locs.build_mode_spawn.getTpLocation();
        for (Player player : Bukkit.getOnlinePlayers()) {
            teleportPlayer(player);
        }
        WorldBorderManager.forceUpdate();
        ScoreboardManager.now();
    }

    public static void endSpectate() {
        spectating = null;
        creative = false;
        teleport = null;
        for (Player player : Bukkit.getOnlinePlayers()) {
            player.getInventory().clear();
            player.setGameMode(GameMode.SURVIVAL);
            player.teleport(Options.get(Options.class).spawn.getLocation());
            player.setHealth(20);
            player.setFoodLevel(20);
            if (player.hasPermission("BedWars.gamehost")) {
                player.getInventory().setItem(4, ItemStacks.getJoinItem());
            }
            player.sendTitle(MessageCreator.t("&7Teleporting..."), MessageCreator.t("&aReturning to the lobby..."), 5, 40, 20);
        }
        WorldBorderManager.forceUpdate();
        ScoreboardManager.now();
    }

    public static GameMap getSpectating() {
        return spectating;
    }

    public static void teleportPlayer(Player player) {
        player.setGameMode(GameMode.CREATIVE);
        player.setFlying(true);
        InvOpener.closeDelay(player);
        player.getInventory().clear();
        if (player.hasPermission("BedWars.gamehost")) {
            player.getInventory().setItem(4, new BetterItem((event) -> {
                        if (event.getPlayer().hasPermission("BedWars.gamehost")) MapSpectateManager.endSpectate();
                        return true;
                    }, Material.RED_STAINED_GLASS_PANE)
                            .setName("&cStop spectating this map")
                            .create(
                                    new BetterItemDescription("&cStop &7spectating" + spectating.name, Arrays.asList("Will teleport all players", " back to the lobby.", "Can only be used by &cadmins&7 (like you)."))
                            )
            );
        }
        player.teleport(teleport);
        if (creative) {
            player.sendTitle(MessageCreator.t("&7Teleporting..."), MessageCreator.t("&cInspecting not empty map"), 5, 40, 20);
        } else {
            player.sendTitle(MessageCreator.t("&7Teleporting..."), MessageCreator.t("&aSpectating " + spectating.name), 5, 40, 20);
        }
    }
}
