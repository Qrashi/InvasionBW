package link.ignyte.plugins.bedwars.Inventories.Shop;

import com.google.gson.annotations.Expose;
import link.ignyte.plugins.bedwars.BedWars;
import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import link.ignyte.utils.JsonSingleton.JsonPath;
import link.ignyte.utils.JsonSingleton.JsonSerializable;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@JsonPath("shops.json")
public class ShopManager extends JsonSerializable {

    private static final ArrayList<Inventory> inventories = new ArrayList<>();
    //Temporary
    public static Shop selectedShop; //Cache  selected shop for easier access
    private static Inventory qshop;
    @Expose
    private String selected;
    @Expose
    private HashMap<String, Shop> shops;


    public ShopManager(boolean generateDefaults) {
        if (generateDefaults) {
            shops = new HashMap<>();
            shops.put("DefaultShop", new Shop());
            selected = "DefaultShop";
            selectedShop = shops.get("DefaultShop");
        }
    }

    public static void generate() {
        inventories.clear();
        BedWars.getLogging().info("Generating shops...");
        for (ShopCategory category : ShopManager.selectedShop.getShopCategories()) {
            inventories.add(category.generate());
        }
        qshop = ShopManager.selectedShop.getQuickShop().generate();
    }

    public static boolean exists(String shopName) {
        return ShopManager.get(ShopManager.class).shops.containsKey(shopName);
    }

    public static void inv(int selected, Player openTo) {
        Inventory inv = inventories.get(selected);

        if (selected > 6) {
            selected = selected + 2;
        }
        int slotToModify = 28 + selected;
        ItemStack stack = inv.getItem(slotToModify);
        ItemMeta meta = stack.getItemMeta();
        ItemMeta unmodified = meta.clone();

        meta.addEnchant(BedWars.getGlow(), 1, true);
        stack.setItemMeta(meta);

        openTo.openInventory(inv);
        stack.setItemMeta(unmodified);
    }

    public static Inventory inv() {
        return qshop;
    }

    public static void load() {
        selectedShop = ShopManager.get(ShopManager.class).shops.get(ShopManager.get(ShopManager.class).selected);
        generate();
    }

    public static void select(String shop) {
        MessageCreator.sendWithPrefix("&aSwitched shop!");
        ShopManager.get(ShopManager.class).selected = shop;
        selectedShop = ShopManager.get(ShopManager.class).shops.get(shop);
        generate();
        BedWars.log("Shop switched!");
    }

    public List<String> shopNames() {
        return new ArrayList<>(shops.keySet());
    }

    public String selected() {
        return selected;
    }
}
