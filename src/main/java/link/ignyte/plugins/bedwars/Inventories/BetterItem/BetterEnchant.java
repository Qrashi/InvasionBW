package link.ignyte.plugins.bedwars.Inventories.BetterItem;

import com.google.gson.annotations.Expose;
import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;

public class BetterEnchant {
    @Expose
    private final int level;
    @Expose
    private final String enchant;

    public BetterEnchant() {
        level = 3;
        enchant = "UNBREAKING";
    }

    public int getLevel() {
        return level;
    }

    public String getRawEnchant() {
        return enchant;
    }

    public Enchantment getEnchant() {
        return Enchantment.getByKey(NamespacedKey.minecraft(enchant));
    }
}
