package link.ignyte.plugins.bedwars.Inventories;

import link.ignyte.plugins.bedwars.Utils.MessageCreator;
import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;

import java.util.stream.IntStream;

public class InventoryHandeler {
    public static Inventory createInventory(String name, int size) {
        Inventory inv = Bukkit.createInventory(null, size, MessageCreator.t(name));
        IntStream.range(0, size).forEachOrdered(n -> inv.setItem(n, ItemStacks.getNothing()));
        return inv;
    }

    public static Inventory createInventory(String name) {
        return createInventory(name, 45);
    }
}
